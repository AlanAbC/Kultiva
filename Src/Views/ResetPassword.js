import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, StatusBar} from 'react-native';
import Button from 'react-native-button';
// import PropTypes from 'prop-types';
import ip from '../Config/AppConfig';
import Input from '../Components/Input/Input';
class ResetPassword extends Component{
    state = {
        mail: '',
    };
    _seeTerms = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Terms', 
            title: 'Terms',
            navigatorStyle: {
              navBarHidden: true
            }
        });
    };
    _send = (mail) =>{
        this.setState({showTheThing: true}) 
        if(mail != ''){
                fetch(ip + 'users/reset_password/',{
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        email: mail,
                      }),
                })
                .then((response) => {
                    if(response.status == 200){
                        this.setState({showTheThing: false}) 
                        Alert.alert(
                            'Gracias!',
                            'Se ah enviado un link de restablecimiento a tu correo',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                          this.props.navigator.pop({
                            animated: true, // does the pop have transition animation or does it happen immediately (optional)
                            animationType: 'fade', // 'fade' (for both) / 'slide-horizontal' (for android) does the pop have different transition animation (optional)
                          });
                    }else if (response.status == 404){
                        this.setState({showTheThing: false}) 
                        Alert.alert(
                            'Error',
                            'No podemos recuperar información Kulttiva',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    }
                })
                .catch((error) => {
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                    'Error',
                    'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                    [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                    )
                    return;
                });
        }else{
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'Por favor ingresa todo los campos',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
    };
    mailChangeHandler = val =>{
        console.log(val);
        this.setState({
            mail: val
        });
    };
    render(){
        var s = require('../Styles/StyleResetPassword');
        return(
            <View style={s.container}>
            <StatusBar barStyle="light-content"/>
                <View style= {s.header}>
                    <Text style={s.titleBlack}>RECUPERA TU</Text>
                    <Text style={s.titleCopper}>CONTRASEÑA</Text>
                    <Text style = {s.txtContent}>Ingresa tu correo para recuperar tu cuenta</Text>
                </View>
                <View style = {s.medium}>
                <Input
                    style={s.viewContainerComponent}
                    name={"ios-mail-outline"}
                    placeholder={'E-mail'}
                    handler={this.mailChangeHandler}
                    value={this.state.mail}
                    spell={false}
                    autoCapitalize={'none'}  
                    keyboardType={'email-address'}    
                    />
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
                 <Button
                    onPress={() => this._send(this.state.mail)}
                    containerStyle={s.btnBlack}
                    style={{fontSize: 20, color: 'white', paddingTop: 10,}}
                >Enviar</Button>
                </View>

                <TouchableOpacity style={s.footer}  onPress={() => this._seeTerms()}>
                        <Image
                         style={s.imgFooter}
                        source={require('../Img/footerImg.png')}
                        />
                        <Text style= {s.txtFooter}>Revisa términos y condiciones</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
export default ResetPassword;