import React, { Component } from 'react';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation';
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, FlatList, Picker, StatusBar} from 'react-native';
import { List, ListItem, SearchBar, Card } from "react-native-elements";
import Menu from 'react-native-pop-menu';
import Icon from 'react-native-vector-icons/Ionicons';
import ListItemNewKultivo from '../Components/ListItemNewKultivo/ListItemNewKultivo';
import ip from '../Config/AppConfig';
import ModalDropdown from 'react-native-modal-dropdown';
import ls from 'react-native-local-storage';
class NewKultivo extends Component {
    state = {
      menuVisible: false,
      location: '',
      loading: false,
      data: [],
      schedules: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      dificulty : 1,
      farmer_id: 0
  };
  componentDidMount() {
    ls.get('farmer_id').then((local) => {this.makeRemoteRequest(this.state.dificulty, local)});
    ls.get('farmer_id').then((local) => {this.setState({farmer_id: local})});
  }
  singleView(id, dificulty){
    this.props.navigator.push({
        screen: 'Kultiva.SingleElement', 
        title: 'Mi Kulttivo',
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white',
          navBarButtonColor: 'white',
          tabBarHidden: true
        }, 
        passProps: {
            id: id,
            dificulty: dificulty 
        }
      });
  }
  makeRemoteRequest = (dificulty,  farmer_id) => {
    if(this.state.refreshing != true){
      this.setState({showTheThing: true}) 
    }
    const { page, seed } = this.state;
    const url = ip + 'crops/get_crops/'+farmer_id+'/' + dificulty + '/';
    this.setState({ loading: true, data: [], dificulty: dificulty });
    fetch(url)
      .then(res => {
        // debugger;
        if(res.status == 200){
          this.setState({showTheThing: false}) 
            data = JSON.parse(res._bodyText);
            console.log(data);
            this.setState({
                location: data.location,
                data: data.crops,
                error: res.error || null,
                loading: false,
                refreshing: false
              });
        }else if(res.status == 204){
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Ups',
                'No hay kulttivos para esta dificultad',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }else if (res.status == 404){
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No podemos recuperar información Kulttiva',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }else{
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No se puede procesar la solicitud',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
      })
      .catch(error => {
        this.setState({showTheThing: false}) 
            Alert.alert(
              'Error',
              'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
      });
  };
    render() {
      var s = require('../Styles/StyleNewKultivo');
        return (
          <View style = {s.container}>
          <StatusBar barStyle="light-content"/>
            <View style = {s.topElements}>
                <Text style = {s.titleBlack}>SELECCIONA TU</Text>
                <Text style = {s.titleCopper}>KULTTIVO</Text>
                <Text style = {s.subTitleBlack}>Kulttivos recomendados para tu zona</Text>
                <Text style = {s.subTitleGray}>{this.state.location}</Text>
                <View style = {s.sectionDropdown}>
                    <Text style = {s.txtDropdown}>Cultivos nivel:</Text>
                    <ModalDropdown onSelect={(selected) => this.makeRemoteRequest((parseInt(selected) + 1), this.state.farmer_id)} textStyle = {s.txtComboBox} dropdownStyle = {s.combocontent} style = {s.combobox} defaultValue = {'Principiante'} options={['Principiante', 'Medio', 'Dificil']}/>
                </View>
            </View>
            <View style = {s.bottomElements}>
            { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
            <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0}}>
                <FlatList
                data={this.state.data}
                renderItem={({ item }) => (
                    <ListItemNewKultivo onPress = {() => this.singleView(item.crop_id, item.difficulty)} image = {item.image} name = {item.name} firstCos = {item.crop_time} dificulty = {item.difficulty} weather = {item.weather}/>
                )}
                keyExtractor = {item => item.crop_id}
                />
            </List>

            </View>
            
          </View>
          
          );
    }
}
export default NewKultivo;