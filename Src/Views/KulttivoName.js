import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, FlatList, Picker, StatusBar} from 'react-native';
import { List, ListItem, SearchBar, Card } from "react-native-elements";
import Menu from 'react-native-pop-menu';
import Icon from 'react-native-vector-icons/Ionicons';
import ListItemNewKultivo from '../Components/ListItemNewKultivo/ListItemNewKultivo';
import ButtonDark from '../Components/ButtonDark/ButtonDark';
import CircleIdentifier from '../Components/CircleIdentifier/CircleIdentifier'
import ip from '../Config/AppConfig';
import ModalDropdown from 'react-native-modal-dropdown';
import {Navigation} from 'react-native-navigation';
import ls from 'react-native-local-storage';
class KulttivoName extends Component {
  state = {
      flags: [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false],
      colors: ['#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff'],
      name: '',
      id: this.props.id,
      lat: this.props.lat,
      lon: this.props.lon,
      farmer_id: 0
  };
  componentDidMount(){
    ls.get('farmer_id').then((local) => {this.setState({farmer_id: local})});
  }
  changeStateElements = (index, color) => {
    for(i = 0; i<15; i++){
        if(i === index){
            if(this.state.flags[index] === true){
                let newColors = this.state.colors.slice()
                newColors[index] = "#ffffff" 
                this.setState({colors: newColors})
                let newFlags = this.state.flags.slice()
                newFlags[index] = false 
                this.setState({flags: newFlags})    
            }else{
                let newColors = this.state.colors.slice()
                newColors[index] = color 
                this.setState({colors: newColors})  
                let newFlags = this.state.flags.slice()
                newFlags[index] = true 
                this.setState({flags: newFlags})  
            } 
        }
    }
  }
  nameChangeHandler = val =>{
    this.setState({
        name: val
    });
    };
    send(){
      this.setState({showTheThing: true}) 
        var contador=0;
        var colorIndex;
        for(i = 0; i<this.state.flags.length; i++){
            if(this.state.flags[i] == true){
                contador++;
                colorIndex = i+1;
            }
        }
        if(this.state.name != ''){
          if(contador>1){
            this.setState({showTheThing: false}) 
              Alert.alert(
                  'Kulttiva',
                  'No puedes seleccionar mas de un color',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
          }else if(contador <1){
            this.setState({showTheThing: false}) 
            Alert.alert(
              'Kulttiva',
              'Por favor selecciona un color',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
          }else{
              fetch(ip + 'users/register_farmer_crop/',{
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      lat: this.state.lat,
                      lon: this.state.lon,
                      alias: this.state.name,
                      color: colorIndex,
                      farmer_id: this.state.farmer_id,
                      crop_id: this.state.id
                    }),
              })
              .then((response) => {
                  if(response.status == 200){
                    this.setState({showTheThing: false}) 
                      this.redirectHome();
                  }else if (response.status == 404){
                    this.setState({showTheThing: false}) 
                      Alert.alert(
                          'Error',
                          'No podemos recuperar información Kulttiva',
                          [
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                          ],
                          { cancelable: false }
                        )
                  }else{
                    this.setState({showTheThing: false}) 
                      Alert.alert(
                          'Error',
                          'No se puede procesar la solicitud',
                          [
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                          ],
                          { cancelable: false }
                        )
                  }
              })
              .catch((error) => {
                this.setState({showTheThing: false}) 
                  Alert.alert(
                  'Error',
                  'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                  [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                  )
                  return;
              });
          }
        }else{
          this.setState({showTheThing: false}) 
          Alert.alert(
            'Error',
            'Por favor ingresa un nombre',
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
            )
        }
        
    }
    redirectHome(){
      Navigation.startTabBasedApp({
        tabs: [
          {
            label: 'Kultivos', // tab label as appears under the icon in iOS (optional)
            screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
            icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
            selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
            // iconInsets: { 
            //   top: 10,
            //   bottom: 10,
            //   left: 10,
            //   right: 10,
            // },
            title: 'Home', // title of the screen as appears in the nav bar (optional)
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white'
            }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
            navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
          },
          {
            label: 'Biblioteca',
            screen: 'Kultiva.Articles',
            icon: require('../assets/Images/iconBiblioteca.png'),
            selectedIcon: require('../assets/Images/iconBiblioteca.png'),
            title: 'Biblioteca',
            // iconInsets: { 
            //   top: 10,
            //   bottom: 10,
            //   left: 10,
            //   right: 10,
            // },
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white'
            },  
          },
          {
            label: 'Tienda',
            screen: 'Kultiva.Store',
            icon: require('../assets/Images/box.png'),
            selectedIcon: require('../assets/Images/box.png'),
            title: 'Tienda',
            // iconInsets: { 
            //   top: 0,
            //   bottom: 0,
            //   left: 0,
            //   right: 0,
            // },
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarHidden: true
            },  
          },
          {
            label: 'Ajustes',
            screen: 'Kultiva.Logout',
            icon: require('../assets/Images/iconSettings.png'),
            selectedIcon: require('../assets/Images/iconSettings.png'),
            title: 'Ajustes',
            // iconInsets: { 
            //   top: 10,
            //   bottom: 10,
            //   left: 10,
            //   right: 10,
            // },
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white'
            },  
          }
        ],
        appStyle: {
          tabBarSelectedButtonColor: '#8EB84A',
          tabFontSize: 10,
          selectedTabFontSize: 12,
        },
        tabsStyle: {
          tabBarSelectedButtonColor: '#8EB84A',
          selectedTabFontSize: 12,
        },
        passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
        animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
      });
    }
    render() {
      var s = require('../Styles/StyleKulttivoName');
        return (
          <View style = {s.container}>
          <StatusBar barStyle="light-content"/>
            <View style = {s.top}>
                <Text style = {s.title}>Nombra tu Kulttivo</Text>
                <Text style = {s.subtitles}>Escoge un nombre para identificar tu Kulttivo.</Text>
                <TextInput
                    style={s.input}
                    placeholder={'Ej. Mis Rabanitos'}
                    underlineColorAndroid= 'rgba(0,0,0,0)'
                    onChangeText ={this.nameChangeHandler}
                    value={this.state.name}
                    returnKeyType='done'
                />
            </View>
            { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
            <View style = {s.bottom}>
                <Text style = {s.subtitles}>Selecciona un color identificador</Text>
                <View style={s.circlesContainer}>
                    <CircleIdentifier onPress={() => this.changeStateElements(0, '#D0021B')} borderColor={'#D0021B'} backGround = {this.state.colors[0]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(1, '#F5A623')} borderColor={'#F5A623'} backGround = {this.state.colors[1]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(2, '#F8E71C')} borderColor={'#F8E71C'} backGround = {this.state.colors[2]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(3, '#8B572A')} borderColor={'#8B572A'} backGround = {this.state.colors[3]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(4, '#7ed321')} borderColor={'#7ed321'} backGround = {this.state.colors[4]} marginTop = {5} marginLeft = {'12%'}/>
                </View>
                <View style={s.circlesContainer}>
                    <CircleIdentifier onPress={() => this.changeStateElements(5, '#417505')} borderColor={'#417505'} backGround = {this.state.colors[5]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(6, '#BD10E0')} borderColor={'#BD10E0'} backGround = {this.state.colors[6]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(7, '#9013FE')} borderColor={'#9013FE'} backGround = {this.state.colors[7]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(8, '#4A90E2')} borderColor={'#4A90E2'} backGround = {this.state.colors[8]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(9, '#50E3C2')} borderColor={'#50E3C2'} backGround = {this.state.colors[9]} marginTop = {5} marginLeft = {'12%'}/>
                </View>
                <View style={s.circlesContainer}>
                    <CircleIdentifier onPress={() => this.changeStateElements(10, '#B8E986')}borderColor={'#B8E986'} backGround = {this.state.colors[10]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(11, '#000000')} borderColor={'#000000'} backGround = {this.state.colors[11]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(12, '#47759F')} borderColor={'#47759F'} backGround = {this.state.colors[12]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(13, '#F19600')} borderColor={'#F19600'} backGround = {this.state.colors[13]} marginTop = {5} marginLeft = {'12%'}/>
                    <CircleIdentifier onPress={() => this.changeStateElements(14, '#F19600')} borderColor={'#F19600'} backGround = {this.state.colors[14]} marginTop = {5} marginLeft = {'12%'}/>
                </View>
                <ButtonDark onPress = {() => this.send()} style = {s.buttonDark} name = {'Registrar Kulttivo'}/>
            </View>
          </View>
          
          );
    }
}
export default KulttivoName;