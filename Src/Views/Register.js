import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator} from 'react-native';
import Button from 'react-native-button';
// import PropTypes from 'prop-types';
import ip from '../Config/AppConfig';
import Input from '../Components/Input/Input';
class Register extends Component{
    state = {
        name: '',
        mail: '',
        password: '',
        passconfirm: '',
    };
    _seeTerms = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Terms', 
            title: 'Terms',
            navigatorStyle: {
              navBarHidden: true
            }
        });
    }
    _seeLogin = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Login', 
            title: 'Terms',
            navigatorStyle: {
              navBarHidden: true
            }
        });
    }
    _register = (mail, password, name, passconfirm) =>{
        this.setState({showTheThing: true}) 
        if(mail != '' && password != '' && name != '' && passconfirm != '' ){
            debugger;
            if(password.length > 6){
                if(password === passconfirm){
                    fetch(ip + 'users/register/',{
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                          },
                          body: JSON.stringify({
                            email: mail,
                            password: password,
                            name: name
                          }),
                    })
                    .then((response) => {
                        if(response.status == 200){
                            this.setState({showTheThing: false}) 
                            data = JSON.parse(response._bodyText);
                            // this.props.navigator.resetTo({
                            //     screen: 'Kultiva.Login', 
                            //     title: 'Login',
                            //     passProps: {farmer_id: data.farmer_id},
                            //     navigatorStyle: {
                            //         navBarHidden: true
                            //       },
                            // });
                            this.props.navigator.resetTo({
                                screen: 'Kultiva.CheckAccount', 
                                title: 'Login',
                                navigatorStyle: {
                                    navBarHidden: true
                                  },
                            });
                        }else if(response.status == 302){
                            this.setState({showTheThing: false}) 
                            Alert.alert(
                                'Error',
                                'Correo ya registrado',
                                [
                                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                                ],
                                { cancelable: false }
                              )
                        }else if (response.status == 404){
                            this.setState({showTheThing: false}) 
                            Alert.alert(
                                'Error',
                                'No podemos recuperar información Kulttiva',
                                [
                                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                                ],
                                { cancelable: false }
                              )
                        }else{
                            this.setState({showTheThing: false}) 
                            Alert.alert(
                                'Error',
                                'No se puede procesar la solicitud',
                                [
                                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                                ],
                                { cancelable: false }
                              )
                        }
                    })
                    .catch((error) => {
                        this.setState({showTheThing: false}) 
                        Alert.alert(
                        'Error',
                        'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                        [
                            {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                        )
                        return;
                    });
                }else{
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'Las contraseñas no coinciden',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }
            }else{
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'La contraseña debe tener mas de 6 caracteres',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
 
        }else{
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'Por favor ingresa todo los campos',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
    };
    mailChangeHandler = val =>{
        console.log(val);
        this.setState({
            mail: val
        });
    };
    passwordChangeHandler = val =>{
        this.setState({
            password: val
        });
    };
    nameChangeHandler = val =>{
        this.setState({
            name: val
        });
    };
    passConfChangeHandler = val =>{
        this.setState({
            passconfirm: val
        });
    };
    render(){
        var s = require('../Styles/StyleRegister');
        return(
            <View style={s.container}>
                <View style= {s.header}>
                    <Text style={s.titleBlack}>BIENVENIDO A</Text>
                    <Text style={s.titleCopper}>KULTTIVA</Text>
                    <Text style = {s.txtContent}>Llena los siguientes campos para registrarte vía Email.</Text>
                </View>
                <View style = {s.medium}>
                <Input
                    style={s.viewContainerComponent}
                    name={"ios-person-outline"}
                    placeholder={'Nombre Completo'}
                    handler={this.nameChangeHandler}
                    value={this.state.name}
                    spell={false}
                    autoCapitalize={'words'}  
                    />
                <Input
                    style={s.viewContainerComponent}
                    name={"ios-mail-outline"}
                    placeholder={'E-mail'}
                    handler={this.mailChangeHandler}
                    value={this.state.mail}
                    spell={false}
                    autoCapitalize={'none'}  
                    keyboardType={'email-address'}    
                    />
                <Input
                    style={s.viewContainerComponent}
                    name={"ios-lock-outline"}
                    placeholder={'Contraseña'}
                    handler={this.passwordChangeHandler}
                    value={this.state.password}
                    secure={true}      
                    />
                <Input
                    style={s.viewContainerComponent}
                    name={"ios-lock-outline"}
                    placeholder={'Confirmar Contraseña'}
                    handler={this.passConfChangeHandler}
                    value={this.state.passconfirm}
                    secure={true}       
                    />
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
                 <Button
                    onPress={() => this._register(this.state.mail, this.state.password, this.state.name, this.state.passconfirm)}
                    containerStyle={s.btnBlack}
                    style={{fontSize: 20, color: 'white', paddingTop: 10,}}
                >Crear Cuenta</Button>
                <TouchableOpacity style = {s.txtcontainer} onPress={() => this._seeLogin()}>
                    <Text style = {s.txtGreen}>¿Ya tienes cuenta? haz tap aquí para iniciar sesión</Text>
                </TouchableOpacity>
                </View>

                <TouchableOpacity style={s.footer}  onPress={() => this._seeTerms()}>
                        <Image
                         style={s.imgFooter}
                        source={require('../Img/footerImg.png')}
                        />
                        <Text style= {s.txtFooter}>Revisa términos y condiciones</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
export default Register;