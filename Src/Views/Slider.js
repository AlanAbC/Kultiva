import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, Image, ScrollView, StyleSheet, ActivityIndicator } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import OneSignal from 'react-native-onesignal'; 
import ip from '../Config/AppConfig';
const slides = [
    {
      key: 'somethun',
      title: 'CREA TU PROPIO HUERTO ',
      text: 'Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.',
      image: require('../Img/footerImgWhite.png'),
      imageStyle: {width: 200,height: 28},
      titleStyle: {width: '80%', textAlign: 'center'},
      source: require('../Img/slider.jpg')
    },
    {
      key: 'somethun1',
      title: 'CREA TU PROPIO HUERTO ',
      text: 'Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.',
      image: require('../Img/footerImgWhite.png'),
      imageStyle: {width: 200,height: 28},
      titleStyle: {width: '80%', textAlign: 'center'},
      source: require('../Img/slider.jpg')
    },
    {
      key: 'somethun2',
      title: 'CREA TU PROPIO HUERTO ',
      text: 'Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.',
      image: require('../Img/footerImgWhite.png'),
      imageStyle: {width: 200,height: 28},
      titleStyle: {width: '80%', textAlign: 'center'},
      source: require('../Img/slider.jpg')
    }
  ];
  
class Slider extends Component{
  state= {
    slides: []
  }
  componentWillMount() {
    OneSignal.inFocusDisplaying(2);
    OneSignal.setSubscription(false);
  }
  componentDidMount(){
    debugger;
    this.makeRemoteRequest();

  }
  _onDone(){
    this.props.navigator.resetTo({
      screen: 'Kultiva.FirstScreen', 
      title: 'Login',
      navigatorStyle: {
          navBarHidden: true
        },
  });
}
  makeSliders = (data) =>{
    debugger;
    for(i = 0; i< data.length; i++){
      var arrayvar = this.state.slides.slice()
      tempjson = {
        key: i,
        title: data[i].title,
        text: data[i].description,
        image: require('../Img/footerImgWhite.png'),
        imageStyle: {width: 200,height: 28},
        titleStyle: {width: '80%', textAlign: 'center'},
        source: require('../Img/slider.jpg')
      }
      arrayvar.push(tempjson)
      this.setState({slides: arrayvar});
    }
      
  }
  makeRemoteRequest = () => {
    debugger;
    this.setState({showTheThing: true}) 
    if(this.state.refreshing != true){
      this.setState({showTheThing: true}) 
    }
    const { page, seed } = this.state;
    const url = ip + 'about/welcome_slides/';
    console.log(url);
    debugger;
    fetch(url)
      .then(res => {
          debugger;
          console.log(res);
        if(res.status == 200){
            
          this.setState({showTheThing: false, showList: true, showEmpty: false}) 
            data = JSON.parse(res._bodyText);
            debugger;
            this.makeSliders(data);
            console.log("está entrando a un success!");
        }else if(res.status == 204){
          this.setState({showTheThing: false, slides: slides}) 

        }else if (res.status == 404){
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No podemos recuperar información Kulttiva',
                [
                  {text: 'OK', onPress: () => console.log("error", 'view_time_line_crop', res.status)},
                ],
                { cancelable: false }
              )
        }else{
          this.setState({showTheThing: false}) 
          Alert.alert(
              'Error',
              'No se puede procesar la solicitud',
              [
                {text: 'OK', onPress: () => console.log("error", 'view_time_line_crop', res.status)},
              ],
              { cancelable: false }
            )
        }
        
      })
      .catch(error => {
        this.setState({showTheThing: false, showList: false, showEmpty: true}) 
            Alert.alert(
              'Error',
              'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
              [
                {text: 'OK', onPress: () => console.log(error, 'view_time_line_crop')},
              ],
              { cancelable: false }
            )
            return;
      });
    };
   
    render(){
        
        return(
          <AppIntroSlider
          slides={this.state.slides}
          onDone={() => this._onDone()}
        />
        );
    }
}
const styles = StyleSheet.create({
    image: {
      width: 320,
      height: 320,
    }
  });
export default Slider;