import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, Image, ScrollView, ActivityIndicator} from 'react-native';
import ip from '../Config/AppConfig';
import OneSignal from 'react-native-onesignal'; 
class Terms extends Component{
    state= {
        terms: '' 
    };
    componentWillMount() {
        OneSignal.inFocusDisplaying(2);
        OneSignal.setSubscription(false);
      }
    componentDidMount(){
        this.makeRemoteRequest();
    };
    makeRemoteRequest() {
        this.setState({showTheThing: true}) 
        const url = ip + 'about/terms_and_conditions/';
        fetch(url)
          .then(res => {
            console.log(res);
            if(res.status == 200){
              this.setState({showTheThing: false})
                data = JSON.parse(res._bodyText);
                this.setState({
                    terms: data.terms_and_conditions,
                  });
            }else if (res.status == 404){
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
            
          })
          .catch(error => {
            this.setState({showTheThing: false}) 
                Alert.alert(
                  'Error',
                  'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
                return;
          });
        };
    
    render(){
        var s = require('../Styles/StyleTerms');
        return(
            <View style = {s.container}>
                <View style={s.footer}>
                <Image
                style={s.imgFooter}
                source={require('../Img/footerImg.png')}
                />
                <Text style= {s.txtFooter}>Revisa términos y condiciones</Text>
                </View>
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
                <ScrollView style = {s.scroll}>
                    <Text style={s.titleBlack}>TÉRMINOS Y</Text>
                    <Text style={s.titleCopper}>CONDICIONES</Text>
                    <Text style={s.txtContent}>{this.state.terms}</Text>
                </ScrollView>
            </View>
        );
    }
}
export default Terms;