import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage, ToastAndroid,
  AlertIOS,
  Platform, Switch, StatusBar } from 'react-native';
  import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import Share, {ShareSheet, Button} from 'react-native-share';
import ip from '../Config/AppConfig';
import OneSignal from 'react-native-onesignal'; 
export default class Notifications extends React.Component {
  state = {
    switch1Value: false,
    txt: 'Activadas'
  }
  toggleSwitch1 = (value) => {
    this.setState({switch1Value: value})
    if(value){
        this.setState({txt: 'Activadas'});
        OneSignal.setSubscription(true);
    }else{
        this.setState({txt: 'Desactivadas'});
        OneSignal.setSubscription(false);
    }
    console.log('Switch 1 is: ' + value)
 }
 componentDidMount(){
    OneSignal.getPermissionSubscriptionState((status) => {
        console.log(status);
        if(status.userSubscriptionEnabled){
            this.setState({switch1Value: true, txt: 'Activadas'});
        }else{
            this.setState({switch1Value: false, txt: 'Desactivadas'});
        }
    });
 }
  onClick() {
    Share.shareSingle(Object.assign(
      {title: "React Native",
      message: "Hola mundo",
      url: TWITTER_ICON,
      subject: "Share Link" //  for email}
     }, {
      "social": "whatsapp"}
    ))
  }
  
  render() {
    var s = require('../Styles/StyleNotifiations');
    return (
      <View style={s.container}>
      <StatusBar barStyle="light-content"/>
        <View onPress={() => this.viewLocation()}  style ={s.btn}>
          <Text style ={s.txtbtn}>{this.state.txt}</Text>
          <Switch style = {s.switch}
          onValueChange = {this.toggleSwitch1}
          value = {this.state.switch1Value}
          />
        </View>
      </View>
    );
  }
}
