import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage, ToastAndroid,
  AlertIOS,
  Platform, StatusBar } from 'react-native';
  import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import Share, {ShareSheet, Button} from 'react-native-share';
import ip from '../Config/AppConfig';
export default class Logout extends React.Component {
  state = {
    farmer_id: 0,
  }
  onClick() {
    Share.shareSingle(Object.assign(
      {title: "React Native",
      message: "Hola mundo",
      url: TWITTER_ICON,
      subject: "Share Link" //  for email}
     }, {
      "social": "whatsapp"}
    ))
  }
  viewPass(){
    this.props.navigator.push({
      screen: 'Kultiva.ChangePasword', 
      title: 'Mi Kulttivo',
      navigatorStyle: {
        navBarHidden: true,
        tabBarHidden: true
      }, 
      
     
  });
}
viewNotifications(){
  this.props.navigator.push({
    screen: 'Kultiva.Notifications', 
    title: 'Notificaciones',
    navigatorStyle: {
      tabBarHidden: true,
      navBarBackgroundColor: '#231F20',
      navBarTextColor: 'white',
      navBarButtonColor: 'white',
    }, 
    
   
});
}
componentDidMount(){
  ls.get('farmer_id').then((local) => {this.makeRemoteRequest(local)});
  ls.get('farmer_id').then((local) => {this.setState({farmer_id: local})});
}
viewLocation(){
  this.props.navigator.push({
    screen: 'Kultiva.ChangeLocation', 
    title: 'Mi Kulttivo',
    navigatorStyle: {
      navBarHidden: true,
      tabBarHidden: true
    }, 
   
});
}
_handlePress(){
  ls.remove('farmer_id');
  ls.remove('location');
  ls.remove('token');
  Navigation.startSingleScreenApp({
    screen:{
      screen: "Kultiva.FirstScreen",
      title: "Home",
      navigatorStyle: {
        navBarBackgroundColor: '#231F20',
        navBarTextColor: 'white',
        navBarHidden: true
      }
    }
  });
}
makeRemoteRequest = (farmer_id) => {
  debugger;
  const url = ip + 'users/farmer_type/'+ farmer_id+ '/';
  console.log(url);

  fetch(url)
  .then(res => {
    console.log(res);
    if(res.status == 200){
      data = JSON.parse(res._bodyText);
      if(data.is_social_network){
        this.setState({showPass: false});
      }else{
        this.setState({showPass: true});
      }
    }else if (res.status == 404){
      Alert.alert(
        'Error',
        'Servidor no disponible',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }else{
      Alert.alert(
        'Error',
        'Servidor no disponible',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }
  })
  .catch(error => {
    Alert.alert(
      'Error',
      'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
          return;
    });
  };
  render() {
    var s = require('../Styles/StyleLogout');
    return (
      <View style={s.container}>
      <StatusBar barStyle="light-content"/>
        <TouchableOpacity onPress={() => this.viewLocation()}  style ={s.btn}>
          <Icon
              style={s.icon}
              size={25}
              name={"ios-navigate-outline"}
              color='#2e8232'
            />
          <Text style ={s.txtbtn}>Cambiar Ubicacion</Text>
        </TouchableOpacity>
        { this.state.showPass && 
        <TouchableOpacity onPress={() => this.viewPass()}  style ={s.btn}>
          <Icon
              style={s.icon}
              size={25}
              name={"ios-lock-outline"}
              color='#2e8232'
            />
          <Text style ={s.txtbtn}>Cambiar Contraseña</Text>
        </TouchableOpacity>
        }
        <TouchableOpacity onPress={() => this.viewNotifications()}  style ={s.btn}>
          <Icon
              style={s.icon}
              size={25}
              name={"ios-notifications-outline"}
              color='#2e8232'
            />
          <Text style ={s.txtbtn}>Notificaciones</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this._handlePress()} style ={s.btn}>
          <Icon
              style={s.icon}
              size={25}
              name={"ios-exit-outline"}
              color='#2e8232'
            />
          <Text style ={s.txtbtn}>Logout</Text>
        </TouchableOpacity>
        <View style={s.version}><Text>Version 1.0.4.3 Released 16 April 2018</Text></View>
      </View>
    );
  }
}
const TWITTER_ICON = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAABvFBMVEUAAAAA//8AnuwAnOsAneoAm+oAm+oAm+oAm+oAm+kAnuwAmf8An+0AqtUAku0AnesAm+oAm+oAnesAqv8An+oAnuoAneoAnOkAmOoAm+oAm+oAn98AnOoAm+oAm+oAmuoAm+oAmekAnOsAm+sAmeYAnusAm+oAnOoAme0AnOoAnesAp+0Av/8Am+oAm+sAmuoAn+oAm+oAnOoAgP8Am+sAm+oAmuoAm+oAmusAmucAnOwAm+oAmusAm+oAm+oAm+kAmusAougAnOsAmukAn+wAm+sAnesAmeoAnekAmewAm+oAnOkAl+cAm+oAm+oAmukAn+sAmukAn+0Am+oAmOoAmesAm+oAm+oAm+kAme4AmesAm+oAjuMAmusAmuwAm+kAm+oAmuoAsesAm+0Am+oAneoAm+wAmusAm+oAm+oAm+gAnewAm+oAle0Am+oAm+oAmeYAmeoAmukAoOcAmuoAm+oAm+wAmuoAneoAnOkAgP8Am+oAm+oAn+8An+wAmusAnuwAs+YAmegAm+oAm+oAm+oAmuwAm+oAm+kAnesAmuoAmukAm+sAnukAnusAm+oAmuoAnOsAmukAqv9m+G5fAAAAlHRSTlMAAUSj3/v625IuNwVVBg6Z//J1Axhft5ol9ZEIrP7P8eIjZJcKdOU+RoO0HQTjtblK3VUCM/dg/a8rXesm9vSkTAtnaJ/gom5GKGNdINz4U1hRRdc+gPDm+R5L0wnQnUXzVg04uoVSW6HuIZGFHd7WFDxHK7P8eIbFsQRhrhBQtJAKN0prnKLvjBowjn8igenQfkQGdD8A7wAAAXRJREFUSMdjYBgFo2AUDCXAyMTMwsrGzsEJ5nBx41HKw4smwMfPKgAGgkLCIqJi4nj0SkhKoRotLSMAA7Jy8gIKing0KwkIKKsgC6gKIAM1dREN3Jo1gSq0tBF8HV1kvax6+moG+DULGBoZw/gmAqjA1Ay/s4HA3MISyrdC1WtthC9ebGwhquzsHRxBfCdUzc74Y9UFrtDVzd3D0wtVszd+zT6+KKr9UDX749UbEBgULIAbhODVHCoQFo5bb0QkXs1RAvhAtDFezTGx+DTHEchD8Ql4NCcSyoGJYTj1siQRzL/JKeY4NKcSzvxp6RmSWPVmZhHWnI3L1TlEFDu5edj15hcQU2gVqmHTa1pEXJFXXFKKqbmM2ALTuLC8Ak1vZRXRxa1xtS6q3ppaYrXG1NWjai1taCRCG6dJU3NLqy+ak10DGImx07LNFCOk2js6iXVyVzcLai7s6SWlbnIs6rOIbi8ViOifIDNx0uTRynoUjIIRAgALIFStaR5YjgAAAABJRU5ErkJggg==";