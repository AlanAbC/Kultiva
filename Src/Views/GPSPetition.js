import React, {Component} from 'react';
import {View, Text, TextInput, Alert, AsyncStorage, ImageBackground, TouchableOpacity, Image, ActivityIndicator, StatusBar} from 'react-native';
import Button from 'react-native-button';
// import PropTypes from 'prop-types';
import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import ip from '../Config/AppConfig';
import BtnIconBlack from '../Components/BtnIcon/BtnIconBlack';
import BtnIconFacebook from '../Components/BtnIcon/BtnIconFacebook';
import Video from 'react-native-video';
import Geolocation from 'react-native-geolocation-service';
import {Navigation} from 'react-native-navigation';
import ls from 'react-native-local-storage';
class GPSPetition extends Component{
    state = {
        farmer_id: this.props.farmer_id,
        latitude: '',
        longitude: '',
    };
    componentDidMount() {
        Geolocation.getCurrentPosition(
            (position) => {
                console.log(position);
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
            },
            (error) => {
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
        // Instead of navigator.geolocation, just use Geolocation.
        
        this.showAlert();
        ls.get('farmer_id').then((data) => {this.setState({farmer_id: data})});
    }
    showAlert(){
      Alert.alert(
        '¿Usar Localización?',
        'Esta aplicación desea cambiar algunos ajustes:',
        [
          {text: 'No', onPress: () => this._handlePress(), style: 'cancel'},
          {text: 'Si', onPress: () => this.sendLocation(this.state.latitude, this.state.longitude)},
        ],
        { cancelable: false }
      )
    }
    _handlePress(){
      ls.remove('farmer_id');
      ls.remove('location');
      Navigation.startSingleScreenApp({
        screen:{
          screen: "Kultiva.FirstScreen",
          title: "Home",
          navigatorStyle: {
            navBarBackgroundColor: '#231F20',
            navBarTextColor: 'white',
            navBarHidden: true
          }
        }
      });
    }
    sendLocation = (latitude, longitude) =>{
      debugger;
        this.setState({showTheThing: true}) 
        fetch(ip + 'users/register_location_farmer/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                farmer_id: this.state.farmer_id,
                lon: longitude,
                lat: latitude
              }),
        })
        .then((response) => {
            console.log(response);
            if(response.status == 200){
                this.setState({showTheThing: false}) 
                ls.save('location', true);
                Navigation.startTabBasedApp({
                    tabs: [
                      {
                        label: 'Kulttivos', // tab label as appears under the icon in iOS (optional)
                        screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                        icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                        selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                        // iconInsets: { 
                        //   top: 10,
                        //   bottom: 10,
                        //   left: 10,
                        //   right: 10,
                        // },
                        title: 'Home', // title of the screen as appears in the nav bar (optional)
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white'
                        }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                        navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                      },
                      {
                        label: 'Biblioteca',
                        screen: 'Kultiva.Articles',
                        icon: require('../assets/Images/iconBiblioteca.png'),
                        selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                        title: 'Biblioteca',
                        // iconInsets: { 
                        //   top: 10,
                        //   bottom: 10,
                        //   left: 10,
                        //   right: 10,
                        // },
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white'
                        },  
                      },
                      {
                        label: 'Tienda',
                        screen: 'Kultiva.Store',
                        icon: require('../assets/Images/box.png'),
                        selectedIcon: require('../assets/Images/box.png'),
                        title: 'Tienda',
                        // iconInsets: { 
                        //   top: 0,
                        //   bottom: 0,
                        //   left: 0,
                        //   right: 0,
                        // },
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white',
                          navBarHidden: true
                        },  
                      },
                      {
                        label: 'Ajustes',
                        screen: 'Kultiva.Logout',
                        icon: require('../assets/Images/iconSettings.png'),
                        selectedIcon: require('../assets/Images/iconSettings.png'),
                        title: 'Ajustes',
                        // iconInsets: { 
                        //   top: 10,
                        //   bottom: 10,
                        //   left: 10,
                        //   right: 10,
                        // },
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white'
                        },  
                      }
                    ],
                    appStyle: {
                      tabBarSelectedButtonColor: '#8EB84A',
                      tabFontSize: 10,
                      selectedTabFontSize: 12,
                    },
                    tabsStyle: {
                      tabBarSelectedButtonColor: '#8EB84A',
                      selectedTabFontSize: 12,
                    },
                    passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                    animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                  });
            }else if (response.status == 404){
              this._handlePress();
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
              this._handlePress();
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
          this._handlePress();
            this.setState({showTheThing: false}) 
            Alert.alert(
            'Error',
            'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
            )
            return;
        });
    };
    render(){
        var _this = this;
        var s = require('../Styles/StyleGPSPetition');
        return(
            <ImageBackground style = {s.backgroundImage} source={require('../Img/slider.jpg')}>
             <StatusBar barStyle="light-content"/>
                <Text style = {s.titleText}>¿PARA QUÉ USAMOS TU UBICACIÓN?</Text>
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="white" />
                }
                <Image source={require('../Img/footerImgWhite.png')} style = {s.imgIcon}/>
                <Text style = {s.txtBottom}>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo.</Text>
            </ImageBackground>
        );
    }
}
export default GPSPetition;