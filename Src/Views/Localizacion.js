import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, FlatList, ScrollView, StyleSheet, StatusBar} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ListItemNewKultivo from '../Components/ListItemNewKultivo/ListItemNewKultivo';
import ButtonDark from '../Components/ButtonDark/ButtonDark';
import MapView from 'react-native-maps';
  
const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: 400,
      width: 400,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
  });
  
  export default class Localizacion extends React.Component {
    render() {
      const { region } = this.props;
  
      return (
        <View style ={styles.container}>
        <StatusBar barStyle="light-content"/>
          <MapView
            style={styles.map}
            initialRegion={{
                latitude: 20.588278,
                longitude: -100.392499,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
          >
            <MapView.Marker
            coordinate={{latitude: 20.588278,
            longitude: -100.392499}}
            title={"Kulltivo"}
            description={"Selecciona tu ubicacion"}
         />
          </MapView>
        </View>
      );
    }
  }