import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage, StatusBar } from 'react-native';
import Button from '../Components/ButtonDark/ButtonDark';
import ip from '../Config/AppConfig';
import Input from '../Components/Input/Input';
import BtnIconFacebook from '../Components/BtnIcon/BtnIconFacebook';
// import PropTypes from 'prop-types';
import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
export default class App extends React.Component {
  
  state = {
    email: '',
    password: '',
    location: false
  };
  
  _login = (email, password) => {
    this.setState({showTheThing: true}) 
    if(email != '' && password != ''){
        fetch(ip + 'login/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                email: email,
                password: password,
              }),
        })
        .then((response) => {
            if(response.status == 200){
                this.setState({showTheThing: false}) 
                data = JSON.parse(response._bodyText);
                ls.save('farmer_id', data.farmer_id);
                if(data.location){
                  Navigation.startTabBasedApp({
                    tabs: [
                      {
                        label: 'Kultivos', // tab label as appears under the icon in iOS (optional)
                        screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                        icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                        selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                        title: 'Home', // title of the screen as appears in the nav bar (optional)
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white'
                        }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                        navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                      },
                      {
                        label: 'Biblioteca',
                        screen: 'Kultiva.Articles',
                        icon: require('../assets/Images/iconBiblioteca.png'),
                        selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                        title: 'Biblioteca',
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white'
                        },  
                      },
                      {
                        label: 'Tienda',
                        screen: 'Kultiva.FirstScreen',
                        icon: require('../assets/Images/box.png'),
                        selectedIcon: require('../assets/Images/box.png'),
                        title: 'Tienda',
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white'
                        }, // 
                      },
                      {
                        label: 'Ajustes',
                        screen: 'Kultiva.Logout',
                        icon: require('../assets/Images/iconSettings.png'),
                        selectedIcon: require('../assets/Images/iconSettings.png'),
                        title: 'Ajustes',
                        navigatorStyle: {
                          navBarBackgroundColor: '#231F20',
                          navBarTextColor: 'white'
                        }, // 
                      }
                    ],
                    appStyle: {
                      tabBarSelectedButtonColor: '#8EB84A',
                      tabFontSize: 10,
                      selectedTabFontSize: 12,
                    },
                    passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                    animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                  });
                }else{
                  this.props.navigator.resetTo({
                    screen: 'Kultiva.GPSPetitiion', 
                    title: 'Login',
                    passProps: {farmer_id: data.farmer_id},
                    navigatorStyle: {
                        navBarHidden: true
                      },
                });
                }
            }else if(response.status == 404){
              this.setState({showTheThing: false}) 
              data = JSON.parse(response._bodyText);
              console.log(data)
                Alert.alert(
                    'Error',
                    'El usuario no existe',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else if (response.status == 401){
              this.setState({showTheThing: false})
              data = JSON.parse(response._bodyText); 
                Alert.alert(
                    'Error',
                    'El usuario no está activo',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
            this.setState({showTheThing: false}) 
            Alert.alert(
              'Error',
              'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
        });
    }else{
      this.setState({showTheThing: false}) 
        Alert.alert(
            'Error',
            'Por favor ingresa todo los campos',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }
    };
    emailChangeHandler = val =>{
        this.setState({
            email: val
        });
    };
    passwordChangeHandler = val =>{
        this.setState({
            password: val
        });
    };
    _seeTerms = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Terms', 
            title: 'Terms',
            navigatorStyle: {
              navBarHidden: true
            }
        });
  };
  _seeReset = () => {
    this.props.navigator.push({
        screen: 'Kultiva.ResetPassword', 
        title: 'Terms',
        navigatorStyle: {
          navBarHidden: true
        }
    });
};
  render() {
    var s = require('../Styles/StyleCheckAcc');
    return (
      <View style={s.container}>
      <StatusBar barStyle="light-content"/>
      <View style={s.titleContent}>
        <Text style = {s.titleBlack}>VERIFICA TU</Text>
        <Text style = {s.titleCopper}>CUENTA</Text>
        { this.state.showTheThing && 
          <ActivityIndicator size="large" color="green" />
        }
        <Text style={s.subtitle}>Te hemos enviado un link a tu correo electrónico para verificar tu cuenta. Haz click en el link y regresa a tu App para iniciar sesión</Text>
      </View>
      <View style={s.inputsContainer}>
        <Input
          style={s.viewContainerComponent}
          name={"ios-mail-outline"}
          placeholder={'E-mail'}
          spell={false}
          value={this.state.email}
          handler={this.emailChangeHandler}  
          keyboardType={'email-address'}  
          autoCapitalize={'none'}  
          />
        <Input style={s.viewContainerComponent}
          name={'ios-lock-outline'}
          placeholder={"Contraseña"}
          secure={true}
          value={this.state.password}
          handler={this.passwordChangeHandler}
          />
          <Button
              style={s.viewContainerButton}
              name={'Iniciar sesión'}
              onPress={() => this._login(this.state.email, this.state.password)}
          />
          <TouchableOpacity onPress = {() => this._seeReset()}><Text style={s.textLight}>
             Olvidé mi Contraseña o Email
         </Text></TouchableOpacity>
      </View>
      <TouchableOpacity style={s.footer}  onPress={() => this._seeTerms()}>
            <Image
                style={s.imgFooter}
                source={require('../Img/footerImg.png')}
            />
            <Text style= {s.txtFooter}>Revisa términos y condiciones</Text>
        </TouchableOpacity>
      </View>
    );
  }
}