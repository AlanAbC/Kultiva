import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage, StatusBar } from 'react-native';
import Button from 'react-native-button';
import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import Input from '../Components/Input/Input';
import Icon from 'react-native-vector-icons/Ionicons';
import ip from '../Config/AppConfig';
export default class ChangePassword extends React.Component {
    state = {
        oldpassword: '',
        password: '',
        passconfirm: '',
        farmer_id: 0
    };
    _handlePress(){
        ls.remove('farmer_id');
        ls.remove('location');
        Navigation.startSingleScreenApp({
          screen:{
            screen: "Kultiva.FirstScreen",
            title: "Home",
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarHidden: true
            }
          }
        });
    }
    componentDidMount(){
        console.disableYellowBox = true;
        ls.get('farmer_id').then((local) => {this.setState({farmer_id: local})});
    }
    changePass(){ 
        if(this.state.password != '' && this.state.oldpassword != '' && this.state.passconfirm != ''){
            if(this.state.password === this.state.passconfirm){
                fetch(ip + 'users/change_password/',{
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        farmer_id: this.state.farmer_id,
                        old_password: this.state.oldpassword,
                        new_password: this.state.password
                      }),
                })
                .then((response) => {
                  debugger;
                    if(response.status == 200){
                        ls.remove('farmer_id');
                        ls.remove('location');
                        Navigation.startSingleScreenApp({
                            screen:{
                            screen: "Kultiva.Login",
                            title: "Home",
                            navigatorStyle: {
                                navBarBackgroundColor: '#231F20',
                                navBarTextColor: 'white',
                                navBarHidden: true
                            }
                            }
                        });
                    }else if(response.status == 404){
                      this.setState({showTheThing: false}) 
                      data = JSON.parse(response._bodyText);
                      console.log(data)
                        Alert.alert(
                            'Error',
                            'No podemos recuperar información Kulttiva',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    }else if (response.status == 401){
                      this.setState({showTheThing: false})
                        Alert.alert(
                            'Error',
                            'Contraseña actual incorrecta',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    }else{
                      this.setState({showTheThing: false}) 
                        Alert.alert(
                            'Error',
                            'No se puede procesar la solicitud',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    }
                })
                .catch((error) => {
                  debugger;
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                      'Error',
                      'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                      [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                    )
                    return;
                });
            }else{
                Alert.alert(
                    'Error',
                    'Las contraseñas no coinciden',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
            
        }else{
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'Por favor ingresa todo los campos',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
            }
        };
    oldpasswordChangeHandler = val =>{
        this.setState({
            oldpassword: val
        });
    };
    passwordChangeHandler = val =>{
        this.setState({
            password: val
        });
    };
    passwordConfChangeHandler = val =>{
        this.setState({
            passconfirm: val
        });
    };
  render() {
    var s = require('../Styles/StyleChangePassword');
    return (
      <View style={s.container}>
      <StatusBar barStyle="light-content"/>
        <Text style ={s.title}>CAMBIAR</Text>
        <Text style={s.titleCopper}>CONTRASEÑA</Text>
        <Text style ={s.subtitle}>Por favor proporcionanos tu contraseña actual y la nueva contraseña</Text>
        <Input
        style={s.viewContainerComponent1}
        name={"ios-lock-outline"}
        placeholder={'Contraseña Actual'}
        handler={this.oldpasswordChangeHandler}
        value={this.state.oldpassword}
        secure={true}      
        />
        <Input
        style={s.viewContainerComponent1}
        name={"ios-lock-outline"}
        placeholder={'Nueva Contraseña'}
        handler={this.passwordChangeHandler}
        value={this.state.password}
        secure={true}      
        />
        <Input
        style={s.viewContainerComponent2}
        name={"ios-lock-outline"}
        placeholder={'Confirmar Contraseña'}
        handler={this.passwordConfChangeHandler}
        value={this.state.passconfirm}
        secure={true}      
        />
        <Button
            onPress= {()=>this.changePass()}
            containerStyle={s.btnBlack}
            style={{fontSize: 20, color: 'white', paddingTop: 10,}}
        >Aceptar</Button>
      </View>
    );
  }
}