import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage } from 'react-native';
import Button from 'react-native-button';
import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
export default class Profile extends React.Component {
    _handlePress(){
        ls.remove('farmer_id');
        ls.remove('location');
        Navigation.startSingleScreenApp({
          screen:{
            screen: "Kultiva.FirstScreen",
            title: "Home",
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarHidden: true
            }
          }
        });
    }
  render() {
    var s = require('../Styles/StyleProfile');
    return (
      <View style={s.container}>
        <View style = {s.inputContainer}>
            <Text style= {s.txtInput}>Nombre</Text>
            <TextInput
                style={s.input}
                placeholder={"Alan Abundis"}
                autoCapitalize={this.props.autoCapitalize}
                spellCheck={this.props.spell}
                underlineColorAndroid= 'rgba(0,0,0,0)'
                keyboardType={this.props.keyboardType}
            />
        </View>
        
      </View>
    );
  }
}