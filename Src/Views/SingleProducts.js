import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {Linking, View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, ScrollView, ImageBackground, StyleSheet, TouchableWithoutFeedback, StatusBar, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import IconVideo from "react-native-vector-icons/FontAwesome";
import ip from '../Config/AppConfig';
import HTML from 'react-native-render-html';
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel';

  function secondsToTime(time) {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
  }
  // const datacarousel = [];
class Home extends Component {
  
    state = {
        paused: true,
        progress: 0,
        duration: 0,
        title: '',
        img: 'https://www.iconfinder.com/icons/170730/clock_loading_refresh_reload_slow_throbber_time_update_wait_waiting_icon',
        content: '<p>Loading</p>',
        datacarousel: [],
        storeUrl: 'https://google.com',
        day: this.props.day
    }
    generateData(data) {

      datacarouselTemp = [];
      i = 3786;
      for(let task of data){
        jsonTemp = {id: "" + i + "",
        imagePath: task};
        datacarouselTemp.push(jsonTemp);
        i++;
      }
      console.log(data.length > 0)
      if (data.length > 0) {
        debugger;
        this.setState({
          datacarousel: datacarouselTemp,
        })
      }else{
        debugger;
        
        jsonTemp1 = {id: "1",
        imagePath: "http://tickets.iwsandbox.com:9000/static/img/logo_kulttiva.png"};
        datacarouselTemp.push(jsonTemp1);
        this.setState({
          datacarousel: datacarouselTemp
        })
      }
    }
    componentDidMount(){
        this.makeRemoteRequest();
        if(this.props.day === "Día 0") {
          this.setState({
            day: 'Requerimiento'
          })
        }
    }
    makeRemoteRequest(){
      console.log("remote request")
        this.setState({showTheThing: true}) 
        const url = ip + 'crops/view_single_task/'+ this.props.id+'/';
        console.log(url)
        this.setState({ loading: true});
        fetch(url)
          .then(res => {
            if(res.status == 200){
              
              console.log('request succeded')
              this.setState({showTheThing: false}) 
                data = JSON.parse(res._bodyText);
                console.log(data.image.length, 'tamaño del arreglo de imagenes')
                this.setState({title: data.title, img: data.image, content: data.content, storeUrl: data.link_store});
                if(data.include_store && data.link_store != null){
                  this.setState({showStore: true});
                }else{
                  this.setState({showStore: false});
                }
      
                this.generateData(data.image);
            }else if(res.status == 204){
                this.setState({showTheThing: false}) 
    
            }else if (res.status == 404) {
                this.setState({showTheThing: false})  
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
              this.setState({showTheThing: false})  
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
            
          })
          .catch(error => {
  
            console.log('error petition')
            this.setState({showTheThing: false})  
                Alert.alert(
                  'Error',
                  'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
                return;
          });
    }
    handleMainButtonTouch = () => {
        if (this.state.progress >= 1) {
          this.player.seek(0);
        }
        this.setState(state => {
          return {
            paused: !state.paused,
          };
        });
    };
    handleProgressPress = e => {
        const position = e.nativeEvent.locationX;
        const progress = (position / 250) * this.state.duration;
        const isPlaying = !this.state.paused;
        
        this.player.seek(progress);
      };
    
      handleProgress = progress => {
        this.setState({
          progress: progress.currentTime / this.state.duration,
        });
      };
    
      handleEnd = () => {
        this.setState({ paused: true });
      };
    
      handleLoad = meta => {
        this.setState({
          duration: meta.duration,
        });
        this.setState({showLoadingOverlay: true})
      };
      _playVideo = () => {
        // if (this.state.shouldPlay) {
            this.setState({showTheThing: true}) ;
            this.setState({paused: false}); 
        // }else{
        //     this.setState({showTheThing: false}) ;
        //     Alert.alert(
        //         'Error',
        //         this.state.dataErrorMessage,
        //         [
        //             {text: 'OK', onPress: () => console.log('OK Pressed')},
        //         ],
        //             { cancelable: false }
        //         )
        // }
           
    }
    render() {
      var s = require('../Styles/StyleSingle');
        return (
            <ScrollView style={s.container}>
            { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
             <StatusBar
                barStyle="light-content"
              />
              <View style = {s.topElements}>
                <Text style = {s.titleBlack}>{this.state.title}</Text>
                <Text style = {s.titleDays}>{this.state.day}</Text>
                <View style={s.imageProduct} >
                  <SwipeableParallaxCarousel data={this.state.datacarousel} parallax={false} navigationType='dots' navigationColor='#5A6872' navigation={true}/>
                </View>
              </View>
              <View style = {s.bottom}>
                <View  style = {s.bottomContainer}>
                    { this.state.showStore && 
                    <TouchableOpacity onPress={ ()=>{ Linking.openURL(this.state.storeUrl)}} style={s.buyButton}>
                    <Icon style={s.Icon}
                    size={20}
                    name={'ios-cube-outline'}
                    color='#000'
                    />
                    <Text style={s.textButton}>Comprar</Text>
                    </TouchableOpacity>
                    }
                    <Text style={s.textDescriptionTitle}>Descripción General</Text>
                    <View style = {s.htmlcontainer}><HTML html={this.state.content} imagesMaxWidth={Dimensions.get('window').width} /></View>
                </View>
              </View>
          </ScrollView>
          
          );
    }
}
export default Home;