import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, FlatList} from 'react-native';
import { List, ListItem, SearchBar, Card } from "react-native-elements";
import Menu from 'react-native-pop-menu';
import Icon from 'react-native-vector-icons/Ionicons';
import SingleArticle from '../Components/Articles/SingleArticle';
import ip from '../Config/AppConfig';
import ls from 'react-native-local-storage';
class Articles2 extends Component {
    state = {
      menuVisible: false,
      colors: ['#D0021B', '#F5A623', '#F8E71C', '#8B572A', '#7ed321', '#417505', '#BD10E0', '#9013FE', '#4A90E2', '#50E3C2', '#B8E986', '#000000', '#47759F', '#F19600', '#F19600'],
      loading: false,
      data: [],
      schedules: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      farmer_id: 0
  };
  singleView(id){
    this.props.navigator.push({
        screen: 'Kultiva.SingleArticle', 
        title: 'Biblioteca',
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white',
          navBarButtonColor: 'white',
          tabBarHidden: true
        }, 
        passProps: {
            id: id
        }
      });
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest() {
    if(this.state.refreshing != true){
      this.setState({showTheThing: true}) 
    }
    const { page, seed } = this.state;
    const url = ip + 'contents/view_all_articles_category/'+ this.props.id + '/';
    this.setState({ loading: true});
    fetch(url)
      .then(res => {
        console.log(res);
        if(res.status == 200){
          this.setState({showTheThing: false, showList: true}) 
            data = JSON.parse(res._bodyText);
            console.log(data);
            this.setState({
                data: data,
                loading: false,
                refreshing: false
              });
        }else if(res.status == 204){
          this.setState({showTheThing: false, showList: false, showEmpty: true}) 

        }else if (res.status == 404){
          //No podemos recuperar informacion kulttiva
          this.setState({showTheThing: false, showList: false}) 
            Alert.alert(
                'Error',
                'No podemos recuperar información Kulttiva',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }else{
          //error 500
          this.setState({showTheThing: false, showList: false}) 
            Alert.alert(
                'Error',
                'No se puede procesar la solicitud',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
      })
      .catch(error => {
        //error de comunicacion
        this.setState({showTheThing: false, showList: false}) 
            Alert.alert(
              'Error',
              'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
      });
    };

    handleRefresh = () => {
      this.setState(
        {
          page: 1,
          seed: this.state.seed + 1,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest();
        }
      );
    };

    handleLoadMore = () => {
      this.setState(
        {
          page: this.state.page + 1,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest();
        }
      );
    };
    render() {
      var s = require('../Styles/StyleArticles');
        return (
          <View style = {s.container}>
            <View style = {s.bottomElements}>
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
                { this.state.showEmpty && 
                  <View style = {s.containerEmpty}>

                  <Image
                      style={s.plantHome}
                      source={require('../Img/footerImg.png')}
                    />
                    <Text style= {s.txtHomeBottom}>Aún no tenemos articulos de esta categoria por favor intentalo mas adelante</Text>
                  </View>
                }
                { this.state.showList && 
                <List containerStyle={s.list}>
                    <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                      <SingleArticle category = {item.category} onPress={() => this.singleView(item.article_id)} image={{uri: item.image}} name={item.title}/>
                    )}
                    keyExtractor = {item => item.article_id}
                    ListFooterComponent={this.renderFooter}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    // onEndReached={this.handleLoadMore}
                    // onEndReachedThreshold={50}
                    />
                </List>
                }
            </View>
            
          </View>
          
          );
    }
}
export default Articles2;