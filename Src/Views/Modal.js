import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage, ToastAndroid,
  AlertIOS,
  Platform, } from 'react-native';
  import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import Share, {ShareSheet, Button} from 'react-native-share';
import ip from '../Config/AppConfig';
export default class Modal extends React.Component {
    markasDoneCrop(){
        this.setState({showTheThing: true}) 
            fetch(ip + 'users/crop_finish/',{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                    crop_id: this.props.id,
                    }),
            })
            .then((response) => {
                if(response.status == 200){
                    this.setState({showTheThing: false, isModalVisible: false}) 
                    if(this.state.isModalVisible == false){
                        Navigation.startTabBasedApp({
                            tabs: [
                              {
                                label: 'Kulttivos', // tab label as appears under the icon in iOS (optional)
                                screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                                icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                                selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                                title: 'Home', // title of the screen as appears in the nav bar (optional)
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                                navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                              },
                              {
                                label: 'Biblioteca',
                                screen: 'Kultiva.Articles',
                                icon: require('../assets/Images/iconBiblioteca.png'),
                                selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                                title: 'Biblioteca',
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                },  
                              },
                              {
                                label: 'Tienda',
                                screen: 'Kultiva.Store',
                                icon: require('../assets/Images/box.png'),
                                selectedIcon: require('../assets/Images/box.png'),
                                title: 'Tienda',
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                },  
                              },
                              {
                                label: 'Ajustes',
                                screen: 'Kultiva.Logout',
                                icon: require('../assets/Images/iconSettings.png'),
                                selectedIcon: require('../assets/Images/iconSettings.png'),
                                title: 'Ajustes',
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                },  
                              }
                            ],
                            appStyle: {
                              tabBarSelectedButtonColor: '#8EB84A',
                              tabFontSize: 10,
                              selectedTabFontSize: 12,
                            },
                            tabsStyle: {
                              tabBarSelectedButtonColor: '#8EB84A',
                              selectedTabFontSize: 12,
                            },
                            passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                            animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                          });
                    }   
                   
                }else if (response.status == 404){
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'No podemos recuperar información Kulttiva',
                        [
                            {text: 'OK', onPress: () => console.log("error", 'crop finish')},
                        ],
                        { cancelable: false }
                        )
                }else{
                  this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'No se puede procesar la solicitud',
                        [
                            {text: 'OK', onPress: () => console.log("error", 'crop finish')},
                        ],
                        { cancelable: false }
                        )
                }
            })
            .catch((error) => {
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                    [
                    {text: 'OK', onPress: () => console.log(error, 'crop finish')},
                    ],
                    { cancelable: false }
                )
                return;
            });


    }
    
  render() {
    var s = require('../Styles/StyleLifeCycle');
    
    return (
        <View style={s.modal}>
            <View style ={s.topModal}>
            <Text style = {s.txtModal}>{this.props.name}</Text>
            <Text style = {s.txtModalDesc}>¿Deseas marcar como realizado este kulttivo?</Text>
            </View>
            <View style ={s.bottomModal}>
                <TouchableOpacity style ={s.btnAccept} onPress={() => this.markasDoneCrop()}>
                <Text style = {s.txtModalButtonAccept}>Aceptar</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
  }
}