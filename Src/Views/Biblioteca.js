import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, StatusBar} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
class Biblioteca extends Component {
    render() {
      var s = require('../Styles/StyleBiblioteca');
        return (
          <View style = {s.container}>
          <StatusBar barStyle="light-content"/>
            <View style = {s.topElements}>
              <Text style = {s.titleCopper}>Biblioteca</Text>
            </View>
            <View style = {s.mediumElements}>
              
            </View>
            <View style = {s.medium2Elements}>
              <Text style = {s.titleCopper}>Artículos</Text>
            </View>
            <View style = {s.bottomElements}>
              
            </View>
            
          </View>
          
          );
    }
}
export default Biblioteca;