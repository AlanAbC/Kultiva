import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator} from 'react-native';
import MapView from 'react-native-maps';
class Maps extends Component {
    render() {
      var s = require('../Styles/StyleHome');
        return (
          <View style = {s.container}>
            <MapView
    initialRegion={{
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }}
  />
            
          </View>
          
          );
    }
}
export default Maps;