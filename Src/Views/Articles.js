import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity ,ActivityIndicator, FlatList, ScrollView} from 'react-native';
import { List, ListItem, SearchBar, Card } from "react-native-elements";
import Menu from 'react-native-pop-menu';
import Icon from 'react-native-vector-icons/Ionicons';
import SingleCategory from '../Components/Articles/SingleCategory';
import ip from '../Config/AppConfig';
import ls from 'react-native-local-storage';
class Articles extends Component {
    state = {
      menuVisible: false,
      colors: ['#D0021B', '#F5A623', '#F8E71C', '#8B572A', '#7ed321', '#417505', '#BD10E0', '#9013FE', '#4A90E2', '#50E3C2', '#B8E986', '#000000', '#47759F', '#F19600', '#F19600'],
      loading: false,
      farmer_id: 0,
      contentLeft: [],
      contentRight: [],
  };
  singleView(id){
    this.props.navigator.push({
        screen: 'Kultiva.SingleArticle', 
        title: 'Biblioteca',
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white',
          navBarButtonColor: 'white',
          tabBarHidden: true
        }, 
        passProps: {
            id: id
        }
      });
  }
  onPressCategory(cat){
    debugger;
    this.props.navigator.push({
     screen: 'Kultiva.Articles2', 
     title: 'Biblioteca',
     navigatorStyle: {
       navBarBackgroundColor: '#231F20',
       navBarTextColor: 'white',
       navBarButtonColor: 'white',
       tabBarHidden: true
     }, 
     passProps: {
         id: cat
     }
   });
 }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeCategories(data){
    if(((data.length)%2) == 0){
      leftSize = (data.length)/2;
    }else{
      leftSize = ((data.length)/2)+.5;
    }
    for(i = 0; i < leftSize; i++){
      var arrayvar = this.state.contentLeft.slice()
      arrayvar.push(<SingleCategory this = {this.props.navigator} key= {data[i].category_id} id = {data[i].category_id} category = {data[i].category} image = {data[i].image} title = {data[i].title}/>)
      this.setState({contentLeft: arrayvar});
    }
    for(j = leftSize; j < data.length; j++){
      var arrayvar = this.state.contentRight.slice()
      arrayvar.push(<SingleCategory this = {this.props.navigator} key= {data[j].category_id}  id = {data[j].category_id} category = {data[j].category} image = {data[j].image} title = {data[j].title}/>)
      this.setState({contentRight: arrayvar});
    }
  }
  makeRemoteRequest() {
    if(this.state.refreshing != true){
      this.setState({showTheThing: true}) 
    }
    const { page, seed } = this.state;
    const url = ip + 'contents/view_all_categories/';
    this.setState({ loading: true});
    fetch(url)
      .then(res => {
        console.log(res);
        if(res.status == 200){
          this.setState({showTheThing: false, showList: true}) 
            data = JSON.parse(res._bodyText);
            this.makeCategories(data);
        }else if(res.status == 204){
          this.setState({showTheThing: false, showList: false}) 

        }else if (res.status == 404){
          //No podemos recuperar información Kulttiva
          this.setState({showTheThing: false, showList: false}) 
            Alert.alert(
                'Error',
                'No podemos recuperar información Kulttiva',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }else{
          //error 500
          //No se puede procesar la solicitud
          this.setState({showTheThing: false, showList: false}) 
            Alert.alert(
                'Error',
                'No se puede procesar la solicitud',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        
      })
      .catch(error => {
        //No podemos comunicarnos con Kulttiva, verifica tu conexión a internet
        this.setState({showTheThing: false, showList: false}) 
            Alert.alert(
              'Error',
              'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
      });
    };

    handleRefresh = () => {
      this.setState(
        {
          page: 1,
          seed: this.state.seed + 1,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest();
        }
      );
    };

    handleLoadMore = () => {
      this.setState(
        {
          page: this.state.page + 1,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest();
        }
      );
    };
    render() {
      var s = require('../Styles/StyleCategories');
        return (
          <View style = {s.container}>
            <View style = {s.topElements}>
              <Text style = {s.titleCopper}>Categorias</Text>
            </View>
            <ScrollView style = {s.bottomElements}>
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
                
                <View style = {s.listsContainer}>
                  <View style = {s.left}>
                    {this.state.contentLeft}
                  </View>
                  <View style = {s.right}>
                    {this.state.contentRight}
                  </View>
                </View>
                
            </ScrollView>
            
          </View>
          
          );
    }
}
export default Articles;