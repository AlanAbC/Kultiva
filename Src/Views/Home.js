import React, { Component } from 'react'
import {Navigation} from 'react-native-navigation';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, ScrollView, TouchableOpacity,ActivityIndicator, FlatList, StatusBar} from 'react-native';
import { List, ListItem, SearchBar, Card } from "react-native-elements";
import Menu from 'react-native-pop-menu';
import Icon from 'react-native-vector-icons/Ionicons';
import HomeListItem from '../Components/HomeListItem/HomeListItem';
import ip from '../Config/AppConfig';
import ls from 'react-native-local-storage';
import OneSignal from 'react-native-onesignal'; 

class Home extends Component {

    state = {
      menuVisible: false,
      colors: ['#D0021B', '#F5A623', '#F8E71C', '#8B572A', '#7ed321', '#417505', '#BD10E0', '#9013FE', '#4A90E2', '#50E3C2', '#B8E986', '#000000', '#47759F', '#F19600', '#F19600'],
      loading: false,
      data: [],
      dataCompleted: [],
      schedules: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      farmer_id: 0, 
      empty1: false,
      empty2: false,
    };
    
  singleView(id, alias, done){
    this.props.navigator.push({
        screen: 'Kultiva.LifeCycle', 
        title: 'Mi Kulttivo',
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white',
          navBarButtonColor: 'white',
          tabBarHidden: true
        }, 
        passProps: {
            id: id,
            alias: alias,
            showNext: true,
            showPrev: false,
            done: done,
        }
      });
  }
  componentDidMount() {
    console.disableYellowBox = true;
      ls.get('farmer_id').then((local) => {this.makeRemoteRequest(local)});
      ls.get('farmer_id').then((local) => {this.makeRemoteRequest2(local)}); 
      ls.get('farmer_id').then((local) => {this.setState({farmer_id: local})});
  }

  componentWillMount() {
    
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened.bind(this));
    OneSignal.addEventListener('ids', this.onIds.bind(this));
    OneSignal.inFocusDisplaying(2);
  }

  componentWillUnmount() {
      OneSignal.removeEventListener('received', this.onReceived);
      OneSignal.removeEventListener('opened', this.onOpened.bind(this));
      OneSignal.addEventListener('opened', this.onOpened.bind(this));
      OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
      console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    if(openResult.notification.payload.additionalData.code == "2"){
      this.props.navigator.push({
        screen: 'Kultiva.LifeCycle', 
        title: 'Mi Kulttivo',
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white',
          navBarButtonColor: 'white',
          tabBarHidden: true
        }, 
        passProps: {
            id: openResult.notification.payload.additionalData.id,
            alias: openResult.notification.payload.additionalData.alias
        }
      });
    }
  }
  
  onIds(device) {
    
    ls.get('token').then((tok) => {

      if(tok == null){
        if(device.pushToken != null){
          ls.get('farmer_id').then((local) => {this.setNotificationToken(local, device.userId)});
        }
      }});
    
  }
  setNotificationToken = (farmer_id, token) => {
    fetch(ip + 'notification/register_device/',{
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_id: farmer_id,
          token: token
        }),
    })
    .then((response) => {
        if(response.status == 202){
            ls.save('token', token);
        
        }else if (response.status == 404){
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No podemos recuperar información Kulttiva',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }else{
          this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No se puede procesar la solicitud',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }
        })
        .catch((error) => {
            this.setState({showTheThing: false}) 
            Alert.alert(
            'Error',
            'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
            )
            return;
        });
    };
  makeRemoteRequest = (farmer_id) => {
    
    if(this.state.refreshing != true){
      this.setState({showTheThing: true}) 
    }
    const { page, seed } = this.state;
    const url = ip + 'users/view_my_crops/'+ farmer_id+'/';
    fetch(url)
      .then(res => {
        console.log(res);
        if(res.status == 200){
          this.setState({showTheThing: false, showList: true, showEmpty: false}) 
            data = JSON.parse(res._bodyText);
            console.log(data);
            this.setState({
                data: data,
                loading: false,
                refreshing: false
              });
        }else if(res.status == 204){
          this.setState({showTheThing: false, showList: false, showEmpty: false, empty1: true}) 
          this.checkEmpty();

        }else if (res.status == 404){
          this.setState({showTheThing: false, showList: false, showEmpty: false}) 
            Alert.alert(
                'Error',
                'No podemos recuperar información Kulttiva',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        }else{
          this.setState({showTheThing: false, showList: false, showEmpty: false}) 
          Alert.alert(
              'Error',
              'No se puede procesar la solicitud',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
        }
      })
      .catch(error => {
        this.setState({showTheThing: false, showList: false, showEmpty: false}) 
            Alert.alert(
              'Error',
              'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
      });
      
    }
    checkEmpty(){
      if(this.state.empty1 && this.state.empty2){
        this.setState({showList: false, showCompleted: false, showEmpty:true});
      }
    }
    makeRemoteRequest2 = (farmer_id) => {
      
      if(this.state.refreshing != true){
        this.setState({showTheThing: true}) 
      }
      const { page, seed } = this.state;
      const url = ip + 'users/view_my_crops_finished/'+ farmer_id+'/';
      console.log(url);
      fetch(url)
        .then(res => {
          console.log(res);
          if(res.status == 200){
            this.setState({showTheThing: false, showList: true, showEmpty: false, showCompleted: true}) 
              data = JSON.parse(res._bodyText);
              console.log(data);
              this.setState({
                  dataCompleted: data,
                  loading: false,
                  refreshing: false
                });
          }else if(res.status == 204){
            this.setState({showTheThing: false, showCompleted: false, empty2: true}) 
            this.checkEmpty();
  
          }else if (response.status == 404){
            this.setState({showTheThing: false, showCompleted: false}) 
              Alert.alert(
                  'Error',
                  'No podemos recuperar información Kulttiva',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
          }else{
            this.setState({showTheThing: false, showCompleted: false}) 
              Alert.alert(
                  'Error',
                  'No se puede procesar la solicitud',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
          }
          
        })
        .catch(error => {
          this.setState({showTheThing: false, showCompleted: false}) 
              Alert.alert(
                'Error',
                'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
              return;
        });
      };
    handleRefresh = () => {
      this.setState(
        {
          page: 1,
          seed: this.state.seed + 1,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest(this.state.farmer_id);
        }
      );
    };

    handleLoadMore = () => {
      this.setState(
        {
          page: this.state.page + 1,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest(this.state.farmer_id);
        }
      );
    };
    render() {
      var s = require('../Styles/StyleHome');
        return (
          <View style = {s.container}>
          <StatusBar barStyle="light-content"/>
            <View style = {s.topElements}>
              <Text style = {s.titleCopper}>Kulttivos</Text>
              <TouchableOpacity onPress={() => {
                            this.setState({
                              menuVisible: true
                            });}}>
                <Icon
                  style={s.icon}
                  size={23}
                  name={'md-add'}
                  color='#231F20'
                  />
              </TouchableOpacity>
              <Menu visible={this.state.menuVisible}
              onVisible={(isVisible) => {
                this.state.menuVisible = isVisible
              }} 
              data={[
                {
                  title: 'Nuevo Kulttivo',
                  onPress: () => {
                    this.props.navigator.push({
                      screen: 'Kultiva.NewKultivo', 
                      title: 'Mi Kulttivo',
                      navigatorStyle: {
                        navBarBackgroundColor: '#231F20',
                        navBarTextColor: 'white',
                        navBarButtonColor: 'white',
                        tabBarHidden: true
                      }, 
                    });
                  }
                },
                /*{
                  title: 'Kultivo existente',
                  onPress: () => {
                    //todo something...
                  }
                },*/
              ]} />
            </View>
            <View style = {s.bottomElements}>
                { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
                { this.state.showList && 
                <ScrollView style = {{height: '85%', width: '100%'}}>
                <View style = {{width: '100%'}}>
                  <View >
                    <FlatList
                    data={this.state.data}
                    renderItem={({ item }) => (
                        <HomeListItem onPress = {() => this.singleView(item.my_crop_id, item.alias, false)} name = {item.alias} marginLeft= {'5%'} color = {this.state.colors[item.color - 1]}/>
                    )}
                    keyExtractor = {item => item.my_crop_id}
                    ListFooterComponent={this.renderFooter}
                    />
                  </View>
                </View>
                  { this.state.showCompleted && 
                  <View>
                  <Text style = {s.topList}>Kulttivos Completados</Text>
                  
                  <View style = {{marginTop: 5}}>
                      <FlatList
                      data={this.state.dataCompleted}
                      renderItem={({ item }) => (
                          <HomeListItem onPress = {() => this.singleView(item.my_crop_id, item.alias, true)} name = {item.alias} marginLeft= {'5%'} color = {this.state.colors[item.color - 1]}/>
                      )}
                      keyExtractor = {item => item.my_crop_id}
                      ListFooterComponent={this.renderFooter}
                      />
                  </View>
                  
                  </View>}
                </ScrollView>
                }
                { this.state.showEmpty && 
                  <View style = {s.containerEmpty}>
                    <Image
                      style={s.arrowHome}
                      source={require('../Img/arrowHome.png')}
                    />
                  <Image
                      style={s.plantHome}
                      source={require('../Img/footerImg.png')}
                    />
                    <Text style= {s.txtHomeBottom}>Aún no tienes ningún kulttivo. Haz tap en el símbolo “+” para registrar un nuevo Kulttivo</Text>
                  </View>
                }
            </View>
            
          </View>
          
          );
    }
}
export default Home;