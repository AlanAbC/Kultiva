import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, FlatList, ScrollView, ImageBackground, StatusBar} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import ListItemNewKultivo from '../Components/ListItemNewKultivo/ListItemNewKultivo'
import ButtonDark from '../Components/ButtonDark/ButtonDark';
import ip from '../Config/AppConfig';
class SingleElement extends Component {
    state = {
      menuVisible: false,
      id: this.props.id,
      dificulty: this.props.dificulty,
      name: '',
      time: '',
      description: '',
      img: 'https://www.iconfinder.com/icons/170730/clock_loading_refresh_reload_slow_throbber_time_update_wait_waiting_icon'
    };
    componentDidMount() {
        this.makeRemoteRequest();
      }
    makeRemoteRequest = () => {
    this.setState({showTheThing: true}) 
    const url = ip + 'crops/view_single_crop/' + this.state.id + '/';
    fetch(url)
        .then(res => {
        if(res.status == 200){
            this.setState({showTheThing: false}) 
            data = JSON.parse(res._bodyText);
            if (data.image !== "") {
                this.setState({
                    img: data.image,
                    });
            }else{
                this.setState({
                    img: "http://tickets.iwsandbox.com:9000/static/img/logo_kulttiva.png",
                    });
            }
            this.setState({
                time: data.crop_time,
                description: data.description,
                name: data.name,
                });
        }else if(res.status == 204){
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Ups',
                'No hay kulttivos para esta dificultad',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
                )
        }else if (res.status == 404){
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No podemos recuperar información Kulttiva',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
                )
        }else{
            this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No se puede procesar la solicitud',
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
                )
        }
        
        })
        .catch(error => {
        this.setState({showTheThing: false}) 
            Alert.alert(
                'Error',
                'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
            )
            return;
        });
    };
    next(){
        this.props.navigator.push({
            screen: 'Kultiva.Location', 
            title: 'Mi Kulttivo',
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarButtonColor: 'white',
              tabBarHidden: true
            }, 
            passProps: {
                id: this.state.id
            }
          });
    }
    render() {
      var s = require('../Styles/StyleSingleElement');
      console.log(this.state.dificulty)
      if (this.state.dificulty === 1) {
        return (
            <View style = {s.container}>
            <StatusBar barStyle="light-content"/>
              <View style = {s.topElements}>
                  <Image style={s.image} source={{uri: this.state.img}}/>
                  <View style={s.info}>
                      <Text style={s.txtInfo}>{this.state.name}</Text>
                      <View style = {s.dificultyContainer}>
                          <Text style = {s.txtDificulty}>Dificultad</Text>
                          <Image style={s.imageDificulty} source={require('../Img/fillDificulty.png')}/>
                          <Image style={s.imageDificulty} source={require('../Img/emptyDificulty.png')}/>
                          <Image style={s.imageDificulty} source={require('../Img/emptyDificulty.png')}/>
                      </View>
                      <View style = {s.clockContainer}>
                          <Image style={s.imageClock} source={require('../Img/clock.png')}/>
                          <Text style = {s.txtClock}>{this.state.time}</Text>
                      </View>
                  </View>
              </View>
              <View style = {s.bottomElements}>
                  <Text style={s.txtTitleDesc}>Descripción General</Text>
                  <ScrollView style={s.containerText}>
                      <Text style={s.txtDescription}>{this.state.description}</Text>
                  </ScrollView>
                  <ButtonDark onPress= {() => this.next()} style = {s.buttonDark} name = {'Continuar con este Kulttivo'}/>
              </View>
              
            </View>
            
            );
      }else if (this.state.dificulty === 2 ) {
        return (
            <View style = {s.container}>
              <View style = {s.topElements}>
                <Image style={s.image} source={{uri: this.state.img}}/>
                  <View style={s.info}>
                      <Text style={s.txtInfo}>{this.state.name}</Text>
                      <View style = {s.dificultyContainer}>
                          <Text style = {s.txtDificulty}>Dificultad</Text>
                          <Image style={s.imageDificulty} source={require('../Img/fillDificulty.png')}/>
                          <Image style={s.imageDificulty} source={require('../Img/fillDificulty.png')}/>
                          <Image style={s.imageDificulty} source={require('../Img/emptyDificulty.png')}/>
                      </View>
                      <View style = {s.clockContainer}>
                          <Image style={s.imageClock} source={require('../Img/clock.png')}/>
                          <Text style = {s.txtClock}>{this.state.time}</Text>
                      </View>
                  </View>
              </View>
              <View style = {s.bottomElements}>
                  <Text style={s.txtTitleDesc}>Descripción General</Text>
                  <ScrollView style={s.containerText}>
                      <Text style={s.txtDescription}>{this.state.description}</Text>
                  </ScrollView>
                  <ButtonDark onPress= {() => this.next()} style = {s.buttonDark} name = {'Continuar con este Kulttivo'}/>
              </View>
              
            </View>
            
            );
      }else{
        return (
            <View style = {s.container}>
              <View style = {s.topElements}>
                  <Image style={s.image} source={{uri: this.state.img}}/>
                  <View style={s.info}>
                      <Text style={s.txtInfo}>{this.state.name}</Text>
                      <View style = {s.dificultyContainer}>
                          <Text style = {s.txtDificulty}>Dificultad</Text>
                          <Image style={s.imageDificulty} source={require('../Img/fillDificulty.png')}/>
                          <Image style={s.imageDificulty} source={require('../Img/fillDificulty.png')}/>
                          <Image style={s.imageDificulty} source={require('../Img/fillDificulty.png')}/>
                      </View>
                      <View style = {s.clockContainer}>
                          <Image style={s.imageClock} source={require('../Img/clock.png')}/>
                          <Text style = {s.txtClock}>{this.state.time}</Text>
                      </View>
                  </View>
              </View>
              <View style = {s.bottomElements}>
              { this.state.showTheThing && 
                <ActivityIndicator size="large" color="#231F20" />
                }
                  <Text style={s.txtTitleDesc}>Descripción General</Text>
                  <ScrollView style={s.containerText}>
                      <Text style={s.txtDescription}>{this.state.description}</Text>
                  </ScrollView>
                  <ButtonDark onPress= {() => this.next()} style = {s.buttonDark} name = {'Continuar con este Kulttivo'}/>
              </View>
              
            </View>
            
            );
      }
        
    }
}
export default SingleElement;