import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage, ScrollView, Dimensions, StatusBar  } from 'react-native';
import Button from '../Components/ButtonDark/ButtonDark';
import ip from '../Config/AppConfig';
import Input from '../Components/Input/Input';
import BtnIconFacebook from '../Components/BtnIcon/BtnIconFacebook';
// import PropTypes from 'prop-types';
import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import HTML from 'react-native-render-html';
export default class SingleArticle extends React.Component {
    state = {
        img:'https://www.iconfinder.com/icons/170730/clock_loading_refresh_reload_slow_throbber_time_update_wait_waiting_icon',
        
        content:'<p>Loading</p>',
        title: ''
    };
    componentDidMount() {
        this.makeRemoteRequest();
      }
      makeRemoteRequest() {
        if(this.state.refreshing != true){
          this.setState({showTheThing: true}) 
        }
        const url = ip + 'contents/view_single_articles/'+ this.props.id + '/';
        fetch(url)
          .then(res => {
            console.log(res);
            if(res.status == 200){
              this.setState({showTheThing: false})
                data = JSON.parse(res._bodyText);
                console.log(data);
                this.setState({
                    img: data.image,
                    content: data.content,
                    title: data.title
                });
            }else if(res.status == 204){
              this.setState({showTheThing: false}) 
    
            }else if (res.status == 404){
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
            
          })
          .catch(error => {
            this.setState({showTheThing: false}) 
                Alert.alert(
                  'Error',
                  'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
                return;
          });
        };
  render() {
    var s = require('../Styles/StyleSingleArticle');
    return (
      <View style={s.container}>
      <StatusBar barStyle="light-content"/>
        { this.state.showTheThing && 
            <ActivityIndicator size="large" color="#231F20" />
            }
          <ScrollView style={{ flex: 1 }}>
          <Image source={{uri: this.state.img}} style ={s.img}/>
        
            <Text style = {s.title}>{this.state.title}</Text>
            <View style = {s.htmlcontainer}><HTML html={this.state.content} imagesMaxWidth={Dimensions.get('window').width} /></View>
        </ScrollView>
      </View>
    );
  }
}