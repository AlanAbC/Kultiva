import React, {Component} from 'react';
import {Navigation} from 'react-native-navigation';
import {View, Text, TextInput, Alert, AsyncStorage, Image, ScrollView, ActivityIndicator, TouchableOpacity, StyleSheet, Dimensions, Platform, StatusBar} from 'react-native';
import ip from '../Config/AppConfig';
import LifeCycleBegining from '../Components/LifeCycle/LifeCycleBegin';
import LifeCycleWeek from '../Components/LifeCycle/LifeCycleWeek';
import LifeCycleSimple from '../Components/LifeCycle/LifeCycleSimple';
import LifeCyclePhoto from '../Components/LifeCycle/LifeCyclePhoto';
import LifeCycleWeather from '../Components/LifeCycle/LifeCycleWeather';
import LifeCycleItem from '../Components/LifeCycle/LifeCycleItem';
import LifeCycleItemFalse from '../Components/LifeCycle/LifeCycleItemFalse';
import LifeCycleWatherPhoto from '../Components/LifeCycle/LifeCycleWatherPhoto';
import LifeCycleEmpty from '../Components/LifeCycle/LifeCycleEmpty';
import ImagePicker from 'react-native-image-picker';
import Modal from 'react-native-modal';


class LifeCycle extends Component{

    state = {
        id: this.props.id,
        rows: [],
        isDisabled: false,
        disabledColor: '#231F20',
        icons: [require('../Img/weather/cloudy.png'), require('../Img/weather/electricRain.png'), require('../Img/weather/fog.png'), require('../Img/weather/rain.png'), require('../Img/weather/snow.png'), require('../Img/weather/sunny.png')],
        isModalVisible: false,
        name: this.props.alias,
        gestureName: 'none',
        showNext: this.props.showNext,
        showPrev: this.props.showPrev,
        numberCrop: '',
    };
    
    markasDoneCrop(){
        this.setState({showTheThing: true}) 
            fetch(ip + 'users/crop_finish/',{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                    crop_id: this.props.id,
                    }),
            })
            .then((response) => {
                if(response.status == 200){
                    this.setState({showTheThing: false, isModalVisible: false}) 
                    if(this.state.isModalVisible == false){
                        Navigation.startTabBasedApp({
                            tabs: [
                              {
                                label: 'Kulttivos', // tab label as appears under the icon in iOS (optional)
                                screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                                icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                                selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                                title: 'Home', // title of the screen as appears in the nav bar (optional)
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                                navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                              },
                              {
                                label: 'Biblioteca',
                                screen: 'Kultiva.Articles',
                                icon: require('../assets/Images/iconBiblioteca.png'),
                                selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                                title: 'Biblioteca',
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                },  
                              },
                              {
                                label: 'Tienda',
                                screen: 'Kultiva.Store',
                                icon: require('../assets/Images/box.png'),
                                selectedIcon: require('../assets/Images/box.png'),
                                title: 'Tienda',
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                },  
                              },
                              {
                                label: 'Ajustes',
                                screen: 'Kultiva.Logout',
                                icon: require('../assets/Images/iconSettings.png'),
                                selectedIcon: require('../assets/Images/iconSettings.png'),
                                title: 'Ajustes',
                                navigatorStyle: {
                                  navBarBackgroundColor: '#231F20',
                                  navBarTextColor: 'white'
                                },  
                              }
                            ],
                            appStyle: {
                              tabBarSelectedButtonColor: '#8EB84A',
                              tabFontSize: 10,
                              selectedTabFontSize: 12,
                            },
                            tabsStyle: {
                              tabBarSelectedButtonColor: '#8EB84A',
                              selectedTabFontSize: 12,
                            },
                            passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                            animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                          });
                    }   
                   
                }else if (response.status == 404){
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'No podemos recuperar información Kulttiva',
                        [
                            {text: 'OK', onPress: () => console.log("error", 'crop finish')},
                        ],
                        { cancelable: false }
                        )
                }else{
                    this.setState({showTheThing: false}) 
                    Alert.alert(
                        'Error',
                        'No se puede procesar la solicitud',
                        [
                            {text: 'OK', onPress: () => console.log("error", 'crop finish')},
                        ],
                        { cancelable: false }
                        )
                }
            })
            .catch((error) => {
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No podemos comunicarnos con Kulttiva, verifica tu conexión a internetNo podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                    [
                    {text: 'OK', onPress: () => console.log(error, 'crop finish')},
                    ],
                    { cancelable: false }
                )
                return;
            });


    }
    dismiss(){
        this.props.navigator.dismissModal({
            animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
          });
    }
    _toggleModal = () =>{
        this.props.navigator.showModal({
            screen: "Kultiva.Modal", // unique ID registered with Navigation.registerScreen
            passProps: {name: this.state.name, id: this.state.id},
            animationType: 'slide-up',
            navigatorStyle: {
                navBarBackgroundColor: '#231F20',
                navBarTextColor: 'white',
                navBarButtonColor: 'white',
                tabBarHidden: true,
            }
        });;
    }
    _addRow(row){
        this.setState({ rows: this.state.rows })
      }
      nextKulttivo() {
          debugger;
        if (this.state.next_id !== 0) {
            this.props.navigator.push({
                screen: 'Kultiva.LifeCycle', 
                title: 'Mi Kulttivo',
                animationType: 'slide-horizontal',
                navigatorStyle: {
                    navBarBackgroundColor: '#231F20',
                    navBarTextColor: 'white',
                    navBarButtonColor: 'white',
                    tabBarHidden: true,
                    // navBarHidden: true,
                    
                  }, 
                  navigatorButtons: {
                      leftButtons: [
                          {
                                title: 'Home', // for a textual button, provide the button title (label)
                                id: 'home', // id for this button, given in onNavigatorEvent(event) to help understand which button was clicked
                                ...Platform.select({
                                    android: {
                                        icon: require('../Img/left-arrow.png')
                                    }
                                })
                          } 
                          
                      ],
                  },
                  passProps: {
                      id: this.state.next_id,
                      alias: this.state.next_name,
                      showNext: true,
                  }
              });
              
          }else{
            this.setState({showNext: false});
          }
      }
      prevKulttivo() {
          debugger;
            if (this.state.preview_id !== 0) {
                this.props.navigator.push({
                    screen: 'Kultiva.LifeCycle', 
                    title: 'Mi Kulttivo',
                    animationType: 'fade',
                    navigatorStyle: {                                                                                                                                                                                             
                        navBarBackgroundColor: '#231F20',
                        navBarTextColor: 'white',
                        navBarButtonColor: 'white',
                        tabBarHidden: true,
                        // navBarHidden: true,
                      }, 
                      navigatorButtons: {
                        leftButtons: [
                            {
                                title: 'Home', // for a textual button, provide the button title (label)
                                id: 'home', // id for this button, given in onNavigatorEvent(event) to help understand which button was clicked
                                ...Platform.select({
                                    android: {
                                        icon: require('../Img/left-arrow.png')
                                    }
                                })
                                
                                
                            }
                            
                        ],
                    },
                      passProps: {
                        id: this.state.preview_id,
                        alias: this.state.preview_name,
                        showPrev: true,
                    }
                  });
              }else{
                this.setState({showPrev: false});
              }
      }
      constructor(props) {
        super(props);
        // if you want to listen on navigator events, set this up
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
      }
      onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
        
        if (event.type == 'NavBarButtonPress') { // this is the event type for button presses
          if (event.id == 'home') { // this is the same id field from the static navigatorButtons definition
            this.props.navigator.popToRoot({
                animated: true, // does the popToRoot have transition animation or does it happen immediately (optional)
                animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the popToRoot have different transition animation (optional)
              });
          }
          if (event.id == 'back') { // this is the same id field from the static navigatorButtons definition
            this.props.navigator.popToRoot({
                animated: true, // does the popToRoot have transition animation or does it happen immediately (optional)
                animationType: 'slide-horizontal', // 'fade' (for both) / 'slide-horizontal' (for android) does the popToRoot have different transition animation (optional)
              });
          }
        }
      }
    componentDidMount(){
        this.makeRemoteRequest();
        if(this.props.done){
            this.setState({showNumber: false, showFinalizado: true})
        }else{
            this.setState({showNumber: true, showFinalizado: false})
        }
    }
    showEdit(){
        this.setState({showName: false, showEdit: true});
    }
    selectPhotoTapped() {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true
          }
        };
    
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };
    
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
    
            this.setState({
              avatarSource: source,
              showImage: true
            });
          }
        });
      }
    begin(my_crop_id){
        this.setState({showTheThing: true})
        fetch(ip + 'users/start_crop/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                my_crop_id: my_crop_id,
              }),
        })
        .then((response) => {
            console.log(response);
            if(response.status == 200){
                this.setState({showTheThing: false, rows: []}) 
                this.makeRemoteRequest();
            }else if (response.status == 404){
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log("error", 'start_crop')},
                    ],
                    { cancelable: false }
                  )
            }else{
                this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log("error", 'start_crop')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
            this.setState({showTheThing: false}) 
            Alert.alert(
            'Error',
            'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
            [
                {text: 'OK', onPress: () => console.log(error, 'start_crop')},
            ],
            { cancelable: false }
            )
            return;
        });
    }
    singleElement(id, day){
        this.props.navigator.push({
            screen: 'Kultiva.SingleProduct', 
            title: 'Mi Kulttivo',
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarButtonColor: 'white',
              tabBarHidden: true
            }, 
            passProps: {
                id: id,
                day: day
            }
          });
    }
    nameChangeHandler = val =>{
        this.setState({
            name: val
        });
    };
    makecards=(data) =>{
        if(data.weather.day != 0){
            this.setState({isDisabled: true, disabledColor: '#c4c8ce'})
        }
        if(data.weather.day > data.cards.length){
            var x = data.cards.length - data.cards.length + 1;
        }else{
            var x = data.cards.length - (data.weather.day + 1);
        }
        for(i = x; i< data.cards.length; i++){
            if(i === data.cards.length - 1){
                if(data.cards[i].content){
                    var tasks = [];
                    var j =1; 
                    var day = data.cards[i].day;
                    var card_id = data.cards[i].card_id
                    for(let task of data.cards[i].tasks){
                        if(task.done){
                            tasks.push(<LifeCycleItem navigator = {this.props.navigator} day = {day} id = {task.task_id} key = {task.task_id} name = {task.task} position = {j}/>)
                        }else{
                            
                            tasks.push(<LifeCycleItemFalse my_crop_id = {this.props.id} card_id={card_id} task_id = {task.task_id} navigator = {this.props.navigator} day = {day} id = {task.task_id} key = {task.task_id} name = {task.task} position = {j}/>)
                        }
                        j++;
                    }
                    var arrayvar = this.state.rows.slice()
                    arrayvar.push(<LifeCycleBegining disabled = {this.state.isDisabled} onPressBegin = {() => this.begin(this.state.id)} tasks = {tasks} day = {data.cards[i].day} key = {data.cards[i].card_id} disabledColor = {this.state.disabledColor}/>)
                    this.setState({rows: arrayvar});
                }else{
                    var arrayvar = this.state.rows.slice()
                    arrayvar.push(<LifeCycleEmpty day = {data.cards[i].day}/>)
                    this.setState({rows: arrayvar});
                }
                
            }else if(i == x){
                if(data.cards[i].content){
                    var tasks = [];
                    var j = 1;
                    var day2 = data.cards[i].day;
                    var card_id = data.cards[i].card_id
                    for(let task of data.cards[i].tasks){
                        if(task.done){
                            tasks.push(<LifeCycleItem navigator = {this.props.navigator} day = {day2} id = {task.task_id} key = {task.task_id} name = {task.task} position = {j}/>)
                        }else{
                            tasks.push(<LifeCycleItemFalse my_crop_id = {this.props.id} card_id={card_id} task_id = {task.task_id} navigator = {this.props.navigator} day = {day2} id = {task.task_id} key = {task.task_id} name = {task.task} position = {j}/>)
                        }
                        j++;
                    }
                
                    var arrayvar = this.state.rows.slice()
                    if(data.cards[i].include_picture){
                        if(data.weather.icon == 'Clear'){
                            if(data.cards[i].image != ""){
                                arrayvar.push(<LifeCycleWatherPhoto showImage = {data.cards[i].image} icon = {this.state.icons[5]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                                this.setState({rows: arrayvar});
                            }else{
                                arrayvar.push(<LifeCycleWatherPhoto icon = {this.state.icons[5]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                                this.setState({rows: arrayvar});
                            }
                        }else if(data.weather.icon == 'Clouds'){
                            if(data.cards[i].image != ""){
                                arrayvar.push(<LifeCycleWatherPhoto showImage = {data.cards[i].image} icon = {this.state.icons[0]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                                this.setState({rows: arrayvar});
                            }else{
                                arrayvar.push(<LifeCycleWatherPhoto icon = {this.state.icons[0]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} />)
                                this.setState({rows: arrayvar});
                            }
                            
                        }else if(data.weather.icon == 'Rain'){
                            if(data.cards[i].image != ""){
                                arrayvar.push(<LifeCycleWatherPhoto showImage = {data.cards[i].image} icon = {this.state.icons[3]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                                this.setState({rows: arrayvar});
                            }else{
                                arrayvar.push(<LifeCycleWatherPhoto icon = {this.state.icons[3]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                                this.setState({rows: arrayvar});
                            }
                            
                        }else if(data.weather.icon == 'Thunderstorm'){
                            if(data.cards[i].image != ""){
                                arrayvar.push(<LifeCycleWatherPhoto showImage = {data.cards[i].image} icon = {this.state.icons[1]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                                this.setState({rows: arrayvar});
                            }else{
                                arrayvar.push(<LifeCycleWatherPhoto icon = {this.state.icons[1]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                                this.setState({rows: arrayvar});
                            }
                            
                        }else if(data.weather.icon == 'Snow'){
                            if(data.cards[i].image != ""){
                                arrayvar.push(<LifeCycleWatherPhoto showImage = {data.cards[i].image} icon = {this.state.icons[4]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                                this.setState({rows: arrayvar});
                            }else{
                                arrayvar.push(<LifeCycleWatherPhoto icon = {this.state.icons[4]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                                this.setState({rows: arrayvar});
                            }
                        }else if(data.weather.icon == 'Atmosphere'){
                            if(data.cards[i].image != ""){
                                arrayvar.push(<LifeCycleWatherPhoto showImage = {data.cards[i].image} icon = {this.state.icons[2]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                                this.setState({rows: arrayvar});
                            }else{
                                arrayvar.push(<LifeCycleWatherPhoto icon = {this.state.icons[2]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                                this.setState({rows: arrayvar});
                            }
                            
                        }else if(data.weather.icon == 'Default'){
                            if(data.cards[i].image != ""){
                                arrayvar.push(<LifeCycleWatherPhoto showImage = {data.cards[i].image} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                                this.setState({rows: arrayvar});
                            }else{
                                arrayvar.push(<LifeCycleWatherPhoto tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                                this.setState({rows: arrayvar});
                            }
                            
                        }
                        
                    }else{
                        if(data.weather.icon == 'Clear'){
                            arrayvar.push(<LifeCycleWeather icon = {this.state.icons[5]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }else if(data.weather.icon == 'Clouds'){
                            arrayvar.push(<LifeCycleWeather icon = {this.state.icons[0]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }else if(data.weather.icon == 'Rain'){
                            arrayvar.push(<LifeCycleWeather icon = {this.state.icons[3]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }else if(data.weather.icon == 'Thunderstorm'){
                            arrayvar.push(<LifeCycleWeather icon = {this.state.icons[1]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }else if(data.weather.icon == 'Snow'){
                            arrayvar.push(<LifeCycleWeather icon = {this.state.icons[4]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }else if(data.weather.icon == 'Atmosphere'){
                            arrayvar.push(<LifeCycleWeather icon = {this.state.icons[2]} tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }else if(data.weather.icon == 'Default'){
                            arrayvar.push(<LifeCycleWeather tasks = {tasks} day = {data.cards[i].day} temp = {data.weather.temp} weather = {data.weather.weather} location = {data.weather.location} message = {data.weather.message} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }
                    }
                }else{
                    var arrayvar = this.state.rows.slice()
                    arrayvar.push(<LifeCycleEmpty day = {data.cards[i].day}/>)
                    this.setState({rows: arrayvar});
                }
                
            }else{
                if(data.cards[i].content){
                    var tasks = [];
                    var j = 1;
                    var day3 = data.cards[i].day;
                    var card_id = data.cards[i].card_id;
                    console.log(data.cards[i].tasks)
                    for(let task of data.cards[i].tasks){
                        if(task.done){
                            tasks.push(<LifeCycleItem navigator = {this.props.navigator} day = {day3} id = {task.task_id} key = {task.task_id} name = {task.task} position = {j}/>)
                        }else{
                            console.log('is getting item (task)')
                            tasks.push(<LifeCycleItemFalse navigator = {this.props.navigator}  my_crop_id = {this.props.id} card_id={card_id} task_id = {task.task_id} day = {day3} id = {task.task_id} key = {task.task_id} name = {task.task} position = {j}/>)
                        }
                        j++;
                    }
                    var arrayvar = this.state.rows.slice()
                    if(data.cards[i].include_picture){
                        console.log('includes picture')
                        if(data.cards[i].image != ""){
                            arrayvar.push(<LifeCyclePhoto showImage = {data.cards[i].image} tasks = {tasks} day = {data.cards[i].day} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id} avatarSource={{uri: data.cards[i].image}} sourceFile={data.cards[i].image_64}/>)
                            this.setState({rows: arrayvar});
                        }else{
                            arrayvar.push(<LifeCyclePhoto tasks = {tasks} day = {data.cards[i].day} key = {data.cards[i].card_id} id = {data.cards[i].card_id} farmer_crop = {this.state.id}/>)
                            this.setState({rows: arrayvar});
                        }
                    }else{
                        console.log('not includes picture')
                        arrayvar.push(<LifeCycleSimple tasks = {tasks} day = {data.cards[i].day} key = {data.cards[i].card_id}/>)
                        this.setState({rows: arrayvar});
                    }
                }else{
                    var arrayvar = this.state.rows.slice()
                    arrayvar.push(<LifeCycleEmpty day = {data.cards[i].day}/>)
                    this.setState({rows: arrayvar});
                }
                
            }
        }
        this.setState({showTheThing: false}) 
    }
    saveName(name){
        fetch(ip + 'users/change_name_my_crop/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                new_name: name,
                my_crop_id: this.state.id,
              }),
        })
        .then((response) => {
            if(response.status == 200){
                this.setState({showName: true, showEdit: false});
            }else if (response.status == 404){
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
            Alert.alert(
            'Error',
            'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
            )
            return;
        });
    }
    makeRemoteRequest = () => {
        this.setState({showTheThing: true}) 
        if(this.state.refreshing != true){
          this.setState({showTheThing: true}) 
        }
        const { page, seed } = this.state;
        const url = ip + 'crops/view_time_line_crop/'+ this.state.id+'/';
        console.log(url);
        this.setState({ loading: true});
        fetch(url)
          .then(res => {
              
              console.log(res);
            if(res.status == 200){
                
              this.setState({showTheThing: false, showList: true, showEmpty: false}) 
                data = JSON.parse(res._bodyText);
                debugger;
                this.makecards(data);
                this.setState({
                    next_id: data.next.id,
                    next_name: data.next.name,
                    preview_id: data.preview.id,
                    preview_name: data.preview.name,
                    numberCrop: data.number_crop,
                });
                console.log("está entrando a un success!");
                if (this.state.next_id === 0) {
                    this.setState({
                        showNext: false
                    })
                    console.log("hay next")
                }else{
                    this.setState({
                        showNext: true
                    })
                }
                if (this.state.preview_id === 0) {
                    this.setState({
                        showPrev: false
                    })
                    console.log("hay prev")
                }else{
                    this.setState({
                        showPrev: true
                    })
                }
            }else if(res.status == 204){
              this.setState({showTheThing: false, showList: false, showEmpty: true}) 
    
            }else if (res.status == 404){
              this.setState({showTheThing: false, showList: false, showEmpty: true}) 
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log("error", 'view_time_line_crop', res.status)},
                    ],
                    { cancelable: false }
                  )
            }else{
                this.setState({showTheThing: false, showList: false, showEmpty: true}) 
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log("error", 'view_time_line_crop', res.status)},
                    ],
                    { cancelable: false }
                  )
            }
            
          })
          .catch(error => {
            this.setState({showTheThing: false, showList: false, showEmpty: true}) 
                Alert.alert(
                  'Error',
                  'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                  [
                    {text: 'OK', onPress: () => console.log(error, 'view_time_line_crop')},
                  ],
                  { cancelable: false }
                )
                return;
          });
        };
    render(){
        var s = require('../Styles/StyleLifeCycle');
        return(     
            <View style = {s.container}>
            
                <View style = {s.header}>
                <StatusBar barStyle="light-content"/>
                    <View style = {s.headerLeft}>
                    {this.state.showPrev &&
                    <TouchableOpacity onPress={()=>this.prevKulttivo()}><Image style = {s.image} resizeMode = {'center'} source={require('../assets/Images/backArrow.png')} /></TouchableOpacity>}
                    </View>
                    <View style = {s.headerCenter}>{ this.state.showFinalizado && <Text style = {{color: '#FC585C'}}>Finalizado</Text>}{ this.state.showNumber && <Text>{this.state.numberCrop}</Text>}{ this.state.showEdit && <View style = {s.contentHeaderCenter}><TextInput onChangeText = {this.nameChangeHandler} underlineColorAndroid = {"#00000000"} style = {s.txtInput} placeholder = {this.state.name}/><TouchableOpacity onPress = {() => this.saveName(this.state.name)}><Image style = {s.imageSave} resizeMode = {'center'} source={require('../Img/checked.png')} /></TouchableOpacity></View>}{ this.state.showName && <View style = {s.contentHeaderCenter}><Text style = {s.txtHeader}>{this.state.name}</Text><TouchableOpacity onPress = {() => this.showEdit()}><Image style = {s.imageEdit} resizeMode = {'center'} source={require('../Img/edit.png')} /></TouchableOpacity></View>}</View>
                    <View style = {s.headerRight}><TouchableOpacity onPress={this._toggleModal}><Image style = {s.nextArrow} resizeMode = {'center'} source={require('../Img/success.png')} /></TouchableOpacity>
                    {this.state.showNext && <TouchableOpacity onPress={()=>this.nextKulttivo()}><Image style = {s.imagecenter} resizeMode = {'center'} source={require('../assets/Images/nextArrow.png')} /></TouchableOpacity>}</View>
                </View>
                <ScrollView style = {s.bottom}>
                <View key={'container'}>
                    {this.state.rows}
                    { this.state.showTheThing && 
                    <ActivityIndicator size="large" color="#231F20" />
                    }
                </View>
                </ScrollView>
            </View>
        );
    }
}
export default LifeCycle;