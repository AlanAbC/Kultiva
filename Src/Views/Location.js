import React, { Component } from 'react'
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import {View, Text, TextInput, Alert, AsyncStorage, Image, TouchableOpacity,ActivityIndicator, StyleSheet, StatusBar} from 'react-native';
import Menu from 'react-native-pop-menu';
import Icon from 'react-native-vector-icons/Ionicons';
import MapView, {AnimatedRegion, Marker} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
import ButtonDark from '../Components/ButtonDark/ButtonDark';
Geocoder.setApiKey('AIzaSyDEZ6vQH4yj8gskmztompn8vYnLnUN04Rk');
const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      height: 400,
      width: 400,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    map: {
      ...StyleSheet.absoluteFillObject,
    },
  });
  
class Location extends Component {
    state = {
        farmer_id: this.props.farmer_id,
        latitude: 20.587190,
        longitude: -100.394909,
        latlng: {
            latitude: 20.587190,
            longitude: -100.394909
        },
        address: '',
        id: this.props.id,
    };
    componentDidMount() {
        Geolocation.getCurrentPosition(
            (position) => {
                console.log(position);
                let tempLatlng = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                }
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latlng: tempLatlng
                });
                this.geocoding(position.coords.latitude, position.coords.longitude);
            },
            (error) => {
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );

        
    }
    geocoding(lat, len) {
        Geocoder.getFromLatLng(lat, len).then(
            json => {
              var address_component = json.results[0].formatted_address;
              this.setState({address: address_component});
            },
            error => {
              alert(error);
            }
          );
    }
    onPressMap = (e) =>{
        let tempLatlng = {
            latitude: e.latitude,
            longitude: e.longitude,
        }
        this.setState({
            latitude: e.latitude,
            longitude: e.longitude,
            latlng: tempLatlng
        });
        this.geocoding(e.latitude, e.longitude);
    };
    next(){
        this.props.navigator.push({
            screen: 'Kultiva.KulttivoName', 
            title: 'Mi Kulttivo',
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarButtonColor: 'white',
              tabBarHidden: true
            }, 
            passProps: {
                lat: this.state.latitude,
                lon: this.state.longitude,
                id: this.state.id
            }
          });
    }
    render() {
      var s = require('../Styles/StyleLocation');
      const { region } = this.props;
        console.log(region);
        return (
            <View style = {s.container}>
            <StatusBar barStyle="light-content"/>
                <View style={s.top}>
                    <Text style= {s.title}>Localización</Text>
                    <Text style = {s.subtitles}>Señala en el mapa la localización de tu Kulttivo.</Text>
                </View>
                <View style = {s.bottom}>
                    <MapView
                    style={styles.map}
                    onLongPress = {(e) => this.onPressMap(e.nativeEvent.coordinate)}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                    >
                        <Marker
                        coordinate={this.state.latlng}
                        image={require('../Img/marker.png')}
                        title={"Kulttiva"}
                        />
                     </MapView>
                     <View style={s.whiteContainer}>
                            <Image style = {s.imgArrow} source={require('../Img/arrowlocation.png')}/>
                            <Text style = {s.txtAddress}>{this.state.address}</Text>
                    </View>
                    <ButtonDark onPress = {() => this.next()} style = {s.buttonDark} name = {'Continuar'}/>
                </View>
                
            </View>
          
          );
    }
}
export default Location;