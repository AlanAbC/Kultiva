import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert, ActivityIndicator, Image, AsyncStorage, Platform } from 'react-native';
import Button from '../Components/ButtonDark/ButtonDark';
import ip from '../Config/AppConfig';
import Input from '../Components/Input/Input';
import BtnIconFacebook from '../Components/BtnIcon/BtnIconFacebook';
import BtnIconGoogle from '../Components/BtnIcon/BtnIconGoogle'
// import PropTypes from 'prop-types';
import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import ls from 'react-native-local-storage';
import {Navigation} from 'react-native-navigation';
import OneSignal from 'react-native-onesignal'; 
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
export default class App extends React.Component {
  
  state = {
    email: '',
    password: '',
    location: false
  };
  _login = (email, password) => {
    debugger;
    this.setState({showTheThing: true}) 
    if(email != '' && password != ''){
        fetch(ip + 'login/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                email: email,
                password: password,
              }),
        })
        .then((response) => {
            if(response.status == 200){
                OneSignal.setSubscription(true);
                this.setState({showTheThing: false}) 
                data = JSON.parse(response._bodyText);
                debugger;
                ls.save('farmer_id', data.farmer_id);
                  if(data.location){
                    Navigation.startTabBasedApp({
                      tabs: [
                        {
                          label: 'Kultivos', // tab label as appears under the icon in iOS (optional)
                          screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                          icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                          selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                          // iconInsets: { 
                          //   top: 10,
                          //   bottom: 10,
                          //   left: 10,
                          //   right: 10,
                          // },
                          title: 'Home', // title of the screen as appears in the nav bar (optional)
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                          navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                        },
                        {
                          label: 'Biblioteca',
                          screen: 'Kultiva.Articles',
                          icon: require('../assets/Images/iconBiblioteca.png'),
                          selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                          title: 'Biblioteca',
                          // iconInsets: { 
                          //   top: 10,
                          //   bottom: 10,
                          //   left: 10,
                          //   right: 10,
                          // },
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          },  
                        },
                        {
                          label: 'Tienda',
                          screen: 'Kultiva.Store',
                          icon: require('../assets/Images/box.png'),
                          selectedIcon: require('../assets/Images/box.png'),
                          title: 'Tienda',
                          // iconInsets: { 
                          //   top: 0,
                          //   bottom: 0,
                          //   left: 0,
                          //   right: 0,
                          // },
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white',
                            navBarHidden: true
                          },  
                        },
                        {
                          label: 'Ajustes',
                          screen: 'Kultiva.Logout',
                          icon: require('../assets/Images/iconSettings.png'),
                          selectedIcon: require('../assets/Images/iconSettings.png'),
                          title: 'Ajustes',
                          // iconInsets: { 
                          //   top: 10,
                          //   bottom: 10,
                          //   left: 10,
                          //   right: 10,
                          // },
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          },  
                        }
                      ],
                      appStyle: {
                        tabBarSelectedButtonColor: '#8EB84A',
                        tabFontSize: 10,
                        selectedTabFontSize: 12,
                      },
                      tabsStyle: {
                        tabBarSelectedButtonColor: '#8EB84A',
                        selectedTabFontSize: 12,
                      },
                      passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                      animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                    });
                  }else{
                    this.props.navigator.resetTo({
                      screen: 'Kultiva.GPSPetitiion', 
                      title: 'Login',
                      passProps: {farmer_id: data.farmer_id},
                      navigatorStyle: {
                          navBarHidden: true
                        },
                    });
                  }
                
            }else if(response.status == 404){
              this.setState({showTheThing: false}) 
              data = JSON.parse(response._bodyText);
              console.log(data)
                Alert.alert(
                    'Error',
                    'El usuario no existe',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else if (response.status == 401){
              this.setState({showTheThing: false})
              data = JSON.parse(response._bodyText); 
                Alert.alert(
                    'Error',
                    'El usuario no está activo',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
              this.setState({showTheThing: false}) 
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
          debugger;
            this.setState({showTheThing: false}) 
            Alert.alert(
              'Error',
              'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
            return;
        });
    }else{
      debugger;
      this.setState({showTheThing: false}) 
        Alert.alert(
            'Error',
            'Por favor ingresa todo los campos',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        }
    };
    emailChangeHandler = val =>{
        this.setState({
            email: val
        });
    };
    passwordChangeHandler = val =>{
        this.setState({
            password: val
        });
    };
    loginGoogle(){
        
      GoogleSignin.signIn()
      .then((user) => {
          console.log(user);
          fetch(ip + 'login/',{
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                      social_network_id: ''+ user.id + '',
                      email: user.email,
                      name: user.name,
                      register_method: 3,
                      social_network_token: user.accessToken
              }),
          })
          .then((response) => {
              console.log(response);
              
              if(response.status == 200){
                OneSignal.setSubscription(true);
                  data = JSON.parse(response._bodyText);
                  console.log(data);
                  ls.save('farmer_id', data.farmer_id);

                  if(data.location){
                    Navigation.startTabBasedApp({
                      tabs: [
                        {
                          label: 'Kulttivos', // tab label as appears under the icon in iOS (optional)
                          screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                          icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                          selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                          title: 'Home', // title of the screen as appears in the nav bar (optional)
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                          navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                        },
                        {
                          label: 'Biblioteca',
                          screen: 'Kultiva.Articles',
                          icon: require('../assets/Images/iconBiblioteca.png'),
                          selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                          title: 'Biblioteca',
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          },  
                        },
                        {
                          label: 'Tienda',
                          screen: 'Kultiva.Store',
                          icon: require('../assets/Images/box.png'),
                          selectedIcon: require('../assets/Images/box.png'),
                          title: 'Tienda',
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white',
                            navBarHidden: true
                          }, // 
                        },
                        {
                          label: 'Ajustes',
                          screen: 'Kultiva.Logout',
                          icon: require('../assets/Images/iconSettings.png'),
                          selectedIcon: require('../assets/Images/iconSettings.png'),
                          title: 'Ajustes',
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          }, // 
                        }
                      ],
                      appStyle: {
                        tabBarSelectedButtonColor: '#8EB84A',
                        tabFontSize: 10,
                        selectedTabFontSize: 12,
                      },
                      tabsStyle: {
                        tabBarSelectedButtonColor: '#8EB84A',
                        selectedTabFontSize: 12,
                      },
                      passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                      animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                    });
                }else{
                  this.props.navigator.resetTo({
                    screen: 'Kultiva.GPSPetitiion', 
                    title: 'Login',
                    passProps: {farmer_id: data.farmer_id},
                    navigatorStyle: {
                        navBarHidden: true
                      },
                  });
                }
              }else if(response.status == 204){
                  Alert.alert(
                      'Error',
                      'Correo ya registrado',
                      [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                  )
              }else if (response.status == 404){
                  Alert.alert(
                      'Error',
                      'El usuario no existe',
                      [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                  )
              }else{
                Alert.alert(
                  'Error',
                  'No se puede procesar la solicitud',
                  [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
              )
              }
          })
          .catch((error) => {
              Alert.alert(
                  'Error',
                  'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                  [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
              )
          });
      })
      .catch((err) => {
          Alert.alert(
              'Error',
              'Imposible logearse con Google',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
      })
      .done();
  }
    _loginFacebook = (_this) => {
      FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.Native); 
      FBLoginManager.loginWithPermissions(["email","user_friends"], function(error, data){
        if (!error) {
          console.log("Login data: ", data);
          debugger;
          console.log('https://graph.facebook.com/v2.5/me?fields=email,name,first_name,last_name,friends&access_token=' + data.credentials.token);
          if (Platform.OS === 'ios') {
            fetch('https://graph.facebook.com/v2.5/me?fields=email,name,first_name,last_name,friends&access_token=' + data.credentials.token)
                .then((response) => 
                response.json())
                .then((json) => {
                  debugger;
                    console.log(json);
                    fetch(ip + 'login/',{
                      method: 'POST',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        social_network_id: data.credentials.userId,
                        email: json.email,
                        social_network_token: data.credentials.token,
                        register_method: 2,
                        name: json.first_name + " " + json.last_name,
                    }),
              })
              .then((response) => {
                  console.log(response);
                  if(response.status == 200){
                      data = JSON.parse(response._bodyText);
                      ls.save('farmer_id', data.farmer_id);
                      _this.props.navigator.resetTo({
                        screen: 'Kultiva.GPSPetitiion', 
                        title: 'Login',
                        passProps: {farmer_id: data.farmer_id},
                        navigatorStyle: {
                            navBarHidden: true
                          },
                    });
                  }else if (response.status == 404){
                    Alert.alert(
                        'Error',
                        'El usuario no existe',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                    }else{
                      Alert.alert(
                        'Error',
                        'No se puede procesar la solicitud',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                    }
              })
              .catch((error) => {
                  Alert.alert(
                      'Error',
                      'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                      [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                    )
              });

                })
                .catch(() => {
                    Alert.alert(
                        'Error',
                        'Imposible logearse con facebook',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                reject('ERROR')
                })
          }else{
            if (data.profile) {
              profile = JSON.parse(data.profile);
              fetch(ip + 'login/',{
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      social_network_id: data.credentials.userId,
                      email: profile.email,
                      social_network_token: data.credentials.token,
                      register_method: 2,
                      name: profile.first_name + " " + profile.last_name,
                    }),
              })
              .then((response) => {
                  console.log(response);
                  if(response.status == 200){
                      data = JSON.parse(response._bodyText);
                      ls.save('farmer_id', data.farmer_id);
                      _this.props.navigator.resetTo({
                        screen: 'Kultiva.GPSPetitiion', 
                        title: 'Login',
                        passProps: {farmer_id: data.farmer_id},
                        navigatorStyle: {
                            navBarHidden: true
                          },
                    });
                  }else if (response.status == 404){
                    Alert.alert(
                        'Error',
                        'El usuario no existe',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                    }else{
                      Alert.alert(
                        'Error',
                        'No se puede procesar la solicitud',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                    }
              })
              .catch((error) => {
                  Alert.alert(
                      'Error',
                      'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                      [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                      ],
                      { cancelable: false }
                    )
              });
          }else{
              Alert.alert(
                  'Error',
                  'Imposible logearse con facebook',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
          }
          }
        } else {
          Alert.alert(
              'Error',
              'Imposible logearse con facebook',
              [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
              ],
              { cancelable: false }
            )
        }
      })
  }
    _seeTerms = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Terms', 
            title: 'Terms',
            navigatorStyle: {
              navBarHidden: true
            }
        });
  };
  _seeReset = () => {
    this.props.navigator.push({
        screen: 'Kultiva.ResetPassword', 
        title: 'Terms',
        navigatorStyle: {
          navBarHidden: true
        }
    });
};
  render() {
    var s = require('../Styles/StyleLogin');
    return (
      <View style={s.container}>
      <View style={s.titleContent}>
        <Text style = {s.titleBlack}>INICIA SESIÓN </Text>
        <Text style = {s.titleCopper}>CON EMAIL O FACEBOOK</Text>
      </View>
      { this.state.showTheThing && 
          <ActivityIndicator size="large" color="green" />
        }
      <View style={s.inputsContainer}>
        <Input
          style={s.viewContainerComponent}
          name={"ios-mail-outline"}
          placeholder={'E-mail'}
          spell={false}
          value={this.state.email}
          handler={this.emailChangeHandler}  
          keyboardType={'email-address'}  
          autoCapitalize={'none'}  
          />
        <Input style={s.viewContainerComponent}
          name={'ios-lock-outline'}
          placeholder={"Contraseña"}
          secure={true}
          value={this.state.password}
          handler={this.passwordChangeHandler}
          />
          <Button
              style={s.viewContainerButton}
              name={'Iniciar sesión'}
              onPress={() => this._login(this.state.email, this.state.password)}
          />
          <TouchableOpacity style={{marginTop: 30}} onPress = {() => this._seeReset()}><Text style={s.textLight}>
             Olvidé mi Contraseña o Email
         </Text></TouchableOpacity>
         <BtnIconFacebook
                    style={s.btnfacebook}
                    name={"logo-facebook"}
                    onPress={() => this._loginFacebook(this)}
                    nameBtn={"Inicia sesión con Facebook"}
                    />
          <BtnIconGoogle
                    style={s.googleButtonStyle}
                    name={"logo-google"}
                    nameBtn={"Inicia sesión con Google"}
                    onPress={() => this.loginGoogle()}
                    />
      </View>
      <TouchableOpacity style={s.footer}  onPress={() => this._seeTerms()}>
            <Image
                style={s.imgFooter}
                source={require('../Img/footerImg.png')}
            />
            <Text style= {s.txtFooter}>Revisa términos y condiciones</Text>
        </TouchableOpacity>
      </View>
    );
  }
}