import React, {Component} from 'react';
import {View, Text, ImageBackground, TextInput, Alert, AsyncStorage, Image, TouchableOpacity, StyleSheet, TouchableWithoutFeedback,
    Dimensions, ActivityIndicator, Platform, StatusBar} from 'react-native';
import Button from 'react-native-button';
// import PropTypes from 'prop-types';
import {FBLogin, FBLoginManager} from 'react-native-facebook-login';
import {Navigation} from 'react-native-navigation';
import ip from '../Config/AppConfig';
import BtnIconBlack from '../Components/BtnIcon/BtnIconBlack';
import BtnIconGoogle from '../Components/BtnIcon/BtnIconGoogle';
import BtnIconFacebook from '../Components/BtnIcon/BtnIconFacebook';
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import Icon from "react-native-vector-icons/FontAwesome";
import ls from 'react-native-local-storage';
import OneSignal from 'react-native-onesignal'; 
import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
function secondsToTime(time) {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
  }

class FirstScreen extends Component{
    state = {
        paused: true,
        progress: 0,
        duration: 0,
    }
    componentWillMount() {
        OneSignal.inFocusDisplaying(2);
        OneSignal.setSubscription(false);
    }
    loginGoogle(){
        
        GoogleSignin.signIn()
        .then((user) => {
            fetch(ip + 'login/',{
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                        social_network_id: ''+ user.id + '',
                        email: user.email,
                        name: user.name,
                        register_method: 3,
                        social_network_token: user.accessToken
                }),
            })
            .then((response) => {
                console.log(response);
                
                if(response.status == 200){
                  OneSignal.setSubscription(true);
                    data = JSON.parse(response._bodyText);
                    console.log(data);
                    ls.save('farmer_id', data.farmer_id);
  
                    if(data.location){
                      Navigation.startTabBasedApp({
                        tabs: [
                          {
                            label: 'Kulttivos', // tab label as appears under the icon in iOS (optional)
                            screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                            icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                            selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                            title: 'Home', // title of the screen as appears in the nav bar (optional)
                            navigatorStyle: {
                              navBarBackgroundColor: '#231F20',
                              navBarTextColor: 'white'
                            }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                            navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                          },
                          {
                            label: 'Biblioteca',
                            screen: 'Kultiva.Articles',
                            icon: require('../assets/Images/iconBiblioteca.png'),
                            selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                            title: 'Biblioteca',
                            navigatorStyle: {
                              navBarBackgroundColor: '#231F20',
                              navBarTextColor: 'white'
                            },  
                          },
                          {
                            label: 'Tienda',
                            screen: 'Kultiva.Store',
                            icon: require('../assets/Images/box.png'),
                            selectedIcon: require('../assets/Images/box.png'),
                            title: 'Tienda',
                            navigatorStyle: {
                              navBarBackgroundColor: '#231F20',
                              navBarTextColor: 'white',
                              navBarHidden: true
                            }, // 
                          },
                          {
                            label: 'Ajustes',
                            screen: 'Kultiva.Logout',
                            icon: require('../assets/Images/iconSettings.png'),
                            selectedIcon: require('../assets/Images/iconSettings.png'),
                            title: 'Ajustes',
                            navigatorStyle: {
                              navBarBackgroundColor: '#231F20',
                              navBarTextColor: 'white'
                            }, // 
                          }
                        ],
                        appStyle: {
                          tabBarSelectedButtonColor: '#8EB84A',
                          tabFontSize: 10,
                          selectedTabFontSize: 12,
                        },
                        tabsStyle: {
                          tabBarSelectedButtonColor: '#8EB84A',
                          selectedTabFontSize: 12,
                        },
                        passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                        animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                      });
                  }else{
                    this.props.navigator.resetTo({
                      screen: 'Kultiva.GPSPetitiion', 
                      title: 'Login',
                      passProps: {farmer_id: data.farmer_id},
                      navigatorStyle: {
                          navBarHidden: true
                        },
                    });
                  }
                }else if(response.status == 204){ // no está contemplado en el documento pero se queda como está
                    Alert.alert(
                        'Error',
                        'Correo ya registrado',
                        [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                }else if (response.status == 404){
                    Alert.alert(
                        'Error',
                        'No podemos recuperar información Kulttiva',
                        [
                        {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                    )
                }else{
                  Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                )
                }
            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                    [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                )
            });
        })
        .catch((err) => {
            Alert.alert(
                'Error',
                'Imposible iniciar sesión con Google',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
        })
        .done();
    }

    handleMainButtonTouch = () => {
        if (this.state.progress >= 1) {
          this.player.seek(0);
        }
        this.setState(state => {
          return {
            paused: !state.paused,
          };
        });
    };
    handleProgressPress = e => {
        const position = e.nativeEvent.locationX;
        const progress = (position / 250) * this.state.duration;
        const isPlaying = !this.state.paused;
        
        this.player.seek(progress);
      };
    
      handleProgress = progress => {
        this.setState({
          progress: progress.currentTime / this.state.duration,
        });
      };
    
      handleEnd = () => {
        this.setState({ paused: true });
      };
    
      handleLoad = meta => {
        this.setState({
          duration: meta.duration,
        });
        this.setState({showLoadingOverlay: true})
      };    

    componentDidMount() {
        this.makeRemoteRequest();
        ls.get('farmer_id').then((data) => {console.log("get: ", data)});
        GoogleSignin.configure({
            scopes: ["https://www.googleapis.com/auth/plus.login"], 
            forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
            iosClientId: '648776248427-1095h4smupbtq2l8d3km78u7n50u5igt.apps.googleusercontent.com',
          })
          .then(() => {
            // you can now call currentUserAsync()
          });
    }
    makeRemoteRequest(){
        fetch(ip + 'about/welcome_video/',{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',                  
            },
        })
        .then((response) => {
          console.log(response)
          if(response.status == 200){
                data = JSON.parse(response._bodyText);
                this.setState({urlVideo: data.welcome_video});
                this.setState({shouldPlay: true})
            }else if(response.status == 204){
                data = JSON.parse(response._bodyText);
                this.setState({shouldPlay: false})
                console.log(data)                  
            }else if (response.status == 404){
                this.setState({shouldPlay: false})
                this.setState({dataErrorMessage: 'No podemos recuperar información Kulttiva'})
            }else{
                this.setState({shouldPlay: false})
                this.setState({dataErrorMessage: 'No se puede procesar la solicitud'})
            }
        })
        .catch((error) => {
            this.setState({shouldPlay: false})
            this.setState({dataErrorMessage: 'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet'})
            return;
        });
    }
    loginFunction(userId, email, firstName, lastName, token){
        fetch(ip + 'login/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                social_network_id: userId, 
                email: email,
                social_network_token: token,
                register_method: 2,
                name: firstName + " " + lastName,
              }),
        })
        .then((response) => {
          debugger;
            console.log(response);
            if(response.status == 200){
                data = JSON.parse(response._bodyText);
                console.log(data);
                ls.save('farmer_id', data.farmer_id);
                if(data.location){
                    Navigation.startTabBasedApp({
                      tabs: [
                        {
                          label: 'Kulttivos', // tab label as appears under the icon in iOS (optional)
                          screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                          icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                          selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                          title: 'Home', // title of the screen as appears in the nav bar (optional)
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                          navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                        },
                        {
                          label: 'Biblioteca',
                          screen: 'Kultiva.Articles',
                          icon: require('../assets/Images/iconBiblioteca.png'),
                          selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                          title: 'Biblioteca',
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          },  
                        },
                        {
                          label: 'Tienda',
                          screen: 'Kultiva.Store',
                          icon: require('../assets/Images/box.png'),
                          selectedIcon: require('../assets/Images/box.png'),
                          title: 'Tienda',
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white',
                            navBarHidden: true
                          }, // 
                        },
                        {
                          label: 'Ajustes',
                          screen: 'Kultiva.Logout',
                          icon: require('../assets/Images/iconSettings.png'),
                          selectedIcon: require('../assets/Images/iconSettings.png'),
                          title: 'Ajustes',
                          navigatorStyle: {
                            navBarBackgroundColor: '#231F20',
                            navBarTextColor: 'white'
                          }, // 
                        }
                      ],
                      appStyle: {
                        tabBarSelectedButtonColor: '#8EB84A',
                        tabFontSize: 10,
                        selectedTabFontSize: 12,
                      },
                      tabsStyle: {
                        tabBarSelectedButtonColor: '#8EB84A',
                        selectedTabFontSize: 12,
                      },
                      passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                      animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                    });
                  }else{
                    this.props.navigator.resetTo({
                      screen: 'Kultiva.GPSPetitiion', 
                      title: 'Login',
                      passProps: {farmer_id: data.farmer_id},
                      navigatorStyle: {
                          navBarHidden: true
                        },
                    });
                  }
            }else if (response.status == 404){
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
                }else{
                  Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
                }
            })
            .catch((error) => {
              Alert.alert(
                  'Error',
                  'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                  [
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                  ],
                  { cancelable: false }
                )
          });
    }
    _loginFacebook = (_this) => {
        FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.Native); 
        FBLoginManager.loginWithPermissions(["email","user_friends"], function(error, data){
          if (!error) {
            console.log("Login data: ", data);
            if (Platform.OS === 'ios') {
                fetch('https://graph.facebook.com/v2.5/me?fields=email,name,first_name,last_name,friends&access_token=' + data.credentials.token)
                .then((response) => 
                response.json())
                .then((json) => {
                  debugger;
                    console.log(json);
                    _this.loginFunction(data.credentials.userId, json.email, json.first_name, json.last_name, data.credentials.token);

                })
                .catch(() => {
                    Alert.alert(
                        'Error',
                        'Imposible logearse con facebook',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                reject('ERROR')
                })
            }else{
                if (data.profile) {
                    profile = JSON.parse(data.profile);
                    fetch(ip + 'login/',{
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                          },
                          body: JSON.stringify({
                            social_network_id: data.credentials.userId,
                            email: profile.email,
                            social_network_token: data.credentials.token,
                            register_method: 2,
                            name: profile.first_name + " " + profile.last_name,
                          }),
                    })
                    .then((response) => {
                        console.log(response);
                        if(response.status == 200){
                          debugger;
                            OneSignal.setSubscription(true);
                            data = JSON.parse(response._bodyText);
                            console.log(data);
                            ls.save('farmer_id', data.farmer_id);
                            if(data.location){
                                Navigation.startTabBasedApp({
                                  tabs: [
                                    {
                                      label: 'Kulttivos', // tab label as appears under the icon in iOS (optional)
                                      screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
                                      icon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
                                      selectedIcon: require('../assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
                                      title: 'Home', // title of the screen as appears in the nav bar (optional)
                                      navigatorStyle: {
                                        navBarBackgroundColor: '#231F20',
                                        navBarTextColor: 'white'
                                      }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
                                      navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
                                    },
                                    {
                                      label: 'Biblioteca',
                                      screen: 'Kultiva.Articles',
                                      icon: require('../assets/Images/iconBiblioteca.png'),
                                      selectedIcon: require('../assets/Images/iconBiblioteca.png'),
                                      title: 'Biblioteca',
                                      navigatorStyle: {
                                        navBarBackgroundColor: '#231F20',
                                        navBarTextColor: 'white'
                                      },  
                                    },
                                    {
                                      label: 'Tienda',
                                      screen: 'Kultiva.Store',
                                      icon: require('../assets/Images/box.png'),
                                      selectedIcon: require('../assets/Images/box.png'),
                                      title: 'Tienda',
                                      navigatorStyle: {
                                        navBarBackgroundColor: '#231F20',
                                        navBarTextColor: 'white',
                                        navBarHidden: true
                                      }, // 
                                    },
                                    {
                                      label: 'Ajustes',
                                      screen: 'Kultiva.Logout',
                                      icon: require('../assets/Images/iconSettings.png'),
                                      selectedIcon: require('../assets/Images/iconSettings.png'),
                                      title: 'Ajustes',
                                      navigatorStyle: {
                                        navBarBackgroundColor: '#231F20',
                                        navBarTextColor: 'white'
                                      }, // 
                                    }
                                  ],
                                  appStyle: {
                                    tabBarSelectedButtonColor: '#8EB84A',
                                    tabFontSize: 10,
                                    selectedTabFontSize: 12,
                                  },
                                  tabsStyle: {
                                    tabBarSelectedButtonColor: '#8EB84A',
                                    selectedTabFontSize: 12,
                                  },
                                  passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
                                  animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
                                });
                              }else{
                                _this.props.navigator.resetTo({
                                  screen: 'Kultiva.GPSPetitiion', 
                                  title: 'Login',
                                  passProps: {farmer_id: data.farmer_id},
                                  navigatorStyle: {
                                      navBarHidden: true
                                    },
                                });
                              }
                        }else if (response.status == 404){
                            Alert.alert(
                                'Error',
                                'No podemos recuperar información Kulttiva',
                                [
                                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                                ],
                                { cancelable: false }
                              )
                        }else{
                          Alert.alert(
                            'Error',
                            'No se puede procesar la solicitud',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                        }
                    })
                    .catch((error) => {
                        Alert.alert(
                            'Error',
                            'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )
                    });
                }else{
                    Alert.alert(
                        'Error',
                        'Imposible logearse con facebook',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }
            }
            
          } else {
            Alert.alert(
                'Error',
                'Imposible logearse con facebook',
                [
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
              )
          }
        })
    }
    _setLogin = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Login', 
            title: 'Registro',
            navigatorStyle: {
                navBarHidden: true
              }
        });
      };
    _seeTerms = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Terms', 
            title: 'Terms',
            navigatorStyle: {
              navBarHidden: true
            }
        });
    };
    _seeRegister = () => {
        this.props.navigator.push({
            screen: 'Kultiva.Register', 
            title: 'Terms',
            navigatorStyle: {
              navBarHidden: true
            }
        });
    };
    _playVideo = () => {
         if (this.state.shouldPlay) {
            this.setState({showTheThing: true, disabled: false}) ;
            this.setState({paused: false}); 
         }else{
             this.setState({showTheThing: false, disabled: true }) ;
             Alert.alert(
                 'Oops',
                 this.state.dataErrorMessage,
                 [
                     {text: 'OK', onPress: () => console.log('OK Pressed')},
                 ],
                     { cancelable: false }
                 )
         }
           
    }
    render(){
        var _this = this;
        var s = require('../Styles/StyleFirstScreen');
        return(
            <View style={s.container}>
            <StatusBar hidden={true} />
                <View style={s.topscreen}>
                    { this.state.showTheThing && 
                        <View style={StyleSheet.absoluteFill}>
                        <Video 
                             source={{uri: this.state.urlVideo}}
                            resizeMode='cover'
                            style={StyleSheet.absoluteFill}
                            paused={this.state.paused}
                            onLoad={this.handleLoad}
                            onProgress={this.handleProgress}
                            onEnd={this.handleEnd}
                            ref={ref => {
                                this.player = ref;
                            }}
                        />
                        {!this.state.showLoadingOverlay &&
                        <View style={{justifyContent: 'center', width: '100%', height: '100%'}}>
                            <ActivityIndicator size='large'/>
                        </View> 
                        }
                        <View style={s.controls}>
                            <TouchableWithoutFeedback onPress={this.handleMainButtonTouch}>
                                <Icon name={!this.state.paused ? "pause" : "play"} size={30} color="#FFF" />
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={this.handleProgressPress}>
                             <View>
                                <ProgressBar
                                    progress={this.state.progress}
                                    color="#FFF"
                                    unfilledColor="rgba(255,255,255,.5)"
                                    borderColor="#FFF"
                                    width={250}
                                    height={20}
                                />
                            </View>
                            </TouchableWithoutFeedback>

                            <Text style={s.duration}>
                                {secondsToTime(Math.floor(this.state.progress * this.state.duration))}
                             </Text>
                         </View>
                        </View>
                    }
                    { !this.state.showTheThing && 
                    <ImageBackground 
                    style={StyleSheet.absoluteFillObject}
                    source={require('../assets/Images/fallVeggiesLayoutKeyhole0915.png')}
                    >
                    <View style = {s.containerVideoText}>
                    <TouchableOpacity style={s.playButton}
                    onPress={() => this._playVideo()} disabled = {this.state.disabled}>
                        <Image
                            source={require('../assets/Images/icon.png')}
                        />
                    </TouchableOpacity>

                    <Text style = {s.videoTextBold}>
                        ¿Qué es Kulttiva? Dale play y descúbrelo 😃
                    </Text>
                    </View>
                 </ImageBackground>
                    }
                </View>
                <View style = {s.bottomscreen}>
                    <BtnIconFacebook
                    style={s.btnfacebook}
                    name={"logo-facebook"}
                    onPress={() => this._loginFacebook(_this)}
                    nameBtn={"Regístrate con Facebook"}
                    />
                    <BtnIconGoogle
                    style={s.googleButtonStyle}
                    name={"logo-google"}
                    nameBtn={"Regístrate con Google"}
                    onPress={() => this.loginGoogle()}
                    />
                    <BtnIconBlack
                    style={s.mailButtonStyle}
                    name={"ios-mail-outline"}
                    nameBtn={"Regístrate con Email"}
                    onPress={() => this._seeRegister()}
                    />
                    <TouchableOpacity onPress={() => this._setLogin()}><Text style= {s.txtGreen}>¿Ya tienes cuenta? haz tap aquí para iniciar sesión</Text></TouchableOpacity>
                    
                </View>
                <TouchableOpacity style={s.footer}  onPress={() => this._seeTerms()}>
                        <Image
                         style={s.imgFooter}
                        source={require('../Img/footerImg.png')}
                        />
                        <Text style= {s.txtFooter}>Revisa términos y condiciones</Text>
                </TouchableOpacity>
                
            </View>
        );
    }
}
export default FirstScreen;