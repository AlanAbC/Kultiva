'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    btn: {
        width: '100%',
        height: 60,
        marginTop: 30,
        borderBottomColor: '#CBCBCB',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtbtn: {
        width: '65%',
        marginLeft: '10%',
        fontSize: 20,
        color: '#4A4A4A'
    },
})

module.exports = myStyles;
