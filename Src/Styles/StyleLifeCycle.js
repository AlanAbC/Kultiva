'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    header: {
        width: '100%',
        height: 50,
        borderBottomColor: '#C2C2C2',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bottom: {
        height: '90%',
        width: '100%',
    },
    image: {
        // width: 25,
        // height: 25,
        width: 45,
        height: 45,
    },
    imagecenter: {
        width: 25,
        height: 25,
        // marginLeft: 6,
        marginRight: 25,
    },
    nextArrow: {
        width: 25,
        height: 25,
        marginRight: 10,
    },
    headerLeft: {
        width: '20%',
        alignItems: 'center',
    },
    headerCenter: {
        width: '60%',
        alignItems: 'center',
    },
    headerRight: {
        width: '20%',
        alignItems: 'center',
        flexDirection: 'row',
    },
    txtHeader: {
        fontSize: 17,
        color: '#4A4A4A'
    },
    contentHeaderCenter: {
        flexDirection: 'row',
    },
    modal:{
        height: '100%',
        width: '100%',
        borderRadius: 7,
        backgroundColor: 'white',
        alignItems: 'center',
    },
    txtModal: {
        color: '#231F20',
        fontWeight: 'bold',
        fontSize: 25,
        textAlign: 'center',
        marginTop: '5%'
    },
    txtModalDesc: {
        color: '#9B9B9B',
        width: '80%',
        marginTop: '5%',
        textAlign: 'center',
        fontSize: 17
    },
    topModal: {
        width: '100%',
        height: '25%',
        alignItems: 'center',
    },
    bottomModal: {
        width: '100%',
        height: '22%',
        flexDirection: 'row',
    },
    btnCancel: {
        width:'50%',
        height: '100%',
        borderBottomLeftRadius: 5,
        borderTopColor: '#231F20',
        borderRightColor: '#231F20',
        borderTopWidth: 1,
        borderRightWidth: 1,
    },
    btnAccept: {
        width:'50%',
        height: '100%',
        marginLeft: '25%',
        borderBottomRightRadius: 5,
        borderTopColor: '#231F20',
        borderTopWidth:1
    },
    txtModalButtonCancel: {
        fontSize: 20,
        color: '#231F20',
        textAlign: 'center',
        marginTop: '5%'
    },
    txtModalButtonAccept: {
        fontSize: 20,
        color: '#8EB84A',
        textAlign: 'center',
        marginTop: '5%'
    },
    txtInput: {
        fontSize: 14,
        color: '#4A4A4A',
        width: '40%',
    },
    imageEdit: {
        width: 25,
        height: 25,
    },
    imageSave: {
        width: 13,
        height: 14,
        marginTop: 18.5
    },
})

module.exports = myStyles;
