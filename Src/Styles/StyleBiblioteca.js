
var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topElements: {
        flexDirection: 'row',
        width: '100%',
        height: '11%',
        alignItems: 'center',
        
    },
    mediumElements: {
        width: '100%',
        height: '52%',
            },
    medium2Elements: {
        flexDirection: 'row',
        width: '100%',
        height: '11%',
        alignItems: 'center',
        
    },
    titleCopper: {
        color: '#C78F51',
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: '5%',
        marginTop: 5,
    },
    bottomElements: {
        width: '100%',
        height: '30%',
        alignItems: 'center',
        flexDirection: 'column',
    },
})

module.exports = myStyles;
