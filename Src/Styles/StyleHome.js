
var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topElements: {
        flexDirection: 'row',
        width: '100%',
        height: '12%',
        alignItems: 'center',
    },
    titleCopper: {
        color: '#C78F51',
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: '5%',
    },
    bottomElements: {
        width: '100%',
        height: '88%',
        alignItems: 'center',
        flexDirection: 'column',
    },
    icon: {
        marginLeft: '70%',
    },
    arrowHome: {
        width: '35%',
        height: 200,
        resizeMode: 'contain',
        marginTop: '10%',
        marginLeft: '15%'
    },
    plantHome: {
        width: '40%',
        height: 30,
        resizeMode: 'contain',
        marginTop: '10%',

    },
    txtHomeBottom:{
        color: '#9B9B9B',
        width: '55%',
        textAlign: 'center',
        marginTop: '5%'
    },
    popMenu: {
        backgroundColor: 'white'
    },
    list: {
        borderTopWidth: 0, 
        borderBottomWidth: 0 ,
        width: '100%',
    },
    listTwo: {
        borderTopWidth: 0, 
        borderBottomWidth: 0 ,
        width: '100%',
    },
    containerEmpty: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    topList: {
        width: '90%',
        height: 35,
        fontWeight: 'bold',
        color: '#C78F51',
        fontSize: 25,
        marginLeft: '5%',
        marginTop: 15,
    }
})

module.exports = myStyles;
