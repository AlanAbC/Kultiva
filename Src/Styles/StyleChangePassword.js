'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        paddingTop: 20,
    },
    btn: {
        width: '100%',
        height: 50,
        borderBottomColor: '#CBCBCB',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtbtn: {
        width: '82%',
        marginLeft: '5%',
        fontSize: 20,
        color: '#4A4A4A'
    },
    icon: {
        marginLeft: '5%',
    },
    title: {
        width: '85%',
        marginTop: 10,
        color: '#231F20',
        fontSize: 36,
        fontWeight: 'bold',
    },
    
    subtitle: {
        width: '85%',
        marginTop: 5,
        color: '#231F20',
        fontSize: 14,
    },
    input: {
        width: '70%'
    },
    inputContainer: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#CBCBCB',
        borderBottomWidth: 1
    },
    txtInput: {
        width: '25%',
        marginLeft: '5%',
        color:'#231F20',
        fontSize:17
    },
    titleCopper: {
        width: '85%',
        textAlign: 'left',
        color: '#C78F51',
        fontSize: 36,
        fontWeight: 'bold'
    },
    viewContainerComponent1: {
        width: '85%',
        height: 47,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#FFFFFF',
        borderColor: '#D7D7D7',
        borderWidth: 1,
        ...React.Platform.select({
            android: {
                paddingBottom: 0,
            }
        }),
        marginTop: 30,
    },
    viewContainerComponent2: {
        width: '85%',
        height: 47,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#FFFFFF',
        borderColor: '#D7D7D7',
        borderWidth: 1,
        ...React.Platform.select({
            android: {
                paddingBottom: 0,
            }
        }),
        marginTop: 10,
    },
    btnBlack: {
        width: '85%',
        height: 47,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#231F20',
        marginTop: 30,
    }
})

module.exports = myStyles;
