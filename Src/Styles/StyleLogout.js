'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
    },
    btn: {
        width: '100%',
        height: 50,
        borderBottomColor: '#CBCBCB',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtbtn: {
        width: '82%',
        marginLeft: '5%',
        fontSize: 20,
        color: '#4A4A4A'
    },
    icon: {
        marginLeft: '5%',
    },
    version:{
        marginTop: '10%',
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
    },
})

module.exports = myStyles;
