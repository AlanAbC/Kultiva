'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        alignItems: 'center',
    },
    titleText: {
        fontSize: 25,
        width: '70%',
        marginTop: '25%',
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center'
    },
    imgIcon: {
        width: '37%',
        height: 30,
        marginTop: '85%'
    },
    txtBottom: {
        fontSize: 15,
        width: '70%',
        marginTop: '5%',
        color: 'white',
        textAlign: 'center'
    }
})

module.exports = myStyles;