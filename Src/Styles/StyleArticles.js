var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topElements: {
        flexDirection: 'row',
        width: '100%',
        height: '9%',
        alignItems: 'center',
    },
    titleCopper: {
        color: '#C78F51',
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: '5%',
    },
    bottomElements: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        flexDirection: 'column',
    },
    icon: {
        marginLeft: '75%',
    },
    list: {
        borderTopWidth: 0, 
        borderBottomWidth: 0 ,
        width: '100%',
        height: '100%'
    },
    containerEmpty: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    plantHome: {
        width: '40%',
        height: 30,
        resizeMode: 'contain',
        marginTop: '10%',

    },
    txtHomeBottom:{
        color: '#9B9B9B',
        width: '65%',
        textAlign: 'center',
        marginTop: '5%'
    },
})

module.exports = myStyles;
