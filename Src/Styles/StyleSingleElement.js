'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
    },
    topElements: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
    },
    bottomElements: {
        width: '100%',
        height: '70%',
    },
    image: {
        width: '40%',
        marginLeft: '5%',
        height: '90%',
        marginTop: '5%'
    },
    info: {
        width: '50%',
        marginLeft: '5%',
    },
    txtInfo: {
       color: '#231F20',
       fontSize: 24,
       fontStyle: 'italic',
       width: '80%',

       marginTop: '10%',
    },
    dificultyContainer: {
        height: '15%',
        marginTop: '2%',
        width: '100%',
        flexDirection: 'row',
    },
    txtDificulty: {
        fontSize: 10,
        color: '#231F20',
    },
    imageDificulty: {
        width: 18,
        height: 10,
        resizeMode: 'center',
        marginTop: 2,
        marginLeft: 4
    },
    txtTitleDesc: {
        width: '90%',
        fontWeight: 'bold',
        color: '#231F20',
        fontSize: 24,
        marginLeft: '5%',
    },
    containerText: {
        width: '100%',
    },
    txtDescription: {
        width: '90%',
        marginLeft: '5%',
        color: '#231F20',
        fontSize: 15,
        marginTop: 15
    },
    buttonDark: {
        width: '80%',
        height: 45,
        backgroundColor: '#231F20',
        borderRadius: 5,
        marginLeft: '10%',
        marginTop: 10,
        marginBottom: 10
    },
    clockContainer: {
        height: '15%',
        width: '100%',
        flexDirection: 'row',
    },
    imageClock: {
        width: 18,
        height: 10,
        resizeMode: 'center',
        marginTop: 2,
    },
    txtClock: {
        fontSize: 10,
        color: '#231F20',
        marginRight: '30%',
    },
    light: {
        height: '20%',
        width: '100%',
        marginTop: '-12%'
    },
    htmlcontainer: {
        width: '90%',
        marginLeft: '5%',
        marginTop: 15,
    }
})

module.exports = myStyles;
