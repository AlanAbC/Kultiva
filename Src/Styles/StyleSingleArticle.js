var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    img: {
        width: '100%',
        height: 200
    },
    title: {
        width: '90%',
        marginLeft: '5%',
        color: '#231F20',
        fontWeight: 'bold',
        fontSize: 26,
    },
    htmlcontainer: {
        width: '90%',
        marginLeft: '5%',
        marginTop: 15,
    }
})

module.exports = myStyles;