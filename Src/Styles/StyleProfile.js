'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        paddingTop: 20,
    },
    btn: {
        width: '100%',
        height: 50,
        borderBottomColor: '#CBCBCB',
        borderBottomWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtbtn: {
        width: '82%',
        marginLeft: '5%',
        fontSize: 20,
        color: '#4A4A4A'
    },
    icon: {
        marginLeft: '5%',
    },
    title: {
        width: '90%',
        marginTop: 15,
        color: '#231F20',
        fontSize: 22,
        fontWeight: 'bold',
    },
    input: {
        width: '70%'
    },
    inputContainer: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#CBCBCB',
        borderBottomWidth: 1
    },
    txtInput: {
        width: '25%',
        marginLeft: '5%',
        color:'#231F20',
        fontSize:17
    }
})

module.exports = myStyles;
