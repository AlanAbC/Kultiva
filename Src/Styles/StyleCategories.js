var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topElements: {
        flexDirection: 'row',
        width: '100%',
        height: '9%',
        alignItems: 'center',
    },
    titleCopper: {
        color: '#C78F51',
        fontSize: 25,
        fontWeight: 'bold',
        marginLeft: '5%',
    },
    bottomElements: {
        width: '100%',
        height: '91%',
        flexDirection: 'column',
    },
    icon: {
        marginLeft: '75%',
    },
    list: {
        borderTopWidth: 0, 
        borderBottomWidth: 0 ,
        width: '100%',
        height: '100%'
    },
    left: {
        width: '50%',
        height: '100%',
        alignItems: 'center',
    },
    right: {
        width: '50%',
        height: '100%',
        alignItems: 'center',
    },
    listsContainer: {
        width: '100%',
        flexDirection: 'row',
    },
    category:{
        width: '95%',
        height: 150,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 7,
        backgroundColor: '#d9d9d9', 
        elevation: 4
    },
    image:{
        width: '100%',
        height:'75%',
        backgroundColor: 'red',
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7,
    },
    txtCat: {
        width: '100%',
        height: '25%',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 15,
        color: '#231F20',
    }
})

module.exports = myStyles;
