var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    topElements: {
        flexDirection: 'column',
        width: '100%',
        alignItems: 'center',
    },
    bottom: {
        flexDirection: 'column',
        width: '100%',
    },
    bottomContainer: {
        alignItems: 'center',
    },
    titleBlack: {
        color: '#000',
        fontSize: 26,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '3%',
    },
    titleDays: {
        color: '#8eb84a',
        fontSize: 18,
        textAlign: 'center',
        marginTop: '2%',
    },
    midElements: {
        width: '100%',
        height: ' 89%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    imageProduct: {
        marginTop: 15,
        height: 200,
    },
    buyButton:{
        width: 112,
        height: 34,
        marginTop: 15,
        marginRight: '55%',
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
        borderWidth: 1,
        borderColor: '#231f20',
        flexDirection: 'row',
    },
    Icon: {
        paddingTop: '5%',
        paddingLeft: '10%',
    },
    textButton: {
        paddingTop: '5%',
        paddingLeft: '10%',
        fontSize: 15,
    },
    textDescriptionTitle:{
        marginTop: 15,
        marginLeft: '3%',
        marginRight: '3%',
        textAlign: 'left',
        fontSize: 23,
        fontWeight: 'bold',
        width: '84%',
    },
    textDescription:{
        marginTop: 10,
        marginLeft: '3%',
        marginRight: '3%',
        fontSize: 16,
        fontWeight: 'normal',
        width: '84%',
    },
    videoScreen: {
        width: '90%',
        height: 220,
        backgroundColor: '#F3F3F3',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '5%',
    },
    playButton: {
        alignSelf: 'center',
        paddingVertical: '20%',
    },
    controls: {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        height: 48,
        left: 0,
        bottom: 0,
        right: 0,
        position: "absolute",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        paddingHorizontal: 10,
      },
      duration: {
        color: "#FFF",
        marginLeft: 15,
      },
      footer: {
        color: "#FFF",
        height: 80,
        width: '100%'
      },
      htmlcontainer: {
        width: '85%',
        marginTop: 15,
    },
    sliderImages: {
        width: '85%',
        marginTop: 15,
    },
})

module.exports = myStyles;