'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    topElements: {
        width: '100%',
        height: '45%',
    },
    titleBlack: {
        width: '90%',
        marginLeft: '5%',
        textAlign: 'left',
        color: '#231F20',
        fontSize: 38,
        marginTop: 15,
        fontWeight: 'bold',
    },
    titleCopper: {
        width: '90%',
        marginLeft: '5%',
        textAlign: 'left',
        color: '#C78F51',
        fontSize: 38,
        fontWeight: 'bold'
    },
    subTitleBlack: {
        width: '90%',
        marginLeft: '5%',
        textAlign: 'left',
        color: '#231F20',
        fontSize: 14,
        marginTop: 10,
        fontWeight: 'normal',
    },
    subTitleGray: {
        width: '90%',
        marginLeft: '5%',
        textAlign: 'left',
        color: '#9B9B9B',
        fontSize: 14,
        marginTop: 2,
        fontWeight: 'normal',
    },
    sectionDropdown: {
        width: '100%',
        height: '20%',
        marginTop: '3%',
        flexDirection: 'row',
    },
    txtDropdown: {
        width: '20%',
        fontSize: 13,
        color: '#231F20',
        marginLeft: '5%',
        marginTop: 12
    },
    bottomElements: {
        width: '100%',
        height: '55%',
    },
    combobox: {
        width: '30%',
        height: 30,
        borderColor: '#231F20',
        borderRadius: 5,
        borderWidth: 1,
        marginTop: 7,
        marginLeft: '2%',
    },
    combocontent: {
        width : '30%',
        backgroundColor : '#F2F2F2',
        height: 107,
        marginTop: 30
    },
    txtComboBox: {
        marginLeft: 10,
        marginTop: 5,
        color: '#231F20',
        fontSize: 14
    }
})

module.exports = myStyles;
