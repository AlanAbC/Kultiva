'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    footer: {
        width: '100%',
        backgroundColor: '#ffffff',
        height: '9%',
        alignItems: 'center',
        marginTop: '2%',
    },
    imgFooter:{
        width: '50%',
        height: 25,
        marginTop: 5,
    },
    txtFooter: {
        color: 'black',
        fontSize: 10
    },
    scroll: {
        width: '100%',
        height: '89%',
    }
    ,
    titleBlack: {
        width: '90%',
        marginLeft: '8%',
        textAlign: 'left',
        color: '#231F20',
        fontSize: 36,
        marginTop: 20,
        fontWeight: 'bold',
    },
    titleCopper: {
        width: '90%',
        marginLeft: '8%',
        textAlign: 'left',
        color: '#C78F51',
        fontSize: 36,
        fontWeight: 'bold'
    },
    txtContent: {
        width: '87%',
        marginLeft: '8%',
        color: '#231F20',
        fontSize: 14,
        marginTop: 15,
        marginBottom: 15
    }
})

module.exports = myStyles;
