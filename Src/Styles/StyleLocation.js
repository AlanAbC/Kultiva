'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    top: {
        width: '100%',
        height: '30%',
        alignItems: 'center'
    },
    title:{
        color: '#231F20',
        fontSize: 25,
        fontWeight: 'bold',
        width: '100%',
        textAlign: 'center',
        marginTop: '10%'
    },
    subtitles: {
        color: '#231F20',
        fontSize: 14,
        width: '70%',
        textAlign: 'center',
        marginTop: '3%'
    },
    bottom: {
        width: '100%',
        height: '70%',
        alignItems: 'center'
    },
    whiteContainer: {
        backgroundColor: '#FFFFFF',
        height: 45,
        width: '90%',   
        flexDirection: 'row', 
        borderRadius: 5,
        marginTop: '65%'
    },
    imgArrow: {
        width: 20,
        height: 20,
        marginTop: 10,
        marginLeft: 10
    },
    txtAddress: {
        width: '85%',
        marginLeft: '4%',
        height: 20,
        fontSize: 16,
        color: '#231F20',
        marginTop: 8,
    },
    buttonDark: {
        width: '90%',
        height: 45,
        backgroundColor: '#231F20',
        borderRadius: 5,
        marginTop: 15,
        marginBottom: 10
    },
})

module.exports = myStyles;
