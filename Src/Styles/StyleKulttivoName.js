'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    top: {
        width: '100%',
        height: '45%',
        alignItems: 'center',
    },
    bottom: {
        width: '100%',
        height: '55%',
        alignItems: 'center',
    },
    title:{
        color: '#231F20',
        fontSize: 25,
        fontWeight: 'bold',
        width: '100%',
        textAlign: 'center',
        marginTop: '10%'
    },
    subtitles: {
        color: '#231F20',
        fontSize: 14,
        width: '70%',
        textAlign: 'center',
        marginTop: '3%'
    },
    buttonDark: {
        width: '80%',
        height: 45,
        backgroundColor: '#231F20',
        borderRadius: 5,
        marginTop: 30,
        marginBottom: 10
    },
    input: {
        width: '85%',
        height: 45,
        borderColor: '#EBEBEB',
        borderRadius: 5,
        borderWidth: 1,
        marginTop: '10%'
    },
    circlesContainer: {
        width: '100%',
        height: 30,
        marginTop: '5%',
        flexDirection: 'row'
    },
    circle: {
        width: 20,
        height: 20,
        marginTop: 5,
        marginLeft: '12%',
        borderRadius: 20,
        borderWidth: 1,
        borderColor: 'red'
    },
})

module.exports = myStyles;
