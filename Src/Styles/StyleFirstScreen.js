'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnfacebook:{
        marginTop: 15, 
        height: 50, 
        marginLeft: '8%', 
        marginRight: '8%', 
        backgroundColor: '#415dae', 
        borderRadius:4,
        flexDirection: 'row',
        alignItems: 'center',
    },
    video: {
        position: 'absolute',
        top: 0, left: 0, right: 0, bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },
    mailButtonStyle:{
        marginTop: 15, 
        height: 50,
        marginLeft: '8%',
        marginRight: '8%', 
        backgroundColor: 'white', 
        borderRadius:4,
        borderColor: 'black',
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    googleButtonStyle:{
        marginTop: 15, 
        height: 50,
        marginLeft: '8%',
        marginRight: '8%', 
        backgroundColor: '#CE4231', 
        borderRadius:4,
        borderColor: '#CE4231',
        borderWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    topscreen: {
        width: '100%',
        height: '45%',
        backgroundColor: '#F3F3F3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    bottomscreen : {
        height: '46%',
        flexDirection: 'column',
        width: '100%',
        backgroundColor: 'white'
    },
    titletopscreen:{
        width: '100%',
        color: '#ffffff',
        fontSize: 25,
        marginTop: 80,
        textAlign: 'center',
    },
    input: {
        width: '70%',
        borderWidth: 1,
        borderColor: '#868686',
        backgroundColor: '#d9d9d9',
        borderRadius: 10,
        marginTop: 50
    },
    button:{
        color: '#000000'
    },
    contentContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: 375,
        height: 300,
      },
    textContainer: {
        color: 'white',
        fontSize: 22,
    },
    txtGreen: {
        color: '#0E763C',
        fontSize: 15,
        marginLeft: '20%',
        marginRight: '20%',
        textAlign: 'center',
        marginTop: 15
    },
    footer: {
        width: '100%',
        backgroundColor: '#ffffff',
        borderTopWidth: 3,
        borderTopColor: '#F2F2F2',
        height: '9%',
        alignItems: 'center'
    },
    imgFooter:{
        width: '50%',
        height: 25,
        marginTop: 5,
    },
    txtFooter: {
        color: 'black',
        fontSize: 10
    },
    imgMail:{
        width: 25,
        height: 25,
        marginTop: 5,
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    videoTextBold: {
        width: '80%',
        marginTop: 7,
        fontWeight: 'bold',
        fontSize: 19,
        backgroundColor: 'transparent',
        color: '#FFF',
        textAlign: 'center',
    },
    videoText: {
        marginTop: 5,
        fontWeight: 'normal',
        fontSize: 20,
        backgroundColor: 'transparent',
        color: '#FFF',
    },
    containerVideoText: {
        flexDirection: 'column',
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
    },
    playButton: {
        alignSelf: 'center',
    },
    controls: {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        height: 48,
        left: 0,
        bottom: 0,
        right: 0,
        position: "absolute",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        paddingHorizontal: 10,
      },
      duration: {
        color: "#FFF",
        marginLeft: 15,
      },
})

module.exports = myStyles;
