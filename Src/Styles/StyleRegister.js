'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleBlack: {
        width: '90%',
        marginLeft: '8%',
        textAlign: 'left',
        color: '#231F20',
        fontSize: 36,
        marginTop: 20,
        fontWeight: 'bold',
    },
    titleCopper: {
        width: '90%',
        marginLeft: '8%',
        textAlign: 'left',
        color: '#C78F51',
        fontSize: 36,
        fontWeight: 'bold'
    },
    txtContent: {
        width: '87%',
        marginLeft: '8%',
        marginRight: '8%',
        color: '#231F20',
        fontSize: 14,
        marginTop: 15,
        marginBottom: 15
    },
    footer: {
        width: '100%',
        backgroundColor: '#ffffff',
        borderTopWidth: 3,
        borderTopColor: '#F2F2F2',
        height: '9%',
        alignItems: 'center'
    },
    imgFooter:{
        width: '50%',
        height: 25,
        marginTop: 5,
    },
    txtFooter: {
        color: 'black',
        fontSize: 10
    },
    header: {
        width: '100%',
        height: '30%'
    },
    medium: {
        width: '100%',
        height: '61%',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    viewContainerComponent: {
        width: '80%',
        height: 47,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#FFFFFF',
        borderColor: '#D7D7D7',
        borderWidth: 1,
        ...React.Platform.select({
            android: {
                paddingBottom: 0,
            }
        }),
        marginTop: 10,
    },
    txtGreen: {
        textAlign: 'center',
        color: '#2E8232',
        fontSize: 14,
        marginTop: 15,

    },
    txtcontainer: {
        width: '62%',
    },
    btnBlack: {
        width: '80%',
        height: 47,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#231F20',
        marginTop: 10,
    }
})

module.exports = myStyles;
