'use strict';

var React = require('react-native');

var myStyles = React.StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    titleBlack: {
        width: '90%',
        marginLeft: '8%',
        textAlign: 'left',
        color: '#231F20',
        fontSize: 40,
        marginTop: 20,
        fontWeight: 'bold',
    },
    titleCopper: {
        width: '90%',
        marginLeft: '8%',
        textAlign: 'left',
        color: '#C78F51',
        fontSize: 40,
        fontWeight: 'bold'
    },
    contentContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: 375,
        height: 300,
      },
    textContainer: {
        color: 'white',
        fontSize: 22,
    },
    inputsContainer: {
        width: '100%',
        height: '51%',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    viewContainerComponent: {
        width: '80%',
        height: 47,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#FFFFFF',
        borderColor: '#D7D7D7',
        borderWidth: 1,
        marginTop: 10,
    },
    titleContent: {
        width: '100%',
        height: '40%'
    },
    subtitle: {
        width: '90%',
        marginLeft: '8%',
        marginRight: 18,
        textAlign: 'left',
        color: '#000000',
        fontSize: 14,
        paddingTop: 21,
    },
    footer: {
        width: '100%',
        backgroundColor: '#ffffff',
        borderTopWidth: 3,
        borderTopColor: '#F2F2F2',
        height: '9%',
        alignItems: 'center'
    },
    viewContainerButton: {
        width: '80%',
        height: 47,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        backgroundColor: '#231f20',
        borderWidth: 1,
        marginTop: 10,
    },
    imgFooter:{
        width: '50%',
        height: 25,
        marginTop: 5,
    },
    txtFooter: {
        color: 'black',
        fontSize: 10
    },
    textBold: {
        color: '#2e8232', 
        fontWeight:'bold'
    },
    textLight:{
        fontWeight: 'normal', 
        color: '#2e8232', 
        marginTop: 12
    },
})

module.exports = myStyles;
