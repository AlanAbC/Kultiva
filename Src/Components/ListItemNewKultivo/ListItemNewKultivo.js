import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
class ListItemNewKultivo extends Component {
    render = () => {
        if(this.props.dificulty === 1) {
            return(
                <TouchableOpacity onPress = {this.props.onPress} style={Style.container}>
                    <Image
                    resizeMode = {'center'}
                    source={{uri: this.props.image}}
                    style={Style.img}
                    />
                    <View style={Style.infoContainer}>
                        <Text style = {Style.txtTitleDesc}>{this.props.name}</Text>
                        <View style = {Style.dificultyContainer}>
                            <Text style = {Style.txtDificulty}>Dificultad</Text>
                            <Image style={Style.imageDificulty} source={require('../../Img/fillDificulty.png')}/>
                            <Image style={Style.imageDificulty} source={require('../../Img/emptyDificulty.png')}/>
                            <Image style={Style.imageDificulty} source={require('../../Img/emptyDificulty.png')}/>
                        </View>
                        <View style= {Style.clockContainer}>
                            <Image style={Style.imageClock} source={require('../../Img/clock.png')}/>
                            <Text style ={Style.txtClock}>{this.props.firstCos}</Text>
                        </View>
                        <View style = {Style.clockContainer}>
                            <Text style = {Style.txtDificulty}>Zona climatica:</Text>
                            <Text style = {Style.txtClock}>{this.props.weather}</Text>
                        </View>
                    </View>
                    <View style = {Style.arrowContainer}>
                        <Image resizeMode={'center'} style={Style.nextArrow} source={require('../../Img/nextArrow.png')}/>
                    </View>
                </TouchableOpacity>
            );
        }else if (this.props.dificulty === 2) {
            return(
                <TouchableOpacity onPress = {this.props.onPress} style={Style.container}>
                    <Image
                    resizeMode = {'center'}
                    source={{uri: this.props.image}}
                    style={Style.img}
                    />
                    <View style={Style.infoContainer}>
                        <Text style = {Style.txtTitleDesc}>{this.props.name}</Text>
                        <View style = {Style.dificultyContainer}>
                            <Text style = {Style.txtDificulty}>Dificultad</Text>
                            <Image style={Style.imageDificulty} source={require('../../Img/fillDificulty.png')}/>
                            <Image style={Style.imageDificulty} source={require('../../Img/fillDificulty.png')}/>
                            <Image style={Style.imageDificulty} source={require('../../Img/emptyDificulty.png')}/>
                        </View>
                        <View style= {Style.clockContainer}>
                            <Image style={Style.imageClock} source={require('../../Img/clock.png')}/>
                            <Text style ={Style.txtClock}>{this.props.firstCos}</Text>
                        </View>
                        <View style = {Style.clockContainer}>
                            <Text style = {Style.txtDificulty}>Zona climatica:</Text>
                            <Text style = {Style.txtClock}>{this.props.weather}</Text>
                        </View>
                    </View>
                    <View style = {Style.arrowContainer}>
                        <Image resizeMode={'center'} style={Style.nextArrow} source={require('../../Img/nextArrow.png')}/>
                    </View>
                </TouchableOpacity>
            );
        }else{
            return(
                <TouchableOpacity onPress = {this.props.onPress} style={Style.container}>
                    <Image
                    resizeMode = {'center'}
                    source={{uri: this.props.image}}
                    style={Style.img}
                    />
                    <View style={Style.infoContainer}>
                        <Text style = {Style.txtTitleDesc}>{this.props.name}</Text>
                        <View style = {Style.dificultyContainer}>
                            <Text style = {Style.txtDificulty}>Dificultad</Text>
                            <Image style={Style.imageDificulty} source={require('../../Img/fillDificulty.png')}/>
                            <Image style={Style.imageDificulty} source={require('../../Img/fillDificulty.png')}/>
                            <Image style={Style.imageDificulty} source={require('../../Img/fillDificulty.png')}/>
                        </View>
                        <View style= {Style.clockContainer}>
                            <Image style={Style.imageClock} source={require('../../Img/clock.png')}/>
                            <Text style ={Style.txtClock}>{this.props.firstCos}</Text>
                        </View>
                        <View style = {Style.clockContainer}>
                            <Text style = {Style.txtDificulty}>Zona climatica:</Text>
                            <Text style = {Style.txtClock}>{this.props.weather}</Text>
                        </View>
                    </View>
                    <View style = {Style.arrowContainer}>
                        <Image resizeMode={'center'} style={Style.nextArrow} source={require('../../Img/nextArrow.png')}/>
                    </View>
                </TouchableOpacity>
            );
        }
        
    }
}
const Style = StyleSheet.create({
    container: {
        width: '100%',
        height: 110,
        borderBottomColor: '#979797',
        borderBottomWidth: 1,
        flexDirection: 'row',
    },
    input: {
        paddingLeft: 12,
        width: '90%',
        height: '100%',
        
    },
    icon: {
        paddingLeft: 12,
    },
    img: {
        width: 50,
        height: 50,
        marginTop: 20,
        marginLeft: '5%',
    },
    infoContainer: {
        width: '60%',
        height: '100%',
    },
    clockContainer: {
        height: '17%',
        width: '100%',
        marginTop: 2,
        flexDirection: 'row',
    },
    imageClock: {
        width: 18,
        height: 10,
        resizeMode: 'center',
        marginTop: 2,
        marginLeft: '8%',
    },
    txtClock: {
        fontSize: 10,
        color: '#231F20',
        marginLeft: '2%',

    },
    dificultyContainer: {
        height: '20%',
        marginTop: 8,
        width: '100%',
        flexDirection: 'row',
    },
    txtDificulty: {
        fontSize: 10,
        color: '#231F20',
        marginLeft: '8%',
    },
    imageDificulty: {
        width: 18,
        height: 10,
        resizeMode: 'center',
        marginTop: 2,
        marginLeft: 4
    },
    txtTitleDesc: {
        width: '100%',
        fontWeight: 'bold',
        color: '#231F20',
        fontSize: 20,
        marginLeft: '8%',
        marginTop: 10
    },
    arrowContainer: {
        width: '30%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    nextArrow: {
        width: 17,
        height: 17,
        marginLeft: '25%',
    }
})
export default ListItemNewKultivo;