import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'; 
class ButtonDark extends Component {
    render = () => {
        return(
            <TouchableOpacity style={this.props.style}
            onPress={this.props.onPress}
            >
                <Text
                style={InputStyle.button}
                >
                {this.props.name}
                </Text>
            </TouchableOpacity>
        );
    }
}
const InputStyle = StyleSheet.create({
    button: {
        width: '100%',
        height: '100%',
        color: '#ffffff',
        textAlign: 'center',
        paddingTop: 10,
        fontSize: 19,
        fontWeight: 'bold',
    },

})
export default ButtonDark;