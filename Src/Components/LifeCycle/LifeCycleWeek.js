import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image} from 'react-native'; 
import ButtonDark from '../ButtonDark/ButtonDark';
class LifeCycleWeek extends Component {
    state = {
        color: this.props.borderColor,
        selected: false
    };
    render = () => {
        return(
            <View style={Style.container}>
                <View style = {Style.lineView}></View>
                <Image style={Style.img} source= {require('../../Img/footerImg.png')}/>
                <Text style ={Style.txt}>{this.props.week}</Text>
                <View style = {Style.lineView}></View>
            </View>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
    },
    lineView: {
        backgroundColor: '#C1C1C1',
        width: '20%',
        height: 1,
        marginLeft: 5,
    },
    img: {
        width: 20,
        height:20,
        marginLeft: 5,
    },
    txt: {
        color: '#C1C1C1',
        fontSize: 15,
        marginLeft: 5,
    }
    
})
export default LifeCycleWeek;