import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image, Platform} from 'react-native'; 
import ButtonDark from '../ButtonDark/ButtonDark';
import LifeCycleItem from '../LifeCycle/LifeCycleItem'
class LifeCycleWeather extends Component {
    state = {
        color: this.props.borderColor,
        selected: false,
        tasks: this.props.tasks
    };
    render = () => {
        return(
            <View style={Style.container}>
                <View style={Style.txtDay}>
                    <Text style = {Style.textDay}>{this.props.day}</Text>
                </View>
                <View style= {Style.containerWeather}>
                    <View style= {Style.containerWeatherDesc}>
                        <Text style= {Style.weatherTemp}>{this.props.temp}</Text>
                        <Text style= {Style.weatherTempDesc}>{this.props.weather}</Text>
                        <Text style= {Style.weatherPos}>{this.props.location}</Text>
                        <Text style= {Style.weatherDesc}>{this.props.message}</Text>
                    </View>
                    <View style= {Style.containerWeatherImg}>
                        <Image resizeMode= {'center'} style={Style.imgWeather} source ={this.props.icon}/>
                    </View>
                </View>
                <View style = {Style.listContainer}>
                {this.state.tasks}
                </View>
            </View>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#F7F9E9'
    },
    txtDay: {
        flexDirection: 'column',
        width: 50,
        height: 20,
        backgroundColor: '#8EB84A',
        ...Platform.select({
            ios: {
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
            },
            android: {
                borderRadius: 8,
                marginTop: 10,
            }
        }),
    },
    textDay:{
        width: 50,
        height: 20,
        color: 'white',
        fontSize: 12,
        textAlign: 'center',
        paddingTop: '5%',
    },
    listContainer: {
        width: '90%',
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 15
    },
    containerWeather: {
        width: '100%',
        flexDirection: 'row',
    },
    containerWeatherDesc: {
        width: '75%'
    },
    containerWeatherImg: {
        width: '25%',
        flexDirection: 'row',
    },
    weatherTemp: {
        fontSize: 35,
        color: '#231F20',
        marginTop: 10,
        marginLeft: 15,
        fontWeight: 'bold',
    },
    weatherTempDesc: {
        fontSize: 20,
        color: '#231F20',
        marginTop: 5,
        marginLeft: 15, 
    }, 
    weatherPos: {
        fontSize: 14,
        color: '#231F20',
        marginTop: 0,
        marginLeft: 15, 
    },
    weatherDesc: {
        fontSize: 13,
        color: '#9B9B9B',
        marginTop: 10,
        marginLeft: 15, 
    },
    imgWeather: {
        width: '100%',
        height: 70,
        marginBottom: 15,
    },
    photoButton: {
        width: 100,
        height: 30,
        borderColor: '#231F20',
        borderWidth: 1,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    camera: {
        width: 15, 
        height: 15,
        marginLeft: 5,
    },
    txtPhotoButton: {
        color: '#231F20',
        fontSize: 12,
        marginLeft: 5,
    },
    txtDesc: {
        width: '70%',
        color: '#9B9B9B',
        fontSize: 13,
        marginBottom: 15,
        marginTop: 15,
        textAlign: 'center'
    }
    
})
export default LifeCycleWeather;