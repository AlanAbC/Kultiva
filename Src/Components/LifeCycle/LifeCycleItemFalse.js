import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image, Alert} from 'react-native'; 
import ButtonDark from '../ButtonDark/ButtonDark';
import ip from '../../Config/AppConfig';
class LifeCycleItemFalse extends Component {
    state = {
        color: this.props.borderColor,
        selected: false,
        image: require('../../Img/unchecked.png'),
    };
    singleElement(){
        this.props.navigator.push({
            screen: 'Kultiva.SingleProduct', 
            title: 'Mi Kulttivo',
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarButtonColor: 'white',
              tabBarHidden: true
            }, 
            passProps: {
                id: this.props.id,
                day: this.props.day
            }
          });
    }
    markAsDone(my_crop_id, task_id, card_id){
        fetch(ip + 'users/register_progress/',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                my_crop_id: my_crop_id,
                task_id: task_id,
                card_id: card_id
              }),
        })
        .then((response) => {
            if(response.status == 200){

                this.setState({image: require('../../Img/check.png')});
            }else if (response.status == 404){
                //No podemos recuperar información Kulttiva
                Alert.alert(
                    'Error',
                    'No podemos recuperar información Kulttiva',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }else{
                //Error 500
                Alert.alert(
                    'Error',
                    'No se puede procesar la solicitud',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            }
        })
        .catch((error) => {
            //No podemos comunicarnos con Kulttiva, verifica tu conexión a Internet
            Alert.alert(
            'Error',
            'No podemos comunicarnos con Kulttiva, verifica tu conexión a Internet',
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
            )
            return;
        });
    }
    render = () => {
        return(
            <TouchableOpacity onPress={() => this.singleElement()} style={Style.container}>
                <TouchableOpacity onPress={() => this.markAsDone(this.props.my_crop_id, this.props.task_id, this.props.card_id)}><Image style={Style.img} source= {this.state.image}/></TouchableOpacity>
                <Text style ={Style.txtNumber}>{this.props.position}.</Text>
                <Text style ={Style.txt}>{this.props.name}</Text>
                <View style = {Style.arrowContainer}>
                    <Image resizeMode={'center'} style={Style.nextArrow} source={require('../../Img/nextArrow.png')}/>
                </View>
            </TouchableOpacity>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '93%',
        marginLeft: '3.5%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#C2C2C2',
    },
    lineView: {
        backgroundColor: '#C1C1C1',
        width: '20%',
        height: 1,
        marginLeft: 5,
    },
    img: {
        width: 25,
        height: 25,
        marginLeft: 7.5,
    },
    txt: {
        color: '#231F20',
        fontSize: 14,
        marginLeft: 8,
        width: '65%'
    },
    txtNumber: {
        color: '#9B9B9B',
        fontSize: 14,
        marginLeft: 10,
    },
    arrowContainer: {
        width: '15%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    nextArrow: {
        width: 12,
        height: 12,
        marginLeft: '5%',
    }
    
})
export default LifeCycleItemFalse;