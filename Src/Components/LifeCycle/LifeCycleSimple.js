import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image, Platform} from 'react-native'; 
import ButtonDark from '../ButtonDark/ButtonDark';
import LifeCycleItem from '../LifeCycle/LifeCycleItem'
class LifeCycleSimple extends Component {
    state = {
        color: this.props.borderColor,
        selected: false,
        tasks: this.props.tasks
    };
    render = () => {
        return(
            <View style={Style.container}>
                <View style={Style.txtDay}>
                    <Text style = {Style.textDay}>{this.props.day}</Text>
                </View>
                <View style = {Style.listContainer}>
                {this.state.tasks}
                </View>
            </View>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    txtDay: {
        flexDirection: 'column',
        width: 50,
        height: 20,
        backgroundColor: '#9B9B9B',
        ...Platform.select({
            ios: {
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
            },
            android: {
                borderRadius: 8,
                marginTop: 10,
            }
        }),
    },
    textDay:{
        width: 50,
        height: 20,
        color: 'white',
        fontSize: 12,
        textAlign: 'center',
        paddingTop: '5%',
    },
    listContainer: {
        width: '90%',
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10
    }
    
})
export default LifeCycleSimple;