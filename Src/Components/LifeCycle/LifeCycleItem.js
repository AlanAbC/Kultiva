import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image} from 'react-native'; 
import ButtonDark from '../ButtonDark/ButtonDark';
class LifeCycleItem extends Component {
    state = {
        color: this.props.borderColor,
        selected: false
    };
    singleElement(){
        this.props.navigator.push({
            screen: 'Kultiva.SingleProduct', 
            title: 'Mi Kulttivo',
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarButtonColor: 'white',
              tabBarHidden: true
            }, 
            passProps: {
                id: this.props.id,
                day: this.props.day
            }
          });
    }
    render = () => {
        return(
            <TouchableOpacity onPress={() => this.singleElement()} style={Style.container}>
                <Image style={Style.img} source= {require('../../Img/check.png')}/>
                <Text style ={Style.txtNumber}>{this.props.position}.</Text>
                <Text style ={Style.txt}>{this.props.name}</Text>
                <View style = {Style.arrowContainer}>
                    <Image resizeMode={'center'} style={Style.nextArrow} source={require('../../Img/nextArrow.png')}/>
                </View>
            </TouchableOpacity>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '93%',
        marginLeft: '3.5%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#C2C2C2',
    },
    lineView: {
        backgroundColor: '#C1C1C1',
        width: '20%',
        height: 1,
        marginLeft: 5,
    },
    img: {
        width: 25,
        height: 25,
        marginLeft: 7.5,
    },
    txt: {
        color: '#231F20',
        fontSize: 14,
        marginLeft: 8,
        width: '65%'
    },
    txtNumber: {
        color: '#9B9B9B',
        fontSize: 14,
        marginLeft: 10,
    },
    arrowContainer: {
        width: '15%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    nextArrow: {
        width: 12,
        height: 12,
        marginLeft: '5%',
    }
    
})
export default LifeCycleItem;