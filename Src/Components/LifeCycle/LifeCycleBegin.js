import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image, ScrollView, Platform} from 'react-native'; 
import Button from 'react-native-button';
import LifeCycleItem from '../LifeCycle/LifeCycleItem'
class LifeCycleBegin extends Component {
    state = {
        color: this.props.borderColor,
        selected: false,
        tasks: this.props.tasks,
        disabledColor: this.props.disabledColor
    };
    render = () => {
        return(
            <View style={Style.container}>
                <View style={Style.top}>
                    <Button disabled = {this.props.disabled} onPress = {this.props.onPressBegin} disabledContainerStyle = {Style.buttonDarkDisabled} containerStyle = {Style.buttonDark} style ={Style.buttonStyle}>Empezar Kulttivo</Button>
                    <Image resizeMode= {'center'} style={Style.arrow} source= {require('../../Img/uparrow.png')}/>
                </View>
                <ImageBackground resizeMode= {'cover'} style={Style.bottom} source= {require('../../Img/begining.png')}>
                    <View style={Style.infoContainer}>
                        <View style = {Style.topInfo}>
                            <Image resizeMode = {'center'} style= {Style.imgFarmer} source= {require('../../Img/farmer.png')}/>
                            <Text style = {Style.txtInfo}>¡Bienvenido a tu Kulttivo! Es importante que cuentes con los suministros necesarios para empezar a sembrar tus semillas. {"\n"}{"\n"} Te recomendamos contar con lo siguiente para empezar.</Text>
                        </View>
                        <View style={Style.scroll}>
                            {this.state.tasks}
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '100%',
    },
    top: {
        width: '100%',
        height: 175,
        alignItems: 'center',
    },
    bottom: {
        width: '100%',
        alignItems: 'center',
    },
    infoContainer :{
        width: '90%',
        marginTop: 100,
        marginBottom: 25,
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 5,
        alignItems: 'center',
    },
    arrow: {
        height: '70%',
    },
    topInfo: {
        width: '100%',
        height: 185,
        alignItems: 'center',
    },
    imgFarmer: {
        height: 43,
        width: 40,
        marginTop: 10,
        ...Platform.select({
            ios: {
                paddingTop: 15,
                paddingBottom: 15,
                paddingLeft: 15,
                paddingRight: 15,
            },
        }),
    },
    txtInfo: {
        width: '90%',
        textAlign: 'center',
        color: '#9B9B9B',
        fontSize: 14,
        marginTop: 7
    },
    buttonDark: {
        width: '80%',
        height: 45,
        backgroundColor:'#231F20',
        borderRadius: 5,
        marginTop: 30,
        paddingTop: 10,
    },
    buttonDarkDisabled: {
        width: '80%',
        height: 45,
        backgroundColor:'#9B9B9B' ,
        borderRadius: 5,
        marginTop: 30,
        paddingTop: 10,
    },
    buttonStyle: {
        color: 'white'
    },
    scroll: {
        width: '93%',
    }
})
export default LifeCycleBegin;