import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image, ScrollView, Platform} from 'react-native'; 
import Button from 'react-native-button';
import LifeCycleItem from '../LifeCycle/LifeCycleItem'
class LifeCycleEmpty extends Component {
    state = {
        color: this.props.borderColor,
        selected: false,
        tasks: this.props.tasks
    };
    render = () => {
        return(
            <View style={Style.container}>
                <View style={Style.txtDay}>
                    <Text style = {Style.textDay}>{this.props.day}</Text>
                </View>
                <View style = {Style.card}>
                <Image resizeMode = {'center'} style= {Style.imgFarmer} source= {require('../../Img/farmer.png')}/>
                <Text style = {Style.txtInfo}>Nada que hacer por hoy{"\n"}{"\n"} Vuelve mañana para continuar con tus actividades{"\n"}{"\n"}</Text>
                </View>
                
            </View>
    
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
    },
    top: {
        width: '100%',
        height: 175,
        alignItems: 'center',
    },
    bottom: {
        width: '100%',
        alignItems: 'center',
    },
    infoContainer :{
        width: '90%',
        marginTop: 100,
        marginBottom: 25,
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 5,
        alignItems: 'center',
    },
    arrow: {
        height: '70%',
    },
    topInfo: {
        width: '100%',
        height: 185,
        alignItems: 'center',
    },
    imgFarmer: {
        height: 43,
        width: 40,
        marginTop: 10,
        ...Platform.select({
            ios: {
                paddingTop: 15,
                paddingBottom: 15,
                paddingLeft: 15,
                paddingRight: 15,
            },
        }),
    },
    txtInfo: {
        width: '90%',
        textAlign: 'center',
        color: '#9B9B9B',
        fontSize: 14,
        marginTop: 7
    },
    txtDay: {
        flexDirection: 'column',
        width: 50,
        height: 20,
        backgroundColor: '#9B9B9B',
        ...Platform.select({
            ios: {
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
            },
            android: {
                borderRadius: 8,
                marginTop: 10,
            }
        }),
    },
    textDay:{
        width: 50,
        height: 20,
        color: 'white',
        fontSize: 12,
        textAlign: 'center',
        paddingTop: '5%',
    },
    buttonDark: {
        width: '80%',
        height: 45,
        backgroundColor: '#231F20',
        borderRadius: 5,
        marginTop: 30,
        paddingTop: 10,
    },
    buttonStyle: {
        color: 'white'
    },
    scroll: {
        width: '93%',
    },
    card:{
        width: '90%',
        marginTop: 15,
        marginBottom: 15,
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 5,
        alignItems: 'center',
    }
})
export default LifeCycleEmpty;