import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ImageBackground, Image, Alert, Platform, ActivityIndicator, ImageEditor, ImageStore} from 'react-native'; 
import ButtonDark from '../ButtonDark/ButtonDark';
import LifeCycleItem from '../LifeCycle/LifeCycleItem';
import ImagePicker from 'react-native-image-picker';
import uuid from 'react-native-uuid';
import ip from '../../Config/AppConfig';
import NativeModules from 'NativeModules';
import Share, {ShareSheet, Button} from 'react-native-share';
class LifeCyclePhoto extends Component {
    state = {
        color: this.props.borderColor,
        selected: false,
        showImage: this.props.showImage,
        avatarSource: this.props.avatarSource,
        sourceFile: 'data:image/jpeg;base64,' +  this.props.sourceFile,
    };
    onClickFb() {
        Share.shareSingle(Object.assign(
          {title: "React Native",
          message: "Mi Kulttivo",
          url: this.state.sourceFile,
          subject: "Share Link" //  for email}
         }, {
          "social": "facebook"}
        ))
      }
      onClickMs() {
        Share.shareSingle(Object.assign(
          {title: "React Native",
          message: "Mi Kulttivo",
          url: this.state.sourceFile,
          subject: "Share Link" //  for email}
         }, {
          "social": "whatsapp"}
        ))
      }
      onClickIns() {
        Share.shareSingle(Object.assign(
          {title: "React Native",
          message: "Mi Kulttivo",
          url: this.state.sourceFile,
          subject: "Share Link" //  for email}
         }, {
          "social": "instagram"}
        ))
      }
      onClickTw() {
        Share.shareSingle(Object.assign(
            {title: "React Native",
            message: "Mi Kulttivo",
            url: this.state.sourceFile,
            subject: "Share Link" //  for email}
           }, {
            "social": "twitter"}
          ))
      }
      onClick() {
        Share.open(
          {title: "React Native",
          message: "Mi Kulttivo",
          url: this.state.sourceFile,
          subject: "Share Link" //  for email}
         }
        )
    }
    selectPhotoTapped() {
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          storageOptions: {
            skipBackup: true
          }
        };
        this.setState({
            showOverlay: true,
          });
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          }
          else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          }
          else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          }
          else {
            let source = { uri: response.uri };
            // ImageStore.getBase64ForTag(source.uri,(base64image) => {console.log('Base 64', base64image);}, (error) => { console.log(error)})
            const imageURL = source.uri;
            Image.getSize(imageURL, (width, height) => {
                var imageSize = {
                    size: {
                      width,
                      height
                    },
                    offset: {
                      x: 0,
                      y: 0,
                    },
                  };
                ImageEditor.cropImage(imageURL, imageSize, (imageURI) => {
                    console.log(imageURI);
                    ImageStore.getBase64ForTag(imageURI, (base64Data) => {
                        console.log(base64Data)
                        this.setState({sourceFile: 'data:image/jpeg;base64,' + response.data});
                        ImageStore.removeImageForTag(imageURI);
                    }, (reason) => console.log(reason, 'reason 1') )
                }, (reason) => console.log(reason, 'reason 1') )
            }, (reason) => console.log(reason, 'reason 1'))
            let formdata = new FormData();
            formdata.append('farmer_crop', this.props.farmer_crop)
            formdata.append('card', this.props.id)
            formdata.append('photo', {uri: source.uri, name: 'image' + uuid.v4() + '.png', type: 'multipart/form-data'})
            console.log(this.props.farmer_crop, 'farmer crop', this.props.id, 'card id', source.uri, 'source')
            fetch(ip + 'users/upload_image_task/',{
                method: 'POST',
                body: formdata
            })
            .then((response) => {
                console.log(response);
                if(response.status == 200){
                    data = JSON.parse(response._bodyText);
                    console.log(data);
                    // ImageStore.getBase64ForTag(data.imagen,(base64image) => {console.log('Base 64', base64image);}, (error) => { console.log(error)})
                    this.setState({
                        showOverlay: false,
                      });
                }else if (response.status == 404){
                    //No podemos recuperar información Kulttiva
                    this.setState({
                        showOverlay: false,
                      });
                    Alert.alert(
                        'Error',
                        'No podemos recuperar información Kulttiva',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }else{
                    //error 500
                    //No se puede procesar la solicitud
                    this.setState({
                        showOverlay: false,
                      });
                    Alert.alert(
                        'Error',
                        'No se puede procesar la solicitud',
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: false }
                      )
                }
            })
            .catch((error) => {
                //No podemos comunicarnos con Kulttiva, verifica tu conexión a internet
                this.setState({
                    showOverlay: false,
                  });
                Alert.alert(
                    'Error',
                    'No podemos comunicarnos con Kulttiva, verifica tu conexión a internet',
                    [
                      {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                  )
            });
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            this.setState({
              avatarSource: source,
              showImage: true,
            });
          }
        });
      }
    render = () => {
        return(
            <View style={Style.container}>
                <View style={Style.txtDay}>
                    <Text style = {Style.textDay}>{this.props.day}</Text>
                </View>
                <View style = {Style.listContainer}>
                    {this.props.tasks}
                </View>
                {!this.state.showImage && 
                <TouchableOpacity style = {Style.photoButton} /*onPress={this.props.onPress}*/ onPress={()=>this.selectPhotoTapped()}>
                        <Image style = {Style.camera} resizeMode= {'center'} source= {require('../../Img/camera.png')}/>
                        <Text style= {Style.txtPhotoButton}>Foto del día</Text>
                </TouchableOpacity>}
                {this.state.showImage && 
                <View style ={Style.imgContainer}>
                <View style={Style.imagePhotoView}>
                    {this.state.showOverlay &&
                    <ActivityIndicator size="large" color="#231F20" style={{alignSelf: 'center',}}/>
                    } 
                    {!this.state.showOverlay && <Image style={Style.imagePhoto}
                    source={this.state.avatarSource}
                    resizeMethod='scale'
                    />}
                </View>
                <View style ={Style.socialContainer}>
                        <TouchableOpacity style = {Style.socialIcon} onPress = {() => this.onClickFb()}><Image resizeMode={'center'} style ={Style.iconimage} source = {require('../../Img/fb.png')}/></TouchableOpacity>
                        <TouchableOpacity style = {Style.socialIcon3} onPress = {() => this.onClickMs()}><Image resizeMode={'center'} style ={Style.iconimage} source = {require('../../Img/whatsapp.png')}/></TouchableOpacity>
                        <TouchableOpacity style = {Style.socialIcon2} onPress = {() => this.onClickTw()}><Image resizeMode={'center'} style ={Style.iconimage} source = {require('../../Img/tw.png')}/></TouchableOpacity>
                        <TouchableOpacity style = {Style.socialIcon2} onPress = {() => this.onClick()}><Image resizeMode={'center'} style ={Style.iconimage} source = {require('../../Img/more.png')}/></TouchableOpacity>
                    </View>
                </View>}
                <Text style = {Style.txtDesc}>Tu kulttivo esta progresando, ¡toma una foto! 😎</Text>
            </View>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    txtDay: {
        flexDirection: 'column',
        width: 50,
        height: 20,
        backgroundColor: '#9B9B9B',
        ...Platform.select({
            ios: {
                borderBottomLeftRadius: 8,
                borderBottomRightRadius: 8,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
            },
            android: {
                borderRadius: 8,
                marginTop: 10,
            }
        }),
    },
    textDay:{
        width: 50,
        height: 20,
        color: 'white',
        fontSize: 12,
        textAlign: 'center',
        paddingTop: '5%',
    },
    listContainer: {
        width: '90%',
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10
    },
    photoButton: {
        width: 100,
        height: 30,
        borderColor: '#231F20',
        borderWidth: 1,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    camera: {
        width: 15, 
        height: 15,
        marginLeft: 5,
    },
    txtPhotoButton: {
        color: '#231F20',
        fontSize: 12,
        marginLeft: 5,
    },
    txtDesc: {
        width: '70%',
        color: '#9B9B9B',
        fontSize: 13,
        marginBottom: 15,
        marginTop: 15,
        textAlign: 'center'
    },
    imagePhotoView: {
        flexDirection: 'column',
        alignItems: 'center',
        width: 110,
        height: 110,
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        borderWidth: 10,
        borderColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
    },
    imagePhoto: {
        width: '100%',
        height: '100%',
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
    },
    imgContainer: {
        width: '100%',
        alignItems: 'center',
    },
    socialContainer: {
        width: '100%',
        height: 70,
        flexDirection: 'row',
        alignItems: 'center',
    },
    iconimage: {
        width: '100%',
    },
    socialIcon: {
        width: '26%',
        marginLeft: '10.5%',
    },
    socialIcon2: {
        width: '15%',
        marginLeft: '1%'
    },
    socialIcon3: {
        width: '15%',
        marginLeft: '-3%',
        padding: '3%',
    }
})
export default LifeCyclePhoto;