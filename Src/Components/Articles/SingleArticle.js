import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
class SingleArticle extends Component {
    render = () => {
        return(
            <TouchableOpacity onPress = {this.props.onPress} style={Style.container}>
                <Image resizeMode={'cover'} style={Style.img} source={this.props.image}/>
                <Text style = {Style.name}>{this.props.name}</Text>
                <Text style = {Style.category}>Ver Articulo</Text>
            </TouchableOpacity>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '90%',
        borderBottomColor: '#E4E3E3',
        borderBottomWidth: .5,
        alignItems: 'center',
        marginLeft: '3.5%',
        marginTop: 7                  
    },
    name: {
        width: '100%',
        marginTop: 7,
        color: '#231F20',
        fontSize: 23,
        fontWeight: 'bold',
    },
    img: {
        width: '100%',
        height: 160,
        borderRadius: 8,
    },
    category: {
        width: '100%',
        marginTop: 7,
        color: '#C78F51',
        fontSize: 14,
        marginBottom: 18
    },
})
export default SingleArticle;