import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
class SingleCategory extends Component {
   
    state = {
        id: this.props.id,
        _this: this.props.this
    }
    onPressItem = () =>{
            this.state._this.push({
            screen: 'Kultiva.Articles2', 
            title: this.props.title,
            navigatorStyle: {
              navBarBackgroundColor: '#231F20',
              navBarTextColor: 'white',
              navBarButtonColor: 'white',
              tabBarHidden: true
            }, 
            passProps: {
                id: this.state.id
            }
          });
    }
    render = () => {
        return(
            <TouchableOpacity onPress = {()=> this.onPressItem()} style = {Style.category}>
                <Image resizeMode = {'cover'} style = {Style.image} source = {{uri: this.props.image}}/>
                <Text style = {Style.txtCat}>{this.props.title}</Text>
            </TouchableOpacity>
        );
    }
}
const Style = StyleSheet.create({
    container: {
        width: '93%',
        borderBottomColor: '#EFEFEF',
        borderBottomWidth: .5,
        alignItems: 'center',
        marginLeft: '3.5%',
        marginTop: 7                  
    },
    name: {
        width: '100%',
        marginTop: 7,
        color: '#231F20',
        fontSize: 23,
        fontWeight: 'bold',
    },
    img: {
        width: '100%',
        height: 180,
        borderRadius: 8,
    },
    category:{
        width: '95%',
        height: 150,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 7,
        backgroundColor: '#F9F9F9', 
        elevation: 6
    },
    image:{
        width: '100%',
        height:'75%',
        borderTopLeftRadius: 7,
        borderTopRightRadius: 7,
    },
    txtCat: {
        width: '100%',
        height: '25%',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 15,
        color: '#231F20',
    }
})
export default SingleCategory;