import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
class BtnIconBlack extends Component {
    render = () => {
        return(
            <TouchableOpacity onPress={this.props.onPress} style={this.props.style}>
                <Icon
                style={InputStyle.icon}
                size={30}
                name={this.props.name}
                color='#000000'
                />
                <Text
                style={InputStyle.input}
                >{this.props.nameBtn}</Text>
            </TouchableOpacity>
        );
    }
}
const InputStyle = StyleSheet.create({
    input: {
        paddingLeft: 15,
        width: '90%',
        height: '100%',
        fontSize: 19,
        ...Platform.select({
            ios: {
                paddingVertical: '4%',
            },
            android: {
                textAlignVertical: 'center',
            }
        }),
        
    },
    icon: {
        paddingLeft: 15,
    }
});
export default BtnIconBlack;