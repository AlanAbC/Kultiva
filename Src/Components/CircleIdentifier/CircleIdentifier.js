import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'; 
class CircleIdentifier extends Component {
    state = {
        color: this.props.borderColor,
        selected: false
    };
    render = () => {
        return(
            <TouchableOpacity onPress = {this.props.onPress} style={Style(this.props.borderColor, this.props.backGround, this.props.marginTop, this.props.marginLeft)}>
            </TouchableOpacity>
        );
    }
}
const Style = function(myColor, backGround, marginTop, marginLeft){
    return {
        width: 20,
        height: 20,
        marginTop: marginTop,
        marginLeft: marginLeft,
        borderRadius: 20,
        borderWidth: 1.5,
        borderColor: myColor,
        backgroundColor: backGround
    }
}
export default CircleIdentifier;