import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet, Image, TouchableOpacity, Platform} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import CircleIdentifier from '../../Components/CircleIdentifier/CircleIdentifier'
class HomeListItem extends Component {
    render = () => {
        if (Platform.OS === 'ios') {
            return(
                <TouchableOpacity onPress = {this.props.onPress} style={Style.container}>
                    <CircleIdentifier borderColor={this.props.color} marginTop={this.props.marginTop} marginLeft={this.props.marginLeft}/>
                    <Text style = {Style.name}>{this.props.name}</Text>
                    <View style = {Style.arrowContainer}>
                        <Image /*resizeMode={'center'}*/ style={Style.nextArrow} source={require('../../assets/Images/nextArrow.png')}/>
                    </View>
                </TouchableOpacity>
            );
        }else{
            return(
                <TouchableOpacity onPress = {this.props.onPress} style={Style.container}>
                    <CircleIdentifier borderColor={this.props.color} marginTop={this.props.marginTop} marginLeft={this.props.marginLeft}/>
                    <Text style = {Style.name}>{this.props.name}</Text>
                    <View style = {Style.arrowContainer}>
                        <Image resizeMode={'center'} style={Style.nextArrow} source={require('../../Img/nextArrow.png')}/>
                    </View>
                </TouchableOpacity>
            );
        }
        
    }
}
const Style = StyleSheet.create({
    container: {
        width: '100%',
        height: 50,
        borderBottomColor: '#979797',
        borderBottomWidth: .5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    name: {
        width: '65%',
        marginLeft: '5%',
        height: 25,
        color: '#231F20',
        fontSize: 18,
    },
    arrowContainer: {
        width: '15%',
        height: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    nextArrow: {
        width: 27,
        height: 27,
        ...Platform.select({
            android: {
                marginLeft: '25%',
                width: 15,
                height: 15,
            },
        }),
    }
})
export default HomeListItem;