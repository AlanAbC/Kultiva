import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
class Input extends Component {
    render = () => {
        return(
            <View style={this.props.style}>
                <Icon
                style={InputStyle.icon}
                size={20}
                name={this.props.name}
                color='#2e8232'
                />
                <TextInput
                style={InputStyle.input}
                placeholder={this.props.placeholder}
                secureTextEntry={this.props.secure}
                onChangeText={this.props.handler}
                value={this.props.value}
                autoCapitalize={this.props.autoCapitalize}
                spellCheck={this.props.spell}
                underlineColorAndroid= 'rgba(0,0,0,0)'
                keyboardType={this.props.keyboardType}
                returnKeyType='done'
                />
            </View>
        );
    }
}
const InputStyle = StyleSheet.create({
    input: {
        paddingLeft: 12,
        width: '90%',
        height: '100%',
        
    },
    icon: {
        paddingLeft: 12,
    }

})
export default Input;