import {Navigation} from 'react-native-navigation';
import FirstScreen from './Src/Views/FirstScreen';
import Terms from './Src/Views/Terms';
import Login from './Src/Views/Login';
import Register from './Src/Views/Register';
import Slider from './Src/Views/Slider';
import GPSPetitiion from './Src/Views/GPSPetition';
import Home from './Src/Views/Home';
import NewKultivo from './Src/Views/NewKultivo';
import KulttivoName from './Src/Views/KulttivoName';
import SingleElement from './Src/Views/SingleElement';
import Location from './Src/Views/Location';
import LifeCycle from './Src/Views/LifeCycle';
import ResetPassword from './Src/Views/ResetPassword';
import Articles from './Src/Views/Articles';
import SingleArticle from './Src/Views/SingleArticle';
import Logout from './Src/Views/Logout';
import SingleProduct from './Src/Views/SingleProducts';
import ChangePasword from './Src/Views/ChangePasword';
import ChangeLocation from './Src/Views/ChangeLocation';
import Articles2 from './Src/Views/Articles2';
import Notifications from './Src/Views/Notifications';
import Store from './Src/Views/Store';
import Modal from './Src/Views/Modal';
import ls from 'react-native-local-storage';
import CheckAccount from './Src/Views/CheckAccount';

//Register screens
Navigation.registerComponent("Kultiva.FirstScreen", ()=> FirstScreen);
Navigation.registerComponent("Kultiva.Login", ()=> Login);
Navigation.registerComponent("Kultiva.Terms",()=>  Terms);
Navigation.registerComponent("Kultiva.Slider",()=>  Slider);
Navigation.registerComponent("Kultiva.Register",()=>  Register);
Navigation.registerComponent("Kultiva.GPSPetitiion",()=>  GPSPetitiion);
Navigation.registerComponent("Kultiva.Home",()=>  Home);
Navigation.registerComponent("Kultiva.NewKultivo",()=>  NewKultivo);
Navigation.registerComponent("Kultiva.KulttivoName",()=>  KulttivoName);
Navigation.registerComponent("Kultiva.SingleElement",()=>  SingleElement);
Navigation.registerComponent("Kultiva.Location",()=>  Location);
Navigation.registerComponent("Kultiva.SingleProduct",()=>  SingleProduct);
Navigation.registerComponent("Kultiva.LifeCycle",()=>  LifeCycle);
Navigation.registerComponent("Kultiva.ResetPassword",()=>  ResetPassword);
Navigation.registerComponent("Kultiva.Articles",()=>  Articles);
Navigation.registerComponent("Kultiva.SingleArticle",()=>  SingleArticle);
Navigation.registerComponent("Kultiva.Logout",()=>  Logout);
Navigation.registerComponent("Kultiva.CheckAccount", () => CheckAccount);
Navigation.registerComponent("Kultiva.ChangePasword", () => ChangePasword);
Navigation.registerComponent("Kultiva.ChangeLocation", () => ChangeLocation);
Navigation.registerComponent("Kultiva.Store", () => Store);
Navigation.registerComponent("Kultiva.Articles2", () => Articles2);
Navigation.registerComponent("Kultiva.Notifications", () => Notifications);
Navigation.registerComponent("Kultiva.Modal", () => Modal);

//Start App
ls.get('farmer_id').then((local) => {
  if(local !== null){
  Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'Kultivos', // tab label as appears under the icon in iOS (optional)
        screen: 'Kultiva.Home', // unique ID registered with Navigation.registerScreen
        icon: require('./Src/assets/Images/iconHome.png'), // local image asset for the tab icon unselected state (optional on iOS)
        selectedIcon: require('./Src/assets/Images/iconHome.png'), // local image asset for the tab icon selected state (optional, iOS only. On Android, Use `tabBarSelectedButtonColor` instead)
        // iconInsets: { 
        //   top: 10,
        //   bottom: 10,
        //   left: 10,
        //   right: 10,
        // },
        title: 'Home', // title of the screen as appears in the nav bar (optional)
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white'
        }, // override the navigator style for the tab screen, see "Styling the navigator" below (optional),
        navigatorButtons: {} // override the nav buttons for the tab screen, see "Adding buttons to the navigator" below (optional)
      },
      {
        label: 'Biblioteca',
        screen: 'Kultiva.Articles',
        icon: require('./Src/assets/Images/iconBiblioteca.png'),
        selectedIcon: require('./Src/assets/Images/iconBiblioteca.png'),
        title: 'Biblioteca',
        // iconInsets: { 
        //   top: 10,
        //   bottom: 10,
        //   left: 10,
        //   right: 10,
        // },
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white'
        },  
      },
      {
        label: 'Tienda',
        screen: 'Kultiva.Store',
        icon: require('./Src/assets/Images/box.png'),
        selectedIcon: require('./Src/assets/Images/box.png'),
        title: 'Tienda',
        // iconInsets: { 
        //   top: 0,
        //   bottom: 0,
        //   left: 0,
        //   right: 0,
        // },
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white',
          navBarHidden: true
        },  
      },
      {
        label: 'Ajustes',
        screen: 'Kultiva.Logout',
        icon: require('./Src/assets/Images/iconSettings.png'),
        selectedIcon: require('./Src/assets/Images/iconSettings.png'),
        title: 'Ajustes',
        // iconInsets: { 
        //   top: 10,
        //   bottom: 10,
        //   left: 10,
        //   right: 10,
        // },
        navigatorStyle: {
          navBarBackgroundColor: '#231F20',
          navBarTextColor: 'white'
        },  
      }
    ],
    appStyle: {
      tabBarSelectedButtonColor: '#8EB84A',
      tabFontSize: 10,
      selectedTabFontSize: 12,
    },
    tabsStyle: {
      tabBarSelectedButtonColor: '#8EB84A',
      selectedTabFontSize: 12,
    },
    passProps: {}, // simple serializable object that will pass as props to all top screens (optional)
    animationType: 'slide-down' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
  });
}else{
  Navigation.startSingleScreenApp({
    screen:{
      screen: "Kultiva.Slider",
      title: "Home",
      navigatorStyle: {
        navBarBackgroundColor: '#231F20',
        navBarTextColor: 'white',
        navBarHidden: true
      }
    }
  });
} 
});