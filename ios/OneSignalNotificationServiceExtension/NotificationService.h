//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Ana Victoria Frias on 27/03/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
