package com.kultiva;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.graphics.Color;
import android.widget.TextView;
import android.view.Gravity;
import android.util.TypedValue;
import com.facebook.react.ReactActivity;
import com.reactnativenavigation.controllers.SplashActivity;

import org.devio.rn.splashscreen.SplashScreen;

public class MainActivity extends SplashActivity {
    @Override
    public LinearLayout createSplashLayout(){
        LinearLayout view = new LinearLayout(this);
        ImageView imageView = new ImageView(this);

        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setGravity(Gravity.CENTER);

        imageView.setBackgroundResource(R.drawable.splash);
        view.addView(imageView);
        return view;
    }
}
