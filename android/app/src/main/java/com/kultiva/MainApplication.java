package com.kultiva;

import android.app.Application;

import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.facebook.react.ReactApplication;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import cl.json.RNSharePackage;
import cl.json.ShareApplication;

import com.imagepicker.ImagePickerPackage;


import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.magus.fblogin.FacebookLoginPackage;
import com.reactnativenavigation.NavigationApplication;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication implements ShareApplication{

    @Override
    public boolean isDebug() {
      // Make sure you are using BuildConfig from your own application
      return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
      // Add additional packages you require here
      // No need to add RnnPackage and MainReactPackage
      return Arrays.<ReactPackage>asList(
              new ReactVideoPackage(),
              new VectorIconsPackage(),
              new RNFusedLocationPackage(),
              new SplashScreenReactPackage(),
              new FacebookLoginPackage(),
              new MapsPackage(),
              new ImagePickerPackage(),
              new RNSharePackage(),
              new ReactNativeOneSignalPackage(),
              new RNGoogleSigninPackage()

      );
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
      return getPackages();
    }
    @Override
    public String getJSMainModuleName() {
      return "index";
    }
    @Override
    public String getFileProviderAuthority() {
        return "com.kultiva.provider";
    }
}
